#pragma once
#include "Description.h"
#include "frmTaskTZ.h"
#include <math.h>


#using <System.dll>
#using <System.Drawing.dll>
#using <System.Windows.Forms.dll>

namespace kurs_srv {

	using namespace System;
	using namespace System::Diagnostics;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Windows::Forms::DataVisualization::Charting;
	using namespace System::Threading;
	

	/// <summary>
	/// ������ ��� Form1
	/// </summary>
	public ref class frmMain : public System::Windows::Forms::Form
	{
	private:
		Stopwatch^ stopWatch;
		Thread^ Task1Thread;
		Thread^ Task2Thread;
		Thread^ Task3Thread;

	private: System::Windows::Forms::Timer^  TimerSystem;
	private: System::Windows::Forms::Label^  lblParams;
	private: System::Windows::Forms::DataGridView^  dgrdParams;
	private: System::Windows::Forms::Button^  btnOpenXlsFile;
	private: System::Windows::Forms::Button^  btnSaveXlsFile;
	private: System::Windows::Forms::Label^  lblLog;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  clmnParamNames;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  clmnTask1;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  clmnTask2;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  clmnTask3;
	private: System::Windows::Forms::RichTextBox^  LogText;

	private: System::Windows::Forms::PictureBox^  picTplan;
	private: System::Windows::Forms::PictureBox^  picTfact;
	private: System::Windows::Forms::PictureBox^  picEdit;
	private: System::Windows::Forms::PictureBox^  picPause;
	private: System::Windows::Forms::PictureBox^  picAbort;
	private: System::Windows::Forms::Label^  lblTplan;
	private: System::Windows::Forms::Label^  lblTfact;
	private: System::Windows::Forms::Label^  lblPause;
	private: System::Windows::Forms::Label^  lblAbort;
	private: System::Windows::Forms::Label^  lblEdit;
	private: System::Windows::Forms::GroupBox^  gbMarkers;
	private: System::Windows::Forms::GroupBox^  gbExtraParam;
	private: System::Windows::Forms::GroupBox^  gbCallDelay;
	private: System::Windows::Forms::GroupBox^  gbExcessSession;

	private: System::Windows::Forms::Label^  lblCallDelay;
	private: System::Windows::Forms::Label^  lblTextInterrupt;

	private: System::Windows::Forms::Label^  lblExcessSession;
	private: System::Windows::Forms::Label^  lblTaskView;
	private: System::Windows::Forms::Label^  lblTextView;
	private: System::Windows::Forms::Label^  lblTaskIsEditable;
	private: System::Windows::Forms::Label^  lblTask�alculation;

	private: System::Windows::Forms::Label^  lblTextIsEditable;
	private: System::Windows::Forms::Label^  lblText�alculation;


	private: System::Windows::Forms::Label^  lblTaskInterrupt;
	private: System::Windows::Forms::Label^  lblTaskKeyInterrupt;
	private: System::Windows::Forms::Label^  lblTextKeyInterrupt;
	private: System::Windows::Forms::Label^  lblTaskPauseContinue;

	private: System::Windows::Forms::Label^  lblTextPauseContinue;


	private: System::Windows::Forms::Button^  btnTZ;
	public:
		frmMain(void)
		{
			stopWatch = gcnew Stopwatch;
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}
	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~frmMain()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::DataVisualization::Charting::Chart^  chrtMain;
	private: System::Windows::Forms::Button^  btnExitMain;
	public: System::Windows::Forms::StatusStrip^  StatusStrip;
	public: System::Windows::Forms::ToolStripStatusLabel^  StatusBarStatus;
	private: System::Windows::Forms::ToolStripStatusLabel^  StatusBarTime;
	private: System::Windows::Forms::Timer^  TimerStopWatch;
	private: System::Windows::Forms::Button^  btnPlayMain;
	private: System::Windows::Forms::Button^  btnPauseMain;
	private: System::Windows::Forms::Button^  btnProceedMain;
	private: System::Windows::Forms::Button^  btnStopMain;
	private: System::Windows::Forms::ToolTip^  ToolTipMain;
	private: System::Windows::Forms::Button^  btnEditMain;
	private: System::Windows::Forms::Label^  lblStopWatch;
	private: System::ComponentModel::IContainer^  components;
	protected: 
	protected: 
	protected: 
	private:
		
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		
#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::Windows::Forms::DataVisualization::Charting::ChartArea^  chartArea1 = (gcnew System::Windows::Forms::DataVisualization::Charting::ChartArea());
			System::Windows::Forms::DataVisualization::Charting::ChartArea^  chartArea2 = (gcnew System::Windows::Forms::DataVisualization::Charting::ChartArea());
			System::Windows::Forms::DataVisualization::Charting::ChartArea^  chartArea3 = (gcnew System::Windows::Forms::DataVisualization::Charting::ChartArea());
			System::Windows::Forms::DataVisualization::Charting::ChartArea^  chartArea4 = (gcnew System::Windows::Forms::DataVisualization::Charting::ChartArea());
			System::Windows::Forms::DataVisualization::Charting::Series^  series1 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
			System::Windows::Forms::DataVisualization::Charting::Series^  series2 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
			System::Windows::Forms::DataVisualization::Charting::Series^  series3 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
			System::Windows::Forms::DataVisualization::Charting::Series^  series4 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
			System::Windows::Forms::DataVisualization::Charting::Series^  series5 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
			System::Windows::Forms::DataVisualization::Charting::Title^  title1 = (gcnew System::Windows::Forms::DataVisualization::Charting::Title());
			System::Windows::Forms::DataVisualization::Charting::Title^  title2 = (gcnew System::Windows::Forms::DataVisualization::Charting::Title());
			System::Windows::Forms::DataVisualization::Charting::Title^  title3 = (gcnew System::Windows::Forms::DataVisualization::Charting::Title());
			System::Windows::Forms::DataVisualization::Charting::Title^  title4 = (gcnew System::Windows::Forms::DataVisualization::Charting::Title());
			System::Windows::Forms::DataVisualization::Charting::Title^  title5 = (gcnew System::Windows::Forms::DataVisualization::Charting::Title());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(frmMain::typeid));
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle1 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle6 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle7 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle2 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle3 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle4 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle5 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			this->chrtMain = (gcnew System::Windows::Forms::DataVisualization::Charting::Chart());
			this->btnExitMain = (gcnew System::Windows::Forms::Button());
			this->StatusStrip = (gcnew System::Windows::Forms::StatusStrip());
			this->StatusBarStatus = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->StatusBarTime = (gcnew System::Windows::Forms::ToolStripStatusLabel());
			this->TimerStopWatch = (gcnew System::Windows::Forms::Timer(this->components));
			this->btnPlayMain = (gcnew System::Windows::Forms::Button());
			this->btnPauseMain = (gcnew System::Windows::Forms::Button());
			this->btnProceedMain = (gcnew System::Windows::Forms::Button());
			this->btnStopMain = (gcnew System::Windows::Forms::Button());
			this->ToolTipMain = (gcnew System::Windows::Forms::ToolTip(this->components));
			this->btnEditMain = (gcnew System::Windows::Forms::Button());
			this->btnTZ = (gcnew System::Windows::Forms::Button());
			this->btnOpenXlsFile = (gcnew System::Windows::Forms::Button());
			this->btnSaveXlsFile = (gcnew System::Windows::Forms::Button());
			this->gbMarkers = (gcnew System::Windows::Forms::GroupBox());
			this->lblTplan = (gcnew System::Windows::Forms::Label());
			this->lblEdit = (gcnew System::Windows::Forms::Label());
			this->picTplan = (gcnew System::Windows::Forms::PictureBox());
			this->picEdit = (gcnew System::Windows::Forms::PictureBox());
			this->lblAbort = (gcnew System::Windows::Forms::Label());
			this->picTfact = (gcnew System::Windows::Forms::PictureBox());
			this->picAbort = (gcnew System::Windows::Forms::PictureBox());
			this->lblPause = (gcnew System::Windows::Forms::Label());
			this->lblTfact = (gcnew System::Windows::Forms::Label());
			this->picPause = (gcnew System::Windows::Forms::PictureBox());
			this->gbExtraParam = (gcnew System::Windows::Forms::GroupBox());
			this->lblTaskKeyInterrupt = (gcnew System::Windows::Forms::Label());
			this->lblTextKeyInterrupt = (gcnew System::Windows::Forms::Label());
			this->lblTaskPauseContinue = (gcnew System::Windows::Forms::Label());
			this->lblTaskView = (gcnew System::Windows::Forms::Label());
			this->lblTextPauseContinue = (gcnew System::Windows::Forms::Label());
			this->lblTextView = (gcnew System::Windows::Forms::Label());
			this->lblTaskIsEditable = (gcnew System::Windows::Forms::Label());
			this->lblTask�alculation = (gcnew System::Windows::Forms::Label());
			this->lblTextIsEditable = (gcnew System::Windows::Forms::Label());
			this->lblText�alculation = (gcnew System::Windows::Forms::Label());
			this->lblTaskInterrupt = (gcnew System::Windows::Forms::Label());
			this->lblTextInterrupt = (gcnew System::Windows::Forms::Label());
			this->gbCallDelay = (gcnew System::Windows::Forms::GroupBox());
			this->lblCallDelay = (gcnew System::Windows::Forms::Label());
			this->gbExcessSession = (gcnew System::Windows::Forms::GroupBox());
			this->lblExcessSession = (gcnew System::Windows::Forms::Label());
			this->lblStopWatch = (gcnew System::Windows::Forms::Label());
			this->TimerSystem = (gcnew System::Windows::Forms::Timer(this->components));
			this->lblParams = (gcnew System::Windows::Forms::Label());
			this->dgrdParams = (gcnew System::Windows::Forms::DataGridView());
			this->clmnParamNames = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->clmnTask1 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->clmnTask2 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->clmnTask3 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->lblLog = (gcnew System::Windows::Forms::Label());
			this->LogText = (gcnew System::Windows::Forms::RichTextBox());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->chrtMain))->BeginInit();
			this->StatusStrip->SuspendLayout();
			this->gbMarkers->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->picTplan))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->picEdit))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->picTfact))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->picAbort))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->picPause))->BeginInit();
			this->gbExtraParam->SuspendLayout();
			this->gbCallDelay->SuspendLayout();
			this->gbExcessSession->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dgrdParams))->BeginInit();
			this->SuspendLayout();
			// 
			// chrtMain
			// 
			this->chrtMain->BorderSkin->BackColor = System::Drawing::Color::Silver;
			this->chrtMain->BorderSkin->PageColor = System::Drawing::SystemColors::Control;
			this->chrtMain->BorderSkin->SkinStyle = System::Windows::Forms::DataVisualization::Charting::BorderSkinStyle::Sunken;
			chartArea1->AxisX->Interval = 2;
			chartArea1->AxisX->IsLabelAutoFit = false;
			chartArea1->AxisX->MajorGrid->Interval = 2;
			chartArea1->AxisX->MajorTickMark->Interval = 2;
			chartArea1->AxisX->MajorTickMark->Size = 2;
			chartArea1->AxisX->Maximum = 98;
			chartArea1->AxisX->Minimum = 0;
			chartArea1->AxisX->MinorGrid->Enabled = true;
			chartArea1->AxisX->MinorGrid->Interval = 1;
			chartArea1->AxisX->MinorGrid->LineDashStyle = System::Windows::Forms::DataVisualization::Charting::ChartDashStyle::Dash;
			chartArea1->AxisX->ScrollBar->Size = 15;
			chartArea1->AxisY->Interval = 0.5;
			chartArea1->AxisY->MajorGrid->Enabled = false;
			chartArea1->AxisY->Maximum = 1.5;
			chartArea1->AxisY->Minimum = 0;
			chartArea1->AxisY->Title = L"f(t) = |cos(t)|";
			chartArea1->AxisY->TitleFont = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Bold));
			chartArea1->Name = L"DefFunctionArea";
			chartArea1->Position->Auto = false;
			chartArea1->Position->Height = 26;
			chartArea1->Position->Width = 100;
			chartArea1->Position->Y = 3;
			chartArea2->AxisX->Interval = 2;
			chartArea2->AxisX->IsLabelAutoFit = false;
			chartArea2->AxisX->MajorGrid->Interval = 2;
			chartArea2->AxisX->Maximum = 98;
			chartArea2->AxisX->Minimum = 0;
			chartArea2->AxisX->MinorGrid->Enabled = true;
			chartArea2->AxisX->MinorGrid->Interval = 1;
			chartArea2->AxisX->MinorGrid->LineDashStyle = System::Windows::Forms::DataVisualization::Charting::ChartDashStyle::Dot;
			chartArea2->AxisY->Interval = 1;
			chartArea2->AxisY->MajorGrid->Enabled = false;
			chartArea2->AxisY->Maximum = 1.99;
			chartArea2->AxisY->Minimum = 0;
			chartArea2->AxisY->Title = L"������ �1";
			chartArea2->AxisY->TitleFont = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Bold));
			chartArea2->Name = L"TaskOneArea";
			chartArea2->Position->Auto = false;
			chartArea2->Position->Height = 20;
			chartArea2->Position->Width = 100;
			chartArea2->Position->Y = 34;
			chartArea3->AxisX->Interval = 2;
			chartArea3->AxisX->IsLabelAutoFit = false;
			chartArea3->AxisX->MajorGrid->Interval = 2;
			chartArea3->AxisX->Maximum = 98;
			chartArea3->AxisX->Minimum = 0;
			chartArea3->AxisX->MinorGrid->Enabled = true;
			chartArea3->AxisX->MinorGrid->Interval = 1;
			chartArea3->AxisX->MinorGrid->LineDashStyle = System::Windows::Forms::DataVisualization::Charting::ChartDashStyle::Dot;
			chartArea3->AxisY->Interval = 1;
			chartArea3->AxisY->MajorGrid->Enabled = false;
			chartArea3->AxisY->Maximum = 1.99;
			chartArea3->AxisY->Minimum = 0;
			chartArea3->AxisY->Title = L"������ �2";
			chartArea3->AxisY->TitleFont = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Bold));
			chartArea3->Name = L"TaskTwoArea";
			chartArea3->Position->Auto = false;
			chartArea3->Position->Height = 20;
			chartArea3->Position->Width = 100;
			chartArea3->Position->Y = 57;
			chartArea4->AxisX->Interval = 2;
			chartArea4->AxisX->IsLabelAutoFit = false;
			chartArea4->AxisX->MajorGrid->Interval = 2;
			chartArea4->AxisX->Maximum = 98;
			chartArea4->AxisX->Minimum = 0;
			chartArea4->AxisX->MinorGrid->Enabled = true;
			chartArea4->AxisX->MinorGrid->Interval = 1;
			chartArea4->AxisX->MinorGrid->LineDashStyle = System::Windows::Forms::DataVisualization::Charting::ChartDashStyle::Dot;
			chartArea4->AxisY->Interval = 1;
			chartArea4->AxisY->MajorGrid->Enabled = false;
			chartArea4->AxisY->Maximum = 1.99;
			chartArea4->AxisY->Minimum = 0;
			chartArea4->AxisY->Title = L"������ �3";
			chartArea4->AxisY->TitleFont = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Bold));
			chartArea4->Name = L"TaskThreeArea";
			chartArea4->Position->Auto = false;
			chartArea4->Position->Height = 20;
			chartArea4->Position->Width = 100;
			chartArea4->Position->Y = 80;
			this->chrtMain->ChartAreas->Add(chartArea1);
			this->chrtMain->ChartAreas->Add(chartArea2);
			this->chrtMain->ChartAreas->Add(chartArea3);
			this->chrtMain->ChartAreas->Add(chartArea4);
			this->chrtMain->Location = System::Drawing::Point(13, 238);
			this->chrtMain->Name = L"chrtMain";
			series1->BorderWidth = 2;
			series1->ChartArea = L"DefFunctionArea";
			series1->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::Spline;
			series1->MarkerSize = 15;
			series1->Name = L"f(t) = |cos(t)|";
			series1->ShadowOffset = 1;
			series1->XValueType = System::Windows::Forms::DataVisualization::Charting::ChartValueType::UInt32;
			series2->BorderColor = System::Drawing::Color::Blue;
			series2->ChartArea = L"TaskOneArea";
			series2->Color = System::Drawing::Color::Black;
			series2->CustomProperties = L"PointWidth=1";
			series2->MarkerSize = 15;
			series2->Name = L"������ �1";
			series2->XValueType = System::Windows::Forms::DataVisualization::Charting::ChartValueType::UInt32;
			series3->BorderColor = System::Drawing::Color::Blue;
			series3->ChartArea = L"TaskTwoArea";
			series3->Color = System::Drawing::Color::Black;
			series3->CustomProperties = L"PointWidth=1";
			series3->MarkerSize = 15;
			series3->Name = L"������ �2";
			series3->XValueType = System::Windows::Forms::DataVisualization::Charting::ChartValueType::UInt32;
			series4->BackSecondaryColor = System::Drawing::Color::White;
			series4->BorderColor = System::Drawing::Color::Blue;
			series4->ChartArea = L"TaskThreeArea";
			series4->Color = System::Drawing::Color::Black;
			series4->CustomProperties = L"PointWidth=1";
			series4->MarkerSize = 15;
			series4->Name = L"������ �3";
			series4->XValueType = System::Windows::Forms::DataVisualization::Charting::ChartValueType::UInt32;
			series5->BorderWidth = 2;
			series5->ChartArea = L"DefFunctionArea";
			series5->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::StepLine;
			series5->Color = System::Drawing::Color::DarkMagenta;
			series5->Name = L"������ �����������";
			this->chrtMain->Series->Add(series1);
			this->chrtMain->Series->Add(series2);
			this->chrtMain->Series->Add(series3);
			this->chrtMain->Series->Add(series4);
			this->chrtMain->Series->Add(series5);
			this->chrtMain->Size = System::Drawing::Size(1240, 528);
			this->chrtMain->TabIndex = 0;
			this->chrtMain->Text = L"�������";
			title1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Bold));
			title1->Name = L"titleDefFunc";
			title1->Position->Auto = false;
			title1->Position->Height = 3;
			title1->Position->Width = 94;
			title1->Position->X = 3;
			title1->Position->Y = 1;
			title1->Text = L"�������� �������";
			title2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Bold));
			title2->Name = L"titleTasks";
			title2->Position->Auto = false;
			title2->Position->Height = 3;
			title2->Position->Width = 94;
			title2->Position->X = 3;
			title2->Position->Y = 29.5F;
			title2->Text = L"������������ ������";
			title3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8, System::Drawing::FontStyle::Italic));
			title3->Name = L"titleTimeTask1";
			title3->Position->Auto = false;
			title3->Position->Height = 14;
			title3->Position->Width = 1;
			title3->Position->X = 2.2F;
			title3->Position->Y = 34.8F;
			title3->Text = L"(�� �������)";
			title3->TextOrientation = System::Windows::Forms::DataVisualization::Charting::TextOrientation::Rotated270;
			title4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8, System::Drawing::FontStyle::Italic));
			title4->Name = L"titleTimeTask2";
			title4->Position->Auto = false;
			title4->Position->Height = 14;
			title4->Position->Width = 1;
			title4->Position->X = 2.2F;
			title4->Position->Y = 57.6F;
			title4->Text = L"(�� �������)";
			title4->TextOrientation = System::Windows::Forms::DataVisualization::Charting::TextOrientation::Rotated270;
			title5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8, System::Drawing::FontStyle::Italic));
			title5->Name = L"titleIntTask3";
			title5->Position->Auto = false;
			title5->Position->Height = 18;
			title5->Position->Width = 1;
			title5->Position->X = 2.2F;
			title5->Position->Y = 79;
			title5->Text = L"(�� ����������)";
			title5->TextOrientation = System::Windows::Forms::DataVisualization::Charting::TextOrientation::Rotated270;
			this->chrtMain->Titles->Add(title1);
			this->chrtMain->Titles->Add(title2);
			this->chrtMain->Titles->Add(title3);
			this->chrtMain->Titles->Add(title4);
			this->chrtMain->Titles->Add(title5);
			this->chrtMain->KeyDown += gcnew System::Windows::Forms::KeyEventHandler(this, &frmMain::chrtMain_KeyDown);
			this->chrtMain->MouseDoubleClick += gcnew System::Windows::Forms::MouseEventHandler(this, &frmMain::chrtMain_MouseDoubleClick);
			this->chrtMain->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &frmMain::chrtMain_MouseDown);
			// 
			// btnExitMain
			// 
			this->btnExitMain->Location = System::Drawing::Point(1168, 26);
			this->btnExitMain->Name = L"btnExitMain";
			this->btnExitMain->Size = System::Drawing::Size(75, 24);
			this->btnExitMain->TabIndex = 2;
			this->btnExitMain->Tag = L"";
			this->btnExitMain->Text = L"�����";
			this->ToolTipMain->SetToolTip(this->btnExitMain, L"����� �� ����� �������������");
			this->btnExitMain->UseVisualStyleBackColor = true;
			this->btnExitMain->Click += gcnew System::EventHandler(this, &frmMain::btnExitMain_Click);
			// 
			// StatusStrip
			// 
			this->StatusStrip->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->StatusBarStatus, 
				this->StatusBarTime});
			this->StatusStrip->Location = System::Drawing::Point(0, 775);
			this->StatusStrip->Name = L"StatusStrip";
			this->StatusStrip->Size = System::Drawing::Size(1260, 25);
			this->StatusStrip->TabIndex = 3;
			// 
			// StatusBarStatus
			// 
			this->StatusBarStatus->AutoSize = false;
			this->StatusBarStatus->BorderSides = System::Windows::Forms::ToolStripStatusLabelBorderSides::Right;
			this->StatusBarStatus->BorderStyle = System::Windows::Forms::Border3DStyle::RaisedOuter;
			this->StatusBarStatus->Name = L"StatusBarStatus";
			this->StatusBarStatus->Size = System::Drawing::Size(1165, 20);
			this->StatusBarStatus->Spring = true;
			this->StatusBarStatus->Text = L"������� ��������...";
			this->StatusBarStatus->TextAlign = System::Drawing::ContentAlignment::MiddleLeft;
			// 
			// StatusBarTime
			// 
			this->StatusBarTime->ActiveLinkColor = System::Drawing::Color::Red;
			this->StatusBarTime->AutoSize = false;
			this->StatusBarTime->BorderStyle = System::Windows::Forms::Border3DStyle::RaisedOuter;
			this->StatusBarTime->Font = (gcnew System::Drawing::Font(L"Bookman Old Style", 9, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->StatusBarTime->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"StatusBarTime.Image")));
			this->StatusBarTime->ImageAlign = System::Drawing::ContentAlignment::MiddleLeft;
			this->StatusBarTime->Name = L"StatusBarTime";
			this->StatusBarTime->Size = System::Drawing::Size(80, 20);
			this->StatusBarTime->TextAlign = System::Drawing::ContentAlignment::MiddleRight;
			this->StatusBarTime->ToolTipText = L"��������� �����";
			// 
			// TimerStopWatch
			// 
			this->TimerStopWatch->Enabled = true;
			this->TimerStopWatch->Tick += gcnew System::EventHandler(this, &frmMain::TimerStopWatch_Tick);
			// 
			// btnPlayMain
			// 
			this->btnPlayMain->Enabled = false;
			this->btnPlayMain->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"btnPlayMain.Image")));
			this->btnPlayMain->Location = System::Drawing::Point(9, 12);
			this->btnPlayMain->Name = L"btnPlayMain";
			this->btnPlayMain->Size = System::Drawing::Size(42, 24);
			this->btnPlayMain->TabIndex = 4;
			this->ToolTipMain->SetToolTip(this->btnPlayMain, L"��������� �������������");
			this->btnPlayMain->UseVisualStyleBackColor = true;
			this->btnPlayMain->Click += gcnew System::EventHandler(this, &frmMain::btnPlayMain_Click);
			// 
			// btnPauseMain
			// 
			this->btnPauseMain->Enabled = false;
			this->btnPauseMain->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"btnPauseMain.Image")));
			this->btnPauseMain->Location = System::Drawing::Point(57, 12);
			this->btnPauseMain->Name = L"btnPauseMain";
			this->btnPauseMain->Size = System::Drawing::Size(42, 24);
			this->btnPauseMain->TabIndex = 5;
			this->ToolTipMain->SetToolTip(this->btnPauseMain, L"������������� �������������");
			this->btnPauseMain->UseVisualStyleBackColor = true;
			this->btnPauseMain->Click += gcnew System::EventHandler(this, &frmMain::btnPauseMain_Click);
			// 
			// btnProceedMain
			// 
			this->btnProceedMain->Enabled = false;
			this->btnProceedMain->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"btnProceedMain.Image")));
			this->btnProceedMain->Location = System::Drawing::Point(105, 12);
			this->btnProceedMain->Name = L"btnProceedMain";
			this->btnProceedMain->Size = System::Drawing::Size(42, 24);
			this->btnProceedMain->TabIndex = 6;
			this->ToolTipMain->SetToolTip(this->btnProceedMain, L"����������� �������������");
			this->btnProceedMain->UseVisualStyleBackColor = true;
			this->btnProceedMain->Click += gcnew System::EventHandler(this, &frmMain::btnProceedMain_Click);
			// 
			// btnStopMain
			// 
			this->btnStopMain->Enabled = false;
			this->btnStopMain->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"btnStopMain.Image")));
			this->btnStopMain->Location = System::Drawing::Point(153, 12);
			this->btnStopMain->Name = L"btnStopMain";
			this->btnStopMain->Size = System::Drawing::Size(42, 24);
			this->btnStopMain->TabIndex = 7;
			this->ToolTipMain->SetToolTip(this->btnStopMain, L"��������� �������������");
			this->btnStopMain->UseVisualStyleBackColor = true;
			this->btnStopMain->Click += gcnew System::EventHandler(this, &frmMain::btnStopMain_Click);
			// 
			// ToolTipMain
			// 
			this->ToolTipMain->IsBalloon = true;
			// 
			// btnEditMain
			// 
			this->btnEditMain->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"btnEditMain.Image")));
			this->btnEditMain->Location = System::Drawing::Point(353, 12);
			this->btnEditMain->Name = L"btnEditMain";
			this->btnEditMain->Size = System::Drawing::Size(42, 24);
			this->btnEditMain->TabIndex = 8;
			this->ToolTipMain->SetToolTip(this->btnEditMain, L"�������� ������� ���������");
			this->btnEditMain->UseVisualStyleBackColor = true;
			this->btnEditMain->Click += gcnew System::EventHandler(this, &frmMain::btnEditMain_Click);
			// 
			// btnTZ
			// 
			this->btnTZ->Location = System::Drawing::Point(401, 12);
			this->btnTZ->Name = L"btnTZ";
			this->btnTZ->Size = System::Drawing::Size(42, 24);
			this->btnTZ->TabIndex = 10;
			this->btnTZ->Text = L"��";
			this->ToolTipMain->SetToolTip(this->btnTZ, L"������� ����������� �������");
			this->btnTZ->UseVisualStyleBackColor = true;
			this->btnTZ->Click += gcnew System::EventHandler(this, &frmMain::btnTZ_Click);
			// 
			// btnOpenXlsFile
			// 
			this->btnOpenXlsFile->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"btnOpenXlsFile.Image")));
			this->btnOpenXlsFile->Location = System::Drawing::Point(257, 12);
			this->btnOpenXlsFile->Name = L"btnOpenXlsFile";
			this->btnOpenXlsFile->Size = System::Drawing::Size(42, 24);
			this->btnOpenXlsFile->TabIndex = 13;
			this->ToolTipMain->SetToolTip(this->btnOpenXlsFile, L"��������� ��������� �� Excel-�����");
			this->btnOpenXlsFile->UseVisualStyleBackColor = true;
			this->btnOpenXlsFile->Click += gcnew System::EventHandler(this, &frmMain::OpenXlsFile_Click);
			// 
			// btnSaveXlsFile
			// 
			this->btnSaveXlsFile->Enabled = false;
			this->btnSaveXlsFile->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"btnSaveXlsFile.Image")));
			this->btnSaveXlsFile->Location = System::Drawing::Point(305, 12);
			this->btnSaveXlsFile->Name = L"btnSaveXlsFile";
			this->btnSaveXlsFile->Size = System::Drawing::Size(42, 24);
			this->btnSaveXlsFile->TabIndex = 14;
			this->ToolTipMain->SetToolTip(this->btnSaveXlsFile, L"��������� ��������� �� Excel-����");
			this->btnSaveXlsFile->UseVisualStyleBackColor = true;
			this->btnSaveXlsFile->Click += gcnew System::EventHandler(this, &frmMain::SaveXlsFile_Click);
			// 
			// gbMarkers
			// 
			this->gbMarkers->Controls->Add(this->lblTplan);
			this->gbMarkers->Controls->Add(this->lblEdit);
			this->gbMarkers->Controls->Add(this->picTplan);
			this->gbMarkers->Controls->Add(this->picEdit);
			this->gbMarkers->Controls->Add(this->lblAbort);
			this->gbMarkers->Controls->Add(this->picTfact);
			this->gbMarkers->Controls->Add(this->picAbort);
			this->gbMarkers->Controls->Add(this->lblPause);
			this->gbMarkers->Controls->Add(this->lblTfact);
			this->gbMarkers->Controls->Add(this->picPause);
			this->gbMarkers->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->gbMarkers->Location = System::Drawing::Point(444, 51);
			this->gbMarkers->Name = L"gbMarkers";
			this->gbMarkers->Size = System::Drawing::Size(138, 181);
			this->gbMarkers->TabIndex = 26;
			this->gbMarkers->TabStop = false;
			this->gbMarkers->Text = L"�������";
			this->ToolTipMain->SetToolTip(this->gbMarkers, L"�������� ����������� ����� �� ��������");
			// 
			// lblTplan
			// 
			this->lblTplan->AutoSize = true;
			this->lblTplan->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->lblTplan->Location = System::Drawing::Point(24, 19);
			this->lblTplan->Name = L"lblTplan";
			this->lblTplan->Size = System::Drawing::Size(49, 15);
			this->lblTplan->TabIndex = 21;
			this->lblTplan->Text = L"- �����";
			// 
			// lblEdit
			// 
			this->lblEdit->AutoSize = true;
			this->lblEdit->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->lblEdit->Location = System::Drawing::Point(24, 91);
			this->lblEdit->Name = L"lblEdit";
			this->lblEdit->Size = System::Drawing::Size(112, 15);
			this->lblEdit->TabIndex = 25;
			this->lblEdit->Text = L"- ��������������";
			// 
			// picTplan
			// 
			this->picTplan->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"picTplan.Image")));
			this->picTplan->Location = System::Drawing::Point(6, 22);
			this->picTplan->Name = L"picTplan";
			this->picTplan->Size = System::Drawing::Size(12, 12);
			this->picTplan->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->picTplan->TabIndex = 16;
			this->picTplan->TabStop = false;
			// 
			// picEdit
			// 
			this->picEdit->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"picEdit.Image")));
			this->picEdit->Location = System::Drawing::Point(6, 94);
			this->picEdit->Name = L"picEdit";
			this->picEdit->Size = System::Drawing::Size(12, 12);
			this->picEdit->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->picEdit->TabIndex = 18;
			this->picEdit->TabStop = false;
			// 
			// lblAbort
			// 
			this->lblAbort->AutoSize = true;
			this->lblAbort->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->lblAbort->Location = System::Drawing::Point(24, 73);
			this->lblAbort->Name = L"lblAbort";
			this->lblAbort->Size = System::Drawing::Size(87, 15);
			this->lblAbort->TabIndex = 24;
			this->lblAbort->Text = L"- ����������";
			// 
			// picTfact
			// 
			this->picTfact->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"picTfact.Image")));
			this->picTfact->Location = System::Drawing::Point(6, 40);
			this->picTfact->Name = L"picTfact";
			this->picTfact->Size = System::Drawing::Size(12, 12);
			this->picTfact->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->picTfact->TabIndex = 17;
			this->picTfact->TabStop = false;
			// 
			// picAbort
			// 
			this->picAbort->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"picAbort.Image")));
			this->picAbort->Location = System::Drawing::Point(6, 76);
			this->picAbort->Name = L"picAbort";
			this->picAbort->Size = System::Drawing::Size(12, 12);
			this->picAbort->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->picAbort->TabIndex = 20;
			this->picAbort->TabStop = false;
			// 
			// lblPause
			// 
			this->lblPause->AutoSize = true;
			this->lblPause->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->lblPause->Location = System::Drawing::Point(24, 55);
			this->lblPause->Name = L"lblPause";
			this->lblPause->Size = System::Drawing::Size(48, 15);
			this->lblPause->TabIndex = 23;
			this->lblPause->Text = L"- �����";
			// 
			// lblTfact
			// 
			this->lblTfact->AutoSize = true;
			this->lblTfact->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->lblTfact->Location = System::Drawing::Point(24, 37);
			this->lblTfact->Name = L"lblTfact";
			this->lblTfact->Size = System::Drawing::Size(52, 15);
			this->lblTfact->TabIndex = 22;
			this->lblTfact->Text = L"- �����";
			// 
			// picPause
			// 
			this->picPause->Image = (cli::safe_cast<System::Drawing::Image^  >(resources->GetObject(L"picPause.Image")));
			this->picPause->Location = System::Drawing::Point(6, 58);
			this->picPause->Name = L"picPause";
			this->picPause->Size = System::Drawing::Size(12, 12);
			this->picPause->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->picPause->TabIndex = 19;
			this->picPause->TabStop = false;
			// 
			// gbExtraParam
			// 
			this->gbExtraParam->Controls->Add(this->lblTaskKeyInterrupt);
			this->gbExtraParam->Controls->Add(this->lblTextKeyInterrupt);
			this->gbExtraParam->Controls->Add(this->lblTaskPauseContinue);
			this->gbExtraParam->Controls->Add(this->lblTaskView);
			this->gbExtraParam->Controls->Add(this->lblTextPauseContinue);
			this->gbExtraParam->Controls->Add(this->lblTextView);
			this->gbExtraParam->Controls->Add(this->lblTaskIsEditable);
			this->gbExtraParam->Controls->Add(this->lblTask�alculation);
			this->gbExtraParam->Controls->Add(this->lblTextIsEditable);
			this->gbExtraParam->Controls->Add(this->lblText�alculation);
			this->gbExtraParam->Controls->Add(this->lblTaskInterrupt);
			this->gbExtraParam->Controls->Add(this->lblTextInterrupt);
			this->gbExtraParam->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Underline)), 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			this->gbExtraParam->Location = System::Drawing::Point(588, 141);
			this->gbExtraParam->Name = L"gbExtraParam";
			this->gbExtraParam->Size = System::Drawing::Size(323, 91);
			this->gbExtraParam->TabIndex = 27;
			this->gbExtraParam->TabStop = false;
			this->gbExtraParam->Text = L"�������������� ���������";
			this->ToolTipMain->SetToolTip(this->gbExtraParam, L"�������������� ��������� �������������");
			// 
			// lblTaskKeyInterrupt
			// 
			this->lblTaskKeyInterrupt->AutoSize = true;
			this->lblTaskKeyInterrupt->Font = (gcnew System::Drawing::Font(L"Segoe UI Semibold", 9, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->lblTaskKeyInterrupt->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), 
				static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(192)));
			this->lblTaskKeyInterrupt->Location = System::Drawing::Point(262, 34);
			this->lblTaskKeyInterrupt->Name = L"lblTaskKeyInterrupt";
			this->lblTaskKeyInterrupt->Size = System::Drawing::Size(14, 15);
			this->lblTaskKeyInterrupt->TabIndex = 1;
			this->lblTaskKeyInterrupt->Text = L"K";
			// 
			// lblTextKeyInterrupt
			// 
			this->lblTextKeyInterrupt->AutoSize = true;
			this->lblTextKeyInterrupt->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Underline, 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			this->lblTextKeyInterrupt->Location = System::Drawing::Point(178, 33);
			this->lblTextKeyInterrupt->Name = L"lblTextKeyInterrupt";
			this->lblTextKeyInterrupt->Size = System::Drawing::Size(84, 15);
			this->lblTextKeyInterrupt->TabIndex = 2;
			this->lblTextKeyInterrupt->Text = L"����������:";
			// 
			// lblTaskPauseContinue
			// 
			this->lblTaskPauseContinue->AutoSize = true;
			this->lblTaskPauseContinue->Font = (gcnew System::Drawing::Font(L"Segoe UI Semibold", 9, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->lblTaskPauseContinue->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), 
				static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(192)));
			this->lblTaskPauseContinue->Location = System::Drawing::Point(179, 65);
			this->lblTaskPauseContinue->Name = L"lblTaskPauseContinue";
			this->lblTaskPauseContinue->Size = System::Drawing::Size(124, 15);
			this->lblTaskPauseContinue->TabIndex = 0;
			this->lblTaskPauseContinue->Text = L"�2 - 2��(�)/2��(�)";
			// 
			// lblTaskView
			// 
			this->lblTaskView->AutoSize = true;
			this->lblTaskView->Font = (gcnew System::Drawing::Font(L"Segoe UI Semibold", 9, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->lblTaskView->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(0)), 
				static_cast<System::Int32>(static_cast<System::Byte>(192)));
			this->lblTaskView->Location = System::Drawing::Point(120, 49);
			this->lblTaskView->Name = L"lblTaskView";
			this->lblTaskView->Size = System::Drawing::Size(75, 15);
			this->lblTaskView->TabIndex = 0;
			this->lblTaskView->Text = L"����������";
			// 
			// lblTextPauseContinue
			// 
			this->lblTextPauseContinue->AutoSize = true;
			this->lblTextPauseContinue->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Underline, 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			this->lblTextPauseContinue->Location = System::Drawing::Point(7, 64);
			this->lblTextPauseContinue->Name = L"lblTextPauseContinue";
			this->lblTextPauseContinue->Size = System::Drawing::Size(175, 15);
			this->lblTextPauseContinue->TabIndex = 0;
			this->lblTextPauseContinue->Text = L"����������/�������������:";
			// 
			// lblTextView
			// 
			this->lblTextView->AutoSize = true;
			this->lblTextView->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Underline, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->lblTextView->Location = System::Drawing::Point(7, 49);
			this->lblTextView->Name = L"lblTextView";
			this->lblTextView->Size = System::Drawing::Size(114, 15);
			this->lblTextView->TabIndex = 0;
			this->lblTextView->Text = L"��� �����������:";
			// 
			// lblTaskIsEditable
			// 
			this->lblTaskIsEditable->AutoSize = true;
			this->lblTaskIsEditable->Font = (gcnew System::Drawing::Font(L"Segoe UI Semibold", 9, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->lblTaskIsEditable->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(0)), 
				static_cast<System::Int32>(static_cast<System::Byte>(192)));
			this->lblTaskIsEditable->Location = System::Drawing::Point(298, 18);
			this->lblTaskIsEditable->Name = L"lblTaskIsEditable";
			this->lblTaskIsEditable->Size = System::Drawing::Size(22, 15);
			this->lblTaskIsEditable->TabIndex = 0;
			this->lblTaskIsEditable->Text = L"��";
			// 
			// lblTask�alculation
			// 
			this->lblTask�alculation->AutoSize = true;
			this->lblTask�alculation->Font = (gcnew System::Drawing::Font(L"Segoe UI Semibold", 9, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->lblTask�alculation->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), 
				static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(192)));
			this->lblTask�alculation->Location = System::Drawing::Point(104, 19);
			this->lblTask�alculation->Name = L"lblTask�alculation";
			this->lblTask�alculation->Size = System::Drawing::Size(39, 15);
			this->lblTask�alculation->TabIndex = 0;
			this->lblTask�alculation->Text = L"�����";
			// 
			// lblTextIsEditable
			// 
			this->lblTextIsEditable->AutoSize = true;
			this->lblTextIsEditable->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Underline, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->lblTextIsEditable->Location = System::Drawing::Point(178, 18);
			this->lblTextIsEditable->Name = L"lblTextIsEditable";
			this->lblTextIsEditable->Size = System::Drawing::Size(119, 15);
			this->lblTextIsEditable->TabIndex = 0;
			this->lblTextIsEditable->Text = L"��������� �����.:";
			// 
			// lblText�alculation
			// 
			this->lblText�alculation->AutoSize = true;
			this->lblText�alculation->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Underline, 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			this->lblText�alculation->Location = System::Drawing::Point(7, 19);
			this->lblText�alculation->Name = L"lblText�alculation";
			this->lblText�alculation->Size = System::Drawing::Size(97, 15);
			this->lblText�alculation->TabIndex = 0;
			this->lblText�alculation->Text = L"������ ������:";
			// 
			// lblTaskInterrupt
			// 
			this->lblTaskInterrupt->AutoSize = true;
			this->lblTaskInterrupt->Font = (gcnew System::Drawing::Font(L"Segoe UI Semibold", 9, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->lblTaskInterrupt->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(0)), 
				static_cast<System::Int32>(static_cast<System::Byte>(192)));
			this->lblTaskInterrupt->Location = System::Drawing::Point(89, 34);
			this->lblTaskInterrupt->Name = L"lblTaskInterrupt";
			this->lblTaskInterrupt->Size = System::Drawing::Size(83, 15);
			this->lblTaskInterrupt->TabIndex = 0;
			this->lblTaskInterrupt->Text = L"�����������";
			// 
			// lblTextInterrupt
			// 
			this->lblTextInterrupt->AutoSize = true;
			this->lblTextInterrupt->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Underline, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->lblTextInterrupt->Location = System::Drawing::Point(7, 34);
			this->lblTextInterrupt->Name = L"lblTextInterrupt";
			this->lblTextInterrupt->Size = System::Drawing::Size(84, 15);
			this->lblTextInterrupt->TabIndex = 0;
			this->lblTextInterrupt->Text = L"����������:";
			// 
			// gbCallDelay
			// 
			this->gbCallDelay->Controls->Add(this->lblCallDelay);
			this->gbCallDelay->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Underline)), 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			this->gbCallDelay->Location = System::Drawing::Point(588, 51);
			this->gbCallDelay->Name = L"gbCallDelay";
			this->gbCallDelay->Size = System::Drawing::Size(323, 45);
			this->gbCallDelay->TabIndex = 28;
			this->gbCallDelay->TabStop = false;
			this->gbCallDelay->Text = L"�������� ������";
			this->ToolTipMain->SetToolTip(this->gbCallDelay, L"������� ������� �� �������� ������");
			// 
			// lblCallDelay
			// 
			this->lblCallDelay->AutoSize = true;
			this->lblCallDelay->Font = (gcnew System::Drawing::Font(L"Segoe UI Semibold", 9, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->lblCallDelay->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(0)), 
				static_cast<System::Int32>(static_cast<System::Byte>(192)));
			this->lblCallDelay->Location = System::Drawing::Point(7, 23);
			this->lblCallDelay->Name = L"lblCallDelay";
			this->lblCallDelay->Size = System::Drawing::Size(207, 15);
			this->lblCallDelay->TabIndex = 0;
			this->lblCallDelay->Text = L"����� ������ ������ � ����������.";
			// 
			// gbExcessSession
			// 
			this->gbExcessSession->Controls->Add(this->lblExcessSession);
			this->gbExcessSession->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Underline)), 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			this->gbExcessSession->Location = System::Drawing::Point(588, 96);
			this->gbExcessSession->Name = L"gbExcessSession";
			this->gbExcessSession->Size = System::Drawing::Size(323, 45);
			this->gbExcessSession->TabIndex = 29;
			this->gbExcessSession->TabStop = false;
			this->gbExcessSession->Text = L"���������� ������";
			this->ToolTipMain->SetToolTip(this->gbExcessSession, L"������� ������� �� ���������� ������");
			// 
			// lblExcessSession
			// 
			this->lblExcessSession->AutoSize = true;
			this->lblExcessSession->Font = (gcnew System::Drawing::Font(L"Segoe UI Semibold", 9, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->lblExcessSession->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(0)), 
				static_cast<System::Int32>(static_cast<System::Byte>(192)));
			this->lblExcessSession->Location = System::Drawing::Point(7, 24);
			this->lblExcessSession->Name = L"lblExcessSession";
			this->lblExcessSession->Size = System::Drawing::Size(307, 15);
			this->lblExcessSession->TabIndex = 0;
			this->lblExcessSession->Text = L"����� � ������ ���������� ����� ���������� n = 5.";
			// 
			// lblStopWatch
			// 
			this->lblStopWatch->AutoSize = true;
			this->lblStopWatch->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 16, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->lblStopWatch->ForeColor = System::Drawing::Color::DarkGreen;
			this->lblStopWatch->Location = System::Drawing::Point(602, 9);
			this->lblStopWatch->Name = L"lblStopWatch";
			this->lblStopWatch->Size = System::Drawing::Size(51, 26);
			this->lblStopWatch->TabIndex = 9;
			this->lblStopWatch->Text = L"--:--\r\n";
			// 
			// TimerSystem
			// 
			this->TimerSystem->Interval = 1000;
			this->TimerSystem->Tick += gcnew System::EventHandler(this, &frmMain::TimerSystem_Tick);
			// 
			// lblParams
			// 
			this->lblParams->AutoSize = true;
			this->lblParams->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Underline, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->lblParams->Location = System::Drawing::Point(16, 48);
			this->lblParams->Name = L"lblParams";
			this->lblParams->Size = System::Drawing::Size(147, 17);
			this->lblParams->TabIndex = 11;
			this->lblParams->Text = L"������� ���������:\r\n";
			// 
			// dgrdParams
			// 
			this->dgrdParams->AllowUserToAddRows = false;
			this->dgrdParams->AllowUserToDeleteRows = false;
			this->dgrdParams->AllowUserToResizeColumns = false;
			this->dgrdParams->AllowUserToResizeRows = false;
			this->dgrdParams->ColumnHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::Sunken;
			dataGridViewCellStyle1->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			dataGridViewCellStyle1->BackColor = System::Drawing::SystemColors::Control;
			dataGridViewCellStyle1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			dataGridViewCellStyle1->ForeColor = System::Drawing::SystemColors::WindowText;
			dataGridViewCellStyle1->Format = L"N0";
			dataGridViewCellStyle1->NullValue = L"0";
			dataGridViewCellStyle1->SelectionBackColor = System::Drawing::SystemColors::ControlLight;
			dataGridViewCellStyle1->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle1->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
			this->dgrdParams->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this->dgrdParams->ColumnHeadersHeight = 20;
			this->dgrdParams->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
			this->dgrdParams->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(4) {this->clmnParamNames, 
				this->clmnTask1, this->clmnTask2, this->clmnTask3});
			this->dgrdParams->EditMode = System::Windows::Forms::DataGridViewEditMode::EditOnEnter;
			this->dgrdParams->Enabled = false;
			this->dgrdParams->ImeMode = System::Windows::Forms::ImeMode::NoControl;
			this->dgrdParams->Location = System::Drawing::Point(16, 73);
			this->dgrdParams->MultiSelect = false;
			this->dgrdParams->Name = L"dgrdParams";
			this->dgrdParams->ReadOnly = true;
			dataGridViewCellStyle6->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			dataGridViewCellStyle6->BackColor = System::Drawing::SystemColors::Control;
			dataGridViewCellStyle6->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			dataGridViewCellStyle6->ForeColor = System::Drawing::SystemColors::WindowText;
			dataGridViewCellStyle6->Format = L"N0";
			dataGridViewCellStyle6->NullValue = L"0";
			dataGridViewCellStyle6->SelectionBackColor = System::Drawing::SystemColors::ControlLight;
			dataGridViewCellStyle6->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle6->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
			this->dgrdParams->RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
			this->dgrdParams->RowHeadersVisible = false;
			this->dgrdParams->RowHeadersWidth = 100;
			this->dgrdParams->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::DisableResizing;
			dataGridViewCellStyle7->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			dataGridViewCellStyle7->Format = L"N0";
			dataGridViewCellStyle7->NullValue = nullptr;
			dataGridViewCellStyle7->SelectionBackColor = System::Drawing::SystemColors::ControlLight;
			this->dgrdParams->RowsDefaultCellStyle = dataGridViewCellStyle7;
			this->dgrdParams->RowTemplate->DefaultCellStyle->Format = L"N0";
			this->dgrdParams->RowTemplate->DefaultCellStyle->NullValue = nullptr;
			this->dgrdParams->RowTemplate->DefaultCellStyle->SelectionForeColor = System::Drawing::Color::Black;
			this->dgrdParams->RowTemplate->Height = 20;
			this->dgrdParams->ScrollBars = System::Windows::Forms::ScrollBars::None;
			this->dgrdParams->SelectionMode = System::Windows::Forms::DataGridViewSelectionMode::ColumnHeaderSelect;
			this->dgrdParams->Size = System::Drawing::Size(422, 160);
			this->dgrdParams->StandardTab = true;
			this->dgrdParams->TabIndex = 12;
			this->dgrdParams->CellValidating += gcnew System::Windows::Forms::DataGridViewCellValidatingEventHandler(this, &frmMain::dgrdParams_CellValidating);
			this->dgrdParams->Leave += gcnew System::EventHandler(this, &frmMain::dgrdParams_Leave);
			// 
			// clmnParamNames
			// 
			dataGridViewCellStyle2->NullValue = nullptr;
			this->clmnParamNames->DefaultCellStyle = dataGridViewCellStyle2;
			this->clmnParamNames->HeaderText = L"���������\\������";
			this->clmnParamNames->Name = L"clmnParamNames";
			this->clmnParamNames->ReadOnly = true;
			this->clmnParamNames->Resizable = System::Windows::Forms::DataGridViewTriState::False;
			this->clmnParamNames->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
			this->clmnParamNames->Width = 120;
			// 
			// clmnTask1
			// 
			dataGridViewCellStyle3->Format = L"N0";
			dataGridViewCellStyle3->NullValue = nullptr;
			this->clmnTask1->DefaultCellStyle = dataGridViewCellStyle3;
			this->clmnTask1->HeaderText = L"������ �1";
			this->clmnTask1->MaxInputLength = 2;
			this->clmnTask1->Name = L"clmnTask1";
			this->clmnTask1->ReadOnly = true;
			this->clmnTask1->Resizable = System::Windows::Forms::DataGridViewTriState::False;
			this->clmnTask1->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
			this->clmnTask1->ToolTipText = L"������ �1 (���������)";
			// 
			// clmnTask2
			// 
			dataGridViewCellStyle4->Format = L"N0";
			dataGridViewCellStyle4->NullValue = nullptr;
			this->clmnTask2->DefaultCellStyle = dataGridViewCellStyle4;
			this->clmnTask2->HeaderText = L"������ �2";
			this->clmnTask2->MaxInputLength = 2;
			this->clmnTask2->Name = L"clmnTask2";
			this->clmnTask2->ReadOnly = true;
			this->clmnTask2->Resizable = System::Windows::Forms::DataGridViewTriState::False;
			this->clmnTask2->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
			this->clmnTask2->ToolTipText = L"������ �2 (�� ����������)";
			// 
			// clmnTask3
			// 
			dataGridViewCellStyle5->Format = L"N0";
			dataGridViewCellStyle5->NullValue = nullptr;
			this->clmnTask3->DefaultCellStyle = dataGridViewCellStyle5;
			this->clmnTask3->HeaderText = L"������ �3";
			this->clmnTask3->MaxInputLength = 2;
			this->clmnTask3->Name = L"clmnTask3";
			this->clmnTask3->ReadOnly = true;
			this->clmnTask3->Resizable = System::Windows::Forms::DataGridViewTriState::False;
			this->clmnTask3->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
			this->clmnTask3->ToolTipText = L"������ �3 (���������)";
			// 
			// lblLog
			// 
			this->lblLog->AutoSize = true;
			this->lblLog->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Underline, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->lblLog->Location = System::Drawing::Point(914, 48);
			this->lblLog->Name = L"lblLog";
			this->lblLog->Size = System::Drawing::Size(103, 17);
			this->lblLog->TabIndex = 11;
			this->lblLog->Text = L"��� ��������:";
			// 
			// LogText
			// 
			this->LogText->BackColor = System::Drawing::SystemColors::ControlLightLight;
			this->LogText->Cursor = System::Windows::Forms::Cursors::IBeam;
			this->LogText->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->LogText->Location = System::Drawing::Point(917, 73);
			this->LogText->Name = L"LogText";
			this->LogText->ReadOnly = true;
			this->LogText->ScrollBars = System::Windows::Forms::RichTextBoxScrollBars::Vertical;
			this->LogText->Size = System::Drawing::Size(326, 160);
			this->LogText->TabIndex = 15;
			this->LogText->Text = L"";
			this->LogText->TextChanged += gcnew System::EventHandler(this, &frmMain::LogText_TextChanged);
			// 
			// frmMain
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(96, 96);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Dpi;
			this->AutoScroll = true;
			this->AutoSize = true;
			this->ClientSize = System::Drawing::Size(1260, 800);
			this->Controls->Add(this->gbExcessSession);
			this->Controls->Add(this->gbCallDelay);
			this->Controls->Add(this->gbExtraParam);
			this->Controls->Add(this->gbMarkers);
			this->Controls->Add(this->LogText);
			this->Controls->Add(this->btnSaveXlsFile);
			this->Controls->Add(this->btnOpenXlsFile);
			this->Controls->Add(this->dgrdParams);
			this->Controls->Add(this->lblLog);
			this->Controls->Add(this->lblParams);
			this->Controls->Add(this->btnTZ);
			this->Controls->Add(this->lblStopWatch);
			this->Controls->Add(this->btnEditMain);
			this->Controls->Add(this->btnStopMain);
			this->Controls->Add(this->btnProceedMain);
			this->Controls->Add(this->btnPauseMain);
			this->Controls->Add(this->btnPlayMain);
			this->Controls->Add(this->StatusStrip);
			this->Controls->Add(this->btnExitMain);
			this->Controls->Add(this->chrtMain);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::None;
			this->MinimizeBox = false;
			this->Name = L"frmMain";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"������� �.�. ���-12";
			this->Load += gcnew System::EventHandler(this, &frmMain::frmMain_Load);
			this->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &frmMain::frmMain_MouseDown);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->chrtMain))->EndInit();
			this->StatusStrip->ResumeLayout(false);
			this->StatusStrip->PerformLayout();
			this->gbMarkers->ResumeLayout(false);
			this->gbMarkers->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->picTplan))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->picEdit))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->picTfact))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->picAbort))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->picPause))->EndInit();
			this->gbExtraParam->ResumeLayout(false);
			this->gbExtraParam->PerformLayout();
			this->gbCallDelay->ResumeLayout(false);
			this->gbCallDelay->PerformLayout();
			this->gbExcessSession->ResumeLayout(false);
			this->gbExcessSession->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dgrdParams))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
		


/** Description.h **/
private: void InitTasks()
{
	for(int i=1; i<=N_Tasks; i++)
	{
		for(int j=0; j<dgrdParams->RowCount; j++)
		{
			switch(j)
			{
			case 0: Int32::TryParse(dgrdParams->Rows[j]->Cells[i]->FormattedValue->ToString(), TaskArr[i-1].tend);
				break;
			case 1: Int32::TryParse(dgrdParams->Rows[j]->Cells[i]->FormattedValue->ToString(), TaskArr[i-1].tbeg);
				break;
			case 2: 
				if(i != i_IntTask)
					Int32::TryParse(dgrdParams->Rows[j]->Cells[i]->FormattedValue->ToString(), TaskArr[i-1].tper);
				break;
			case 3: Int32::TryParse(dgrdParams->Rows[j]->Cells[i]->FormattedValue->ToString(), TaskArr[i-1].twait);
				break;
			case 4: Int32::TryParse(dgrdParams->Rows[j]->Cells[i]->FormattedValue->ToString(), TaskArr[i-1].tmax_exec);
				break;
			case 5: Int32::TryParse(dgrdParams->Rows[j]->Cells[i]->FormattedValue->ToString(), TaskArr[i-1].tses);
				break;
			case 6: Int32::TryParse(dgrdParams->Rows[j]->Cells[i]->FormattedValue->ToString(), TaskArr[i-1].prior);
				break;
			}
		}	
		if(i!=i_IntTask)
		{
			TaskArr[i-1].tplan = TaskArr[i-1].tbeg;
			TaskArr[i-1].prev_tplan = TaskArr[i-1].tplan;
			chrtMain->Series[i]->Points->AddXY(TaskArr[i-1].tplan, 0);
			chrtMain->Series[i]->Points[TaskArr[i-1].tplan]->MarkerColor = Color::Blue;
			chrtMain->Series[i]->Points[TaskArr[i-1].tplan]->MarkerStyle = MarkerStyle::Cross;
			LogText->AppendText(DateTime::Now.ToLongTimeString() + " ������ �"+i+": ����� = "+TaskArr[i-1].tplan+"\n");
		}
		else
		{
			chrtMain->Series[i]->Points->AddXY(TaskArr[i-1].tbeg, 0);
			chrtMain->Series[i]->Points[TaskArr[i-1].tbeg]->MarkerColor = Color::Blue;
			chrtMain->Series[i]->Points[TaskArr[i-1].tbeg]->MarkerStyle = MarkerStyle::Triangle;
			chrtMain->Series[i]->Points->AddXY(TaskArr[i-1].tend, 0);
			chrtMain->Series[i]->Points[TaskArr[i-1].tend]->MarkerColor = Color::Blue;
			chrtMain->Series[i]->Points[TaskArr[i-1].tend]->MarkerStyle = MarkerStyle::Triangle;
		}
	}

	Task1Thread = gcnew Thread(gcnew ThreadStart(this, &kurs_srv::frmMain::Tasks1Func));
	Task1Thread->Start();

	Task2Thread = gcnew Thread(gcnew ThreadStart(this, &kurs_srv::frmMain::Tasks2Func));
	Task2Thread->Start();

	Task3Thread = gcnew Thread(gcnew ThreadStart(this, &kurs_srv::frmMain::Tasks3Func));
	Task3Thread->Start();

}
private: void ResetTaskParams()
{
	ActiveTask=-1;
	for(int i=0;i<N_Tasks;i++)
	{
		TaskArr[i].tend = -1;
		TaskArr[i].tbeg = -1;
		TaskArr[i].tper = -1;
		TaskArr[i].twait = -1;
		TaskArr[i].tmax_exec = -1;
		TaskArr[i].tses = -1;
		TaskArr[i].prior = -1;
		TaskArr[i].tcur_exec = 0;
		TaskArr[i].tcur_wait = 0;
		TaskArr[i].Ndelay = 0;
		TaskArr[i].status = NotActive;
	}
}
private: bool Check_Current_Param(int i_row, int i_col, String^ CellStr)
{
	int CellValue=-1;
	if (!Int32::TryParse(CellStr, CellValue))
	{
		ErrorNumber = 19;
		return true;
	}
	else if(CellValue<0)
	{
		ErrorNumber = 1;
		return true;
	}
	else if(CellValue==0)
	{
		if(i_row!=1)
		{
			ErrorNumber = 16;
			return true;
		}
	}
	else if(CellValue>Tmod)
	{
		ErrorNumber = 2;
		return true;
	}
	else
	{
		switch(i_row)
		{
		case 0:
			if(CellValue<tcur && tcur!=-1)
			{
				ErrorNumber = 21;
				return true;
			}
			else if (CellValue > TaskArr[i_col-1].tbeg) 
				TaskArr[i_col-1].tend = CellValue;
			break;

		case 1: 
			if(CellValue<tcur && tcur!=-1)
			{
				ErrorNumber = 21;
				return true;
			}
			else if(CellValue >= TaskArr[i_col-1].tend)
			{
				this->dgrdParams->Rows[i_row]->Cells[i_col]->Value = "";
				dgrdParams->RefreshEdit();
				if(TaskArr[i_col-1].tend == -1)
					ErrorNumber = 3;
				else
				{
					ErrorNumber = 4;
					return true;
				}
			}
			else
				TaskArr[i_col-1].tbeg = CellValue;
			break;

		case 2: 
			if (TaskArr[i_col-1].tend == -1)
			{
				ErrorNumber = 5;
				this->dgrdParams->Rows[i_row]->Cells[i_col]->Value = "";
				dgrdParams->RefreshEdit();
			}
			else if(TaskArr[i_col-1].tbeg == -1)
			{
				ErrorNumber = 6;
				this->dgrdParams->Rows[i_row]->Cells[i_col]->Value = "";
				dgrdParams->RefreshEdit();
			}
			else if(CellValue > TaskArr[i_col-1].tend - TaskArr[i_col-1].tbeg)
			{
				ErrorNumber = 7;
				return true;
			}
			else
				TaskArr[i_col-1].tper = CellValue;
			if(tcur>0)
			{
				chrtMain->Series[i_col]->Points[TaskArr[i_col-1].tplan]->MarkerStyle = MarkerStyle::None;
				TaskArr[i_col-1].tplan = TaskArr[i_col-1].tfact + TaskArr[i_col-1].tper;
				chrtMain->Series[i_col]->Points[TaskArr[i_col-1].tplan]->MarkerColor = Color::Blue;
				chrtMain->Series[i_col]->Points[TaskArr[i_col-1].tplan]->MarkerStyle = MarkerStyle::Cross;
			}
			break;

		case 3:
			if (TaskArr[i_col-1].tper == -1 && i_col!=i_IntTask)
			{
				ErrorNumber = 8;
				this->dgrdParams->Rows[i_row]->Cells[i_col]->Value = "";
				dgrdParams->RefreshEdit();
			}
			else if(CellValue > TaskArr[i_col-1].tper && i_col!=i_IntTask)
			{
				ErrorNumber = 9;
				return true;
			}
			else if(CellValue > TaskArr[i_col-1].tend - TaskArr[i_col-1].tbeg && i_col == i_IntTask)
			{
				ErrorNumber = 20;
				return true;
			}
			else 
				TaskArr[i_col-1].twait = CellValue;
			break;

		case 4:
			if (TaskArr[i_col-1].tper == -1 && i_col != i_IntTask)
			{
				ErrorNumber = 12;
				this->dgrdParams->Rows[i_row]->Cells[i_col]->Value = "";
				dgrdParams->RefreshEdit();
			}
			else if(CellValue > TaskArr[i_col-1].tper && i_col != i_IntTask)
			{
				ErrorNumber = 13;
				return true;
			}
			else if(CellValue > TaskArr[i_col-1].tend - TaskArr[i_col-1].tbeg && i_col == i_IntTask)
			{
				ErrorNumber = 17;
				return true;
			}
			else
				TaskArr[i_col-1].tmax_exec = CellValue;
			break;

		case 5:
			if (TaskArr[i_col-1].tper == -1 && i_col != i_IntTask)
			{
				ErrorNumber = 10;
				this->dgrdParams->Rows[i_row]->Cells[i_col]->Value = "";
				dgrdParams->RefreshEdit();
			}
			else if(CellValue > TaskArr[i_col-1].tper && i_col != i_IntTask)
			{
				ErrorNumber = 11;
				return true;
			}
			else if(CellValue > TaskArr[i_col-1].tend - TaskArr[i_col-1].tbeg && i_col == i_IntTask)
			{
				ErrorNumber = 15;
				return true;
			}
			else if(CellValue > TaskArr[i_col-1].tmax_exec)
			{
				ErrorNumber = 18;
				return true;
			}
			else
				TaskArr[i_col-1].tses = CellValue;
			break;				 

		case 6: 
			for(int i=0;i<N_Tasks;i++)
				if(i == i_IntTask-1 || i == i_col-1 || i_col == i_IntTask)
					continue;
				else if(CellValue == TaskArr[i].prior)
				{
					ErrorNumber = 14;
					return true;
				}
				TaskArr[i_col-1].prior = CellValue;
				break;
		}
	}
	ErrorNumber = 0;
	return false;
}
private: void Final_Check_Param()
{
	if(flDataEditing)
	{
		DlgRes Result = MessageBox::Show("������������ ������� ��������� ��� ������������?", "���� ����������...", MessageBoxButtons::YesNo, MessageBoxIcon::Question);
		if(Result == DlgRes::Yes)
		{
			flDataLoaded = true;
			for(int i_col=1;i_col<dgrdParams->ColumnCount;i_col++)
				for(int i_row=0;i_row<dgrdParams->RowCount;i_row++)
					if(i_col == i_IntTask && i_row == 2)
						continue;
					else if(String::IsNullOrEmpty((String^)dgrdParams->Rows[i_row]->Cells[i_col]->FormattedValue))
					{
						flDataLoaded = false;
						MessageBox::Show("��������� ���� �������� �� ���������. ���������� ������ ��� ���������.", "���� ����������...", MessageBoxButtons::OK, MessageBoxIcon::Warning);
						dgrdParams->Focus();
						dgrdParams->Rows[i_row]->Cells[i_col]->Selected = true;
						return; 
					}
					else
					{
						Check_Current_Param(i_row, i_col, dgrdParams->Rows[i_row]->Cells[i_col]->FormattedValue->ToString());
						if(ErrorNumber)
						{
							ShowErrorMessage(ErrorNumber, i_col);
							flDataLoaded = false;
							dgrdParams->Focus();
							dgrdParams->Rows[i_row]->Cells[i_col]->Selected = true;
							return;
						}
					}
					if(flDataLoaded && !btnProceedMain->Enabled)
					{
						btnSaveXlsFile->Enabled = true;
						btnPlayMain->Enabled = true;				
					}
		}
		dgrdParams->ClearSelection();
		dgrdParams->Enabled = false;
		flDataEditing = false;
	}
}
private: bool ShowErrorMessage(int err_no, int task_no)
{
	DlgRes MessageResult;
		switch(err_no)
		{
		case 1: MessageResult = MessageBox::Show("������� ������������� �����. �������� ������� �� ����� ���� �������������.", "������ �"+task_no.ToString()+". "+"��������������...", MessageBoxButtons::OK, MessageBoxIcon::Warning);
			break;
		case 2: MessageResult = MessageBox::Show("�������� ��������� ����� ��������� �������������.", "������ �"+task_no.ToString()+". "+"��������������...", MessageBoxButtons::OKCancel, MessageBoxIcon::Warning);
			break;
		case 3: MessageResult = MessageBox::Show("�������� �� �� �����. ������� ���������� ������ ����� ��������� ��������� ����������.", "������ �"+task_no.ToString()+". "+"��������������...", MessageBoxButtons::OKCancel, MessageBoxIcon::Warning);
			break;
		case 4: MessageResult = MessageBox::Show("�������� �� ������ ��� ����� ��������� ��. ����� ������ ��������� ���������� ������ ���� ������ ��� ���������.", "������ �"+task_no.ToString()+". "+"��������������...", MessageBoxButtons::OKCancel, MessageBoxIcon::Warning);
			break;
		case 5: MessageResult = MessageBox::Show("�������� �� �� �����. ������� ���������� ������� ����� ��������� ��������� ����������.", "������ �"+task_no.ToString()+". "+"��������������...", MessageBoxButtons::OKCancel, MessageBoxIcon::Warning);
			break;
		case 6: MessageResult = MessageBox::Show("�������� �� �� �����. ������� ���������� ������� ����� ������ ��������� ����������.", "������ �"+task_no.ToString()+". "+"��������������...", MessageBoxButtons::OKCancel, MessageBoxIcon::Warning);
			break;
		case 7: MessageResult = MessageBox::Show("������ ������ ��������� ����������. ���������� �������� �������� �� ��� ��, ����� ������ �������� � �������� ����������.", "������ �"+task_no.ToString()+". "+"��������������...", MessageBoxButtons::OKCancel, MessageBoxIcon::Warning);
			break;
		case 8: MessageResult = MessageBox::Show("������ �� �����. ������� ���������� ������� ����� ���������� ���������� ������.", "������ �"+task_no.ToString()+". "+"��������������...", MessageBoxButtons::OKCancel, MessageBoxIcon::Warning);
			break;
		case 9: MessageResult = MessageBox::Show("����� �������� ������ �������. ������ �� ����� ������� ������ ���������� ������ ��������� ����������.", "������ �"+task_no.ToString()+". "+"��������������...", MessageBoxButtons::OKCancel, MessageBoxIcon::Warning);
			break;
		case 10: MessageResult = MessageBox::Show("������ �� �����. ������� ���������� ������� ����� ���������� ���������� ������.", "������ �"+task_no.ToString()+". "+"��������������...", MessageBoxButtons::OKCancel, MessageBoxIcon::Warning);
			break;
		case 11: MessageResult = MessageBox::Show("����� ����� ������ �������. ������ �� ����� ����������� ������ ������ ��������� ����������.", "������ �"+task_no.ToString()+". "+"��������������...", MessageBoxButtons::OKCancel, MessageBoxIcon::Warning);
			break;
		case 12: MessageResult = MessageBox::Show("������ �� �����. ������� ���������� ������� ����� ���������� ���������� ������.", "������ �"+task_no.ToString()+". "+"��������������...", MessageBoxButtons::OKCancel, MessageBoxIcon::Warning);
			break;
		case 13: MessageResult = MessageBox::Show("����� ������������� ���������� ������ �������. ������ �� ����� ��������� �������� ������ ����������.", "������ �"+task_no.ToString()+". "+"��������������...", MessageBoxButtons::OKCancel, MessageBoxIcon::Warning);
			break;
		case 14: MessageResult = MessageBox::Show("������ � ����� ����������� ��� ����������. ������� ���������� ��������.", "������ �"+task_no.ToString()+". "+"��������������...", MessageBoxButtons::OKCancel, MessageBoxIcon::Warning);
			break;
		case 15: MessageResult = MessageBox::Show("����� ������ ������� �� ������� ��������� ����������. ������� ��������, �� ����������� ����� ���������� ��� �������������.", "������ �"+task_no.ToString()+". "+"��������������...", MessageBoxButtons::OKCancel, MessageBoxIcon::Warning);
			break;
		case 16: MessageResult = MessageBox::Show("������� ������� �������� �������. ��� ����� ��������� ������� ������ ���������� �� ����.", "������ �"+task_no.ToString()+". "+"��������������...", MessageBoxButtons::OKCancel, MessageBoxIcon::Warning);
			break;
		case 17: MessageResult = MessageBox::Show("����� ������������� ���������� ������� �� ������� ��������� ����������. ������� ��������, �� ����������� ����� ���������� ��� �������������.", "������ �"+task_no.ToString()+". "+"��������������...", MessageBoxButtons::OKCancel, MessageBoxIcon::Warning);
			break;
		case 18: MessageResult = MessageBox::Show("����� ���������� ��������� ����������� ��������� ������������ ������. ������� ��������, �� ����������� ����������� ���������.", "������ �"+task_no.ToString()+". "+"��������������...", MessageBoxButtons::OKCancel, MessageBoxIcon::Warning);
			break;
		case 19: MessageResult = MessageBox::Show("������� �������� �������� � ������!", "������ �"+task_no.ToString()+". "+"��������������...", MessageBoxButtons::OKCancel, MessageBoxIcon::Hand); 
			break;
		case 20: MessageResult = MessageBox::Show("����� ������������� �������� ������� �� ������� ��������� ����������. ������� ��������, �� ����������� ����� ���������� ��� �������������.", "������ �"+task_no.ToString()+". "+"��������������...", MessageBoxButtons::OKCancel, MessageBoxIcon::Warning);
			break;
		case 21: MessageResult = MessageBox::Show("����� ��������� �� ����� ���� ������ �������� ������� �������. ������� ��������, �� ����������� ����� �������, ����� = "+tcur+".","������ �"+task_no.ToString()+". "+"��������������...", MessageBoxButtons::OKCancel, MessageBoxIcon::Warning);
			break;
		}
		ErrorNumber = 0;
		if(MessageResult == DlgRes::OK)
			return true;
		return false;
}
private: void DisableCell(DataGridViewCell^ cell) {
	cell->ReadOnly = true;
	cell->Style->BackColor = System::Drawing::Color::LightGray;
	cell->Style->ForeColor = System::Drawing::Color::DarkGray;
	cell->ToolTipText = "�������������� ��������� ���������!";
}
private: System::Void frmMain_Load(System::Object^  sender, System::EventArgs^  e) 
{   
	System::Windows::Forms::Control::CheckForIllegalCrossThreadCalls = false;
	this->StatusBarTime->Text = DateTime::Now.ToLongTimeString();
	this->WindowState = FormWindowState::Maximized;
	this->chrtMain->Width = 0.98*this->Width;
	this->chrtMain->Height = 0.66*this->Height;
	LogText->Width = this->Width-(this->Width - 24 - gbCallDelay->Width);
	LogText->Location = Point(this->Width - LogText->Width - 16, LogText->Location.Y);
	lblLog->Location = Point(LogText->Location.X, lblLog->Location.Y);
	lblStopWatch->Location = Point((this->Width - lblStopWatch->Width)/2, lblStopWatch->Location.Y);
	btnExitMain->Location = Point(this->Width - btnExitMain->Width - 16, btnExitMain->Location.Y);

		
	dgrdParams->Rows->Add("��");
	dgrdParams->Rows[0]->Cells[0]->ToolTipText = "����� ��������� ��������� ����������";
	dgrdParams->Rows->Add("��");
	dgrdParams->Rows[1]->Cells[0]->ToolTipText = "����� ������ ��������� ����������";
	dgrdParams->Rows->Add("�������");
	dgrdParams->Rows[2]->Cells[0]->ToolTipText = "����� ������������(����������) ������";
	dgrdParams->Rows->Add("�������_max");
	dgrdParams->Rows[3]->Cells[0]->ToolTipText = "����� ����������� ���������� ��������";
	dgrdParams->Rows->Add("T���_max");
	dgrdParams->Rows[4]->Cells[0]->ToolTipText = "����� ������������ ����������������� ���������� ������";
	dgrdParams->Rows->Add("��");
	dgrdParams->Rows[5]->Cells[0]->ToolTipText = "����� ����� ���������� ������";
	dgrdParams->Rows->Add("���������");
	dgrdParams->Rows[6]->Cells[0]->ToolTipText = "��������� ������ � �������";
				 
	LogText->AppendText(DateTime::Now.ToLongTimeString() + " ������ ��������� �������������.\n");
			
	DisableCell(this->dgrdParams->Rows[2]->Cells[i_IntTask]);

	dgrdParams->ClearSelection();

	for(int i=0;i<=Tmod;i++)	
	{
		this->chrtMain->Series[0]->Points->AddXY(i, abs(Math::Cos(i*Math::PI/14)));
		this->chrtMain->Series[1]->Points->AddXY(i, 0);
		this->chrtMain->Series[2]->Points->AddXY(i, 0);
		this->chrtMain->Series[3]->Points->AddXY(i, 0);
	}
	ResetTaskParams();		 
}
private: System::Void btnExitMain_Click(System::Object^  sender, System::EventArgs^  e) 
{
	if(Task1Thread)
	Task1Thread->Abort();
	if(Task2Thread)
		Task2Thread->Abort();
	if(Task3Thread)
		Task3Thread->Abort();
	System::Windows::Forms::Control::CheckForIllegalCrossThreadCalls = true;
	Application::Exit();
}
private: System::Void TimerStopWatch_Tick(System::Object^  sender, System::EventArgs^  e) {
			 this->StatusBarTime->Text = DateTime::Now.ToLongTimeString(); 
			 int i;
			 if(TimerSystem->Enabled)
			 {
				 for(i=0;i<N_Tasks;i++)
				 {
					 if((TaskArr[i].prev_tplan==tcur || TaskArr[i].status == Extracted) && TaskArr[i].tplan<=TaskArr[i].tend)
					 {
						 chrtMain->Series[i+1]->Points[TaskArr[i].tplan]->MarkerColor = Color::Blue;
						 chrtMain->Series[i+1]->Points[TaskArr[i].tplan]->MarkerStyle = MarkerStyle::Cross;
					 }

					 if(ActiveTask == i || TaskArr[i].status == Extracted)
					 {
						 if(TaskArr[i].tfact==tcur)
						 {
							 chrtMain->Series[i+1]->Points[TaskArr[i].tfact]->YValues[0] = 1;
							 chrtMain->Series[i+1]->Points[TaskArr[i].tfact]->MarkerColor = Color::Orange;
							 chrtMain->Series[i+1]->Points[TaskArr[i].tfact]->MarkerStyle = MarkerStyle::Cross;
						 }
					 }

					 if(tcur==TaskArr[i].tend)
					 {
						 chrtMain->Series[i+1]->Points->AddXY(TaskArr[i].tend, 0);
						 chrtMain->Series[i+1]->Points[TaskArr[i].tend]->MarkerColor = Color::Red;
						 chrtMain->Series[i+1]->Points[TaskArr[i].tend]->MarkerStyle = MarkerStyle::Diamond;
					 }
				 }
			 }
		 }
private: void ExecutionExcess(int DelayedTask)
{
	TaskArr[DelayedTask].Ndelay++;
	if(TaskArr[DelayedTask].Ndelay == 5)
	{
		TaskArr[DelayedTask].tend = tcur;
		dgrdParams->Rows[0]->Cells[DelayedTask+1]->Value = TaskArr[DelayedTask].tend;
		LogText->AppendText(DateTime::Now.ToLongTimeString() + " ������ �"+(DelayedTask+1)+": �������� ���������� ����_��� = "+TaskArr[DelayedTask].tcur_exec+" ("+tcur+"), N = "+TaskArr[DelayedTask].Ndelay+". ������ ����� � ����������.\n");
		TaskArr[DelayedTask].status = Aborted;
	}
	else
		LogText->AppendText(DateTime::Now.ToLongTimeString() + " ������ �"+(DelayedTask+1)+": �������� ���������� ����_��� = "+TaskArr[DelayedTask].tcur_exec+" ("+tcur+"), N = "+TaskArr[DelayedTask].Ndelay+"\n");
}
private: void Tasks1Func()
{
	int prev_tcur=-1;
	while(true)
	{
		if(prev_tcur != tcur)
		{
			if(ActiveTask == 0)
				chrtMain->Series[i_TimeTask1]->Points->AddXY(tcur, 1);
			prev_tcur = tcur;
		}
		Thread::Sleep(100);
	}
}
private: void Tasks2Func()
{
	int prev_tcur=-1;
	double show_model=0;
	while(true)
	{
		if(prev_tcur != tcur)
		{
			if(ActiveTask == 1)
			{
				chrtMain->Series[i_TimeTask2]->Points->AddXY(tcur, 1);
				show_model = abs(Math::Cos(tcur*Math::PI/14));
				chrtMain->Series[4]->Points->AddXY(tcur, show_model);
			}
			else
				chrtMain->Series[4]->Points->AddXY(tcur, show_model);
			prev_tcur = tcur;
		}
		Thread::Sleep(100);
	}
}
private: void Tasks3Func()
{
	int prev_tcur=-1;
	while(true)
	{
		if(prev_tcur != tcur)
		{
			if(ActiveTask == 2)
				chrtMain->Series[i_IntTask]->Points->AddXY(tcur, 1);

			prev_tcur = tcur;
		}
		Thread::Sleep(100);
	}
}
private: System::Void btnPlayMain_Click(System::Object^  sender, System::EventArgs^  e) {
			 StatusBarStatus->Text = "���������� �������������";
			 LogText->AppendText(DateTime::Now.ToLongTimeString() + " ������������� ��������!\n");
			 InitTasks();
			 lblStopWatch->ForeColor = Color::DarkGreen;
			 lblStopWatch->Text = String::Format("{0:00}:{1:00}", "--", "--");
			 btnPlayMain->Enabled = false;
			 btnPauseMain->Enabled = true;
			 btnProceedMain->Enabled = false;
			 btnStopMain->Enabled = true;
			 flPlayPressed = true;
			 TimerSystem->Enabled = true;
			 chrtMain->Focus();
		 }
private: System::Void btnPauseMain_Click(System::Object^  sender, System::EventArgs^  e) {
			 if(flPlayPressed && !flDataEditing)
			 {
				 chrtMain->Series[0]->Points[tcur]->MarkerColor = Color::Blue;
				 chrtMain->Series[0]->Points[tcur]->MarkerStyle = MarkerStyle::Diamond;
			 }
			 TimerSystem->Enabled = false;
			 flPlayPressed = false;
			 btnPlayMain->Enabled = false;
			 btnPauseMain->Enabled = false;
			 btnProceedMain->Enabled = true;
			 LogText->AppendText(DateTime::Now.ToLongTimeString() + " ������������� �������������� tcur = "+tcur+".\n");
			 StatusBarStatus->Text = "������������� ��������������";
		 }
private: System::Void btnProceedMain_Click(System::Object^  sender, System::EventArgs^  e) {
			 flPlayPressed = true;
			 btnPauseMain->Enabled = true;
			 btnProceedMain->Enabled = false;
			 StatusBarStatus->Text = "���������� �������������";
			 TimerSystem->Enabled = true;
		 }
private: System::Void btnStopMain_Click(System::Object^  sender, System::EventArgs^  e) {
			 TimerSystem->Enabled = false;
			 Task1Thread->Abort();
			 Task2Thread->Abort();
			 Task3Thread->Abort();
			 LogText->Clear();
			 ResetTaskParams();
			 chrtMain->Series[0]->Points->Clear();
			 chrtMain->Series[1]->Points->Clear();
			 chrtMain->Series[2]->Points->Clear();
			 chrtMain->Series[3]->Points->Clear();
			 chrtMain->Series[4]->Points->Clear();
			 for(int i=0;i<=Tmod;i++)	
			 {
				 this->chrtMain->Series[0]->Points->AddXY(i, abs(Math::Cos(i*Math::PI/14)));
				 this->chrtMain->Series[1]->Points->AddXY(i, 0);
				 this->chrtMain->Series[2]->Points->AddXY(i, 0);
				 this->chrtMain->Series[3]->Points->AddXY(i, 0);
			 }
			 flPlayPressed = false;
			 btnPauseMain->Enabled = false;
			 btnProceedMain->Enabled = false;
			 btnStopMain->Enabled = false;
			 btnPlayMain->Enabled = true;
			 flSystemBusy = false;
			 tcur = -1;
			 StatusBarStatus->Text = "������������� �����������";
			 lblStopWatch->Text = String::Format("{0:00}:{1:00}", "--", "--");
			 LogText->AppendText(DateTime::Now.ToLongTimeString() + " ������������� �����������!\n");
		 }
private: System::Void btnTZ_Click(System::Object^  sender, System::EventArgs^  e) {
			 frmTaskTZ^ hfrmTZ=gcnew frmTaskTZ();
			 hfrmTZ->ShowDialog();
		 }
private: System::Void btnEditMain_Click(System::Object^  sender, System::EventArgs^  e) {
			 flDataEditing = true;
			 if(flPlayPressed)
			 {
				 btnPauseMain_Click(btnEditMain,e->Empty);
				 chrtMain->Series[0]->Points[tcur]->MarkerColor = Color::Blue;
				 chrtMain->Series[0]->Points[tcur]->MarkerStyle = MarkerStyle::Star6;
			 }
			 this->dgrdParams->Enabled = true;
			 dgrdParams->ReadOnly = false;
			 this->clmnParamNames->ReadOnly = true;
			 this->dgrdParams->Rows[0]->Cells[1]->Selected = true;			 
			 btnPlayMain->Enabled = false;
			 DisableCell(this->dgrdParams->Rows[2]->Cells[i_IntTask]);
		 }		 
private: System::Void dgrdParams_CellValidating(System::Object^  sender, System::Windows::Forms::DataGridViewCellValidatingEventArgs^  e) {
			 if (e->ColumnIndex >= 1 && e->ColumnIndex < 4 && flDataEditing)
			 {
				 if (String::IsNullOrEmpty(e->FormattedValue->ToString()))
				 {
					 switch(e->RowIndex)
					 {
					 case 0: TaskArr[e->ColumnIndex-1].tend = -1; break;
					 case 1: TaskArr[e->ColumnIndex-1].tbeg = -1; break;
					 case 2: TaskArr[e->ColumnIndex-1].tper = -1; break;
					 case 3: TaskArr[e->ColumnIndex-1].twait = -1; break;
					 case 4: TaskArr[e->ColumnIndex-1].tmax_exec = -1; break;
					 case 5: TaskArr[e->ColumnIndex-1].tses = -1; break;
					 case 6: TaskArr[e->ColumnIndex-1].prior = -1; break;
					 }
					 return;
				 }	 
				 else 
					 Check_Current_Param(e->RowIndex, e->ColumnIndex, e->FormattedValue->ToString());
				 if(ErrorNumber)
					 e->Cancel = ShowErrorMessage(ErrorNumber, e->ColumnIndex);
			 }
		 }
private: System::Void OpenXlsFile_Click(System::Object^  sender, System::EventArgs^  e) 
{
	OpenFileDialog^ OpenFileDlg = gcnew OpenFileDialog;
	OpenFileDlg->Filter = "���� Excel|*.XLSX;*.XLS";
			 
	if(OpenFileDlg->ShowDialog() == DlgRes::OK)
	{
		if(OpenFileDlg->FileName == "")
			MessageBox::Show("���� �� ������. ��� �������� ���������� ���������� ������� ���� � �������� ������.", "��������������...", MessageBoxButtons::OK, MessageBoxIcon::Warning);
		else
		{
			this->Cursor->Current = Cursors::WaitCursor;
			array<Process^>^ process1 = Process::GetProcessesByName("EXCEL");
			_Excel::Application^ ExcelApp = gcnew _Excel::Application();
			_Excel::_Workbook^ ExcelWorkBook;
			_Excel::Worksheet^ ExcelWorkSheet;

			ExcelWorkBook = ExcelApp->Workbooks->Open(OpenFileDlg->FileName, nullptr, true, 5, nullptr, nullptr, true, _Excel::XlPlatform::xlWindows, "\t", false, true, nullptr, false, true, _Excel::XlCorruptLoad::xlNormalLoad);
			ExcelWorkSheet = (_Excel::Worksheet^)ExcelWorkBook->Worksheets[1];

			_Excel::Range^ range;

			for(int i_col=1;i_col<dgrdParams->ColumnCount;i_col++)
			{
				for(int i_row=0;i_row<dgrdParams->RowCount;i_row++)
				{
					if(i_col == i_IntTask && i_row == 2)
						continue;
					range = (_Excel::Range^)ExcelWorkSheet->Cells[i_row+1,i_col];
					
					dgrdParams->Rows[i_row]->Cells[i_col]->Value = range->Value2;
					if(Check_Current_Param(i_row, i_col, dgrdParams->Rows[i_row]->Cells[i_col]->FormattedValue->ToString()))
					{
						ShowErrorMessage(ErrorNumber, i_col);
						this->dgrdParams->Enabled = true;
						dgrdParams->Rows[i_row]->Cells[i_col]->Selected = true;
						dgrdParams->ReadOnly = false;
						this->clmnParamNames->ReadOnly = true;

						flDataEditing = true;
						while (Check_Current_Param(i_row, i_col, dgrdParams->Rows[i_row]->Cells[i_col]->FormattedValue->ToString())) 
						{
							Thread::Sleep(50);
							Application::DoEvents();
						}
						flDataEditing = false;
						dgrdParams->ClearSelection();
					}					 							 
				}
			}
			ExcelWorkBook->Close(false, OpenFileDlg->FileName, nullptr);
			array<Process^>^ process2 = Process::GetProcessesByName("EXCEL");
			process2[process1->Length]->Kill();
			delete[] process1;
			delete[] process2;
			dgrdParams->Enabled = true;
			this->Cursor->Current = Cursors::Default;
			flDataEditing = true;
			Final_Check_Param();
		}
	}
}
private: System::Void SaveXlsFile_Click(System::Object^  sender, System::EventArgs^  e) {
			 if(flDataEditing)
				 Final_Check_Param();
			 else if(flDataLoaded)
			 {
				 SaveFileDialog^ SaveFileDlg = gcnew SaveFileDialog;
				 SaveFileDlg->Filter = "���� Excel|*.xls";
				 if(SaveFileDlg->ShowDialog() == DlgRes::OK)
				 {
					 this->Cursor->Current = Cursors::WaitCursor;
					 _Excel::Application^ ExcelApp = gcnew _Excel::Application();
					 _Excel::_Workbook^ ExcelWorkBook;
					 _Excel::Worksheet^ ExcelWorkSheet;

					 ExcelWorkBook = ExcelApp->Workbooks->Add(System::Reflection::Missing::Value);
					 ExcelWorkSheet = (_Excel::Worksheet^)ExcelWorkBook->Worksheets[1];

					 for(int i_col=1;i_col<dgrdParams->ColumnCount;i_col++)
					 {
						 for(int i_row=0;i_row<dgrdParams->RowCount;i_row++)
						 {
							 if(i_col == i_IntTask && i_row == 2)
								 continue;
							 ExcelApp->Cells[i_row+1, i_col] = dgrdParams->Rows[i_row]->Cells[i_col]->Value;
						 }
					 }
					 ExcelWorkBook->SaveAs(SaveFileDlg->FileName, _Excel::XlFileFormat::xlWorkbookNormal,
						 nullptr, nullptr, false, false, _Excel::XlSaveAsAccessMode::xlExclusive,
						 _Excel::XlSaveConflictResolution::xlUserResolution, false, false, false, false);
					 ExcelWorkBook->Close(true, SaveFileDlg->FileName, false);
					 ExcelApp->Quit();
					 this->Cursor->Current = Cursors::Default;
				 }
			 }
		 }
private: System::Void dgrdParams_Leave(System::Object^  sender, System::EventArgs^  e) {
			 if(flDataEditing && sender!=dgrdParams)
			 {
				 Point MousePoint = this->PointToClient(MousePosition);
				 if (MousePoint.X<btnExitMain->Left || MousePoint.X>btnExitMain->Right && MousePoint.Y<btnExitMain->Top || MousePoint.Y>btnExitMain->Bottom)
					 Final_Check_Param();				 
			 }
		 }
private: System::Void frmMain_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
			 if(flDataEditing)
				 Final_Check_Param();
		 }
private: System::Void chrtMain_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
			 if(flDataEditing)
				 Final_Check_Param();
		 }
private: System::Void TimerSystem_Tick(System::Object^  sender, System::EventArgs^  e) 
{
	Object ^ pObject = gcnew Object;
	System::Threading::Monitor::Enter(pObject);
	try
	{
		tcur++;
		lblStopWatch->Text = String::Format("{0:00}:{1:00}", tcur/60, tcur-60*(tcur/60));
	
		if(ActiveTask!=-1)
		{
			if(TaskArr[ActiveTask].tcur_exec==TaskArr[ActiveTask].tses)
			{
				TaskArr[ActiveTask].status = Active;
				TaskArr[ActiveTask].tcur_exec = 0;
				TaskArr[ActiveTask].tcur_wait = 0;
				if(ActiveTask==(i_TimeTask1-1))
				{
					TaskArr[ActiveTask].tses++;
					dgrdParams->Rows[5]->Cells[i_TimeTask1]->Value = TaskArr[ActiveTask].tses;
				}
				ActiveTask=-1;
				flSystemBusy=false;
			}
		}
		
		if(!TaskQueue.empty() && ActiveTask==-1)
		{
			TaskQueue_Iter = TaskQueue.find(i_IntTask-1);

			if(TaskQueue_Iter != TaskQueue.end())
			{
				ActiveTask = i_IntTask-1;
				TaskQueue.erase(TaskQueue_Iter);
			}
			else
			{
				TaskQueue_Iter = TaskQueue.begin();
				if(TaskQueue.size()==1)
				{
					ActiveTask = TaskQueue_Iter->first;
				}
				else if(TaskQueue.size()>1)
				{
					if(TaskQueue.begin()->first==TaskQueue.rbegin()->first)
						ActiveTask = TaskQueue_Iter->first;
					else
					{
						if(TaskArr[i_TimeTask1-1].prior>=TaskArr[i_TimeTask2-1].prior)
							ActiveTask = i_TimeTask1-1;
						else
							ActiveTask = i_TimeTask2-1;
						TaskQueue_Iter = TaskQueue.find(ActiveTask);
					}
				}
				TaskQueue.erase(TaskQueue_Iter);
			}
			TaskArr[ActiveTask].status = Extracted;
			flSystemBusy = true;
		}

		for(int l=N_TimeTask; l<N_Tasks; l++)
		{
			if (TaskArr[l].status == EndActive)
				continue;

			if(TaskArr[l].tbeg == tcur)
			{
				LogText->AppendText(DateTime::Now.ToLongTimeString() + " A����������� ������ �"+(l+1)+" � tcur = "+tcur+"\n");
				TaskArr[l].status = Active;

				if(TaskArr[l].status==NotActive)
					continue;
			}

			if(flKeyInterrupt)
			{
				if(ActiveTask==-1)
				{
					ActiveTask = l;
					flSystemBusy = true;
				}
				else
					TaskQueue.insert(Int_Pair( i_IntTask-1, tcur));
				flKeyInterrupt=false;
			}

			if(ActiveTask == l || TaskArr[l].status == Extracted)
			{
				if (TaskArr[l].status != Execution)
				{
					TaskArr[l].status = Execution;
					LogText->AppendText(DateTime::Now.ToLongTimeString() + " ����������� ������ �"+(l+1)+" � tcur = "+tcur+"\n");
					TaskArr[l].tfact = tcur;
					TaskArr[l].tcur_wait = 0;
					LogText->AppendText(DateTime::Now.ToLongTimeString() + " ������ �"+(l+1)+": ����� = "+TaskArr[l].tfact+"\n");
				}
			}

			if(ActiveTask == l)
				TaskArr[l].tcur_exec++;

			if(flKeyInterrupt && ActiveTask!=l)
				TaskArr[l].tcur_wait++;

			if(TaskArr[l].tcur_wait > TaskArr[l].twait && ActiveTask!=l)
			{
				TaskArr[l].status = CallDelayed;
				TaskArr[l].tend = tcur;
				LogText->AppendText(DateTime::Now.ToLongTimeString() + " ������ �"+(l+1)+": �������� ������ �������_��� = "+TaskArr[l].tcur_wait+" ("+TaskArr[l].tend+"). ������ ����� � ����������.\n");
				TaskArr[l].status = Aborted;
				dgrdParams->Rows[0]->Cells[l+1]->Value = TaskArr[l].tend;
			}

			if(TaskArr[l].tcur_exec>TaskArr[l].tmax_exec && TaskArr[l].status != ExecutionDelayed && ActiveTask==l && TaskArr[l].tcur_exec==TaskArr[l].tses)
			{
				TaskArr[l].status = ExecutionDelayed;
				ExecutionExcess(l);
			}

			if(tcur>TaskArr[l].tend && TaskArr[l].status != EndActive)
			{
				if(ActiveTask==l)
				{
					ActiveTask=-1;
					flSystemBusy=false;
				}

				for(TaskQueue_Iter=TaskQueue.begin();TaskQueue_Iter!=TaskQueue.end();TaskQueue_Iter++)
				{
					if(TaskQueue_Iter->first == l)
					{
						TaskQueue.erase(TaskQueue_Iter);
						TaskQueue_Iter=TaskQueue.begin();
					}
				}
				TaskArr[l].status = EndActive;
				LogText->AppendText(DateTime::Now.ToLongTimeString() + " ������ �"+(l+1)+": ������� �������� ���������� � "+TaskArr[l].tend+" �.\n");

				switch(l)
				{
				case 2: if(Task3Thread)
							Task3Thread->Abort(); break;
				}
			}
		}

		for(int i=0; i<N_TimeTask; i++)
		{
			if (TaskArr[i].status == EndActive)
				continue;

			if(i == i_TimeTask2-1)
			{
				if(flTaskLeftDoubleClick)
				{
					/*if(TaskArr[i].tplan == tcur)
					{
						TaskArr[i].prev_tplan = TaskArr[i].tplan;
						TaskArr[i].tplan = TaskArr[i].tplan + TaskArr[i].tper;
						LogText->AppendText(DateTime::Now.ToLongTimeString() + " ������ �"+(i+1)+": ����� = "+TaskArr[i].tplan+" ("+tcur+")\n");
					}*/
					if(!TaskPause)
					{
						//TaskArr[i_TimeTask2-1].status = Paused;
						TaskPause = true;
						if(ActiveTask==i_TimeTask2-1)
						{
							//chrtMain->Series[i_TimeTask2]->Points->AddXY(tcur, 1);
							ActiveTask=-1;
						}
						LogText->AppendText(DateTime::Now.ToLongTimeString() + " ������ �"+(i_TimeTask2)+" �������������� � tcur = "+tcur+"\n");
					}
					continue;
				}
				else if(flTaskRightDoubleClick && TaskPause)
				{
					TaskPause = false;
					flTaskRightDoubleClick = false;
					if(ActiveTask==-1 && TaskArr[i_TimeTask2-1].status==Execution)
					{
						ActiveTask = i_TimeTask2-1;
					}
					else
						TaskArr[i_TimeTask2-1].status = Active;
					LogText->AppendText(DateTime::Now.ToLongTimeString() + " ������ �"+(i_TimeTask2)+" ������������ � tcur = "+tcur+"\n");
				}
			}

			if(TaskArr[i].tbeg == tcur)
			{
				LogText->AppendText(DateTime::Now.ToLongTimeString() + " A����������� ������ �"+(i+1)+" � tcur = "+tcur+"\n");
				TaskArr[i].status = Active;
			}

			if(TaskArr[i].status==NotActive)
				continue;

			if(TaskArr[i].tplan == tcur )
			{
				if(ActiveTask==-1)
				{
					ActiveTask = i;
					if(TaskArr[i].prior>TaskArr[ActiveTask].prior && ActiveTask!=i_IntTask-1)
						ActiveTask = i;
					
					flSystemBusy = true;
				}
				else
					TaskQueue.insert(Int_Pair(i, tcur));
			}

			if(TaskArr[i].tplan == tcur || TaskArr[i].status == Extracted)
			{
				if (ActiveTask == i && TaskArr[i].status != Execution)
				{
					TaskArr[i].status = Execution;
					LogText->AppendText(DateTime::Now.ToLongTimeString() + " ����������� ������ �"+(i+1)+" � tcur = "+tcur+"\n");
					TaskArr[i].tfact = tcur;
					TaskArr[i].prev_tplan = tcur;
					TaskArr[i].tplan = TaskArr[i].tfact + TaskArr[i].tper;
					LogText->AppendText(DateTime::Now.ToLongTimeString() + " ������ �"+(i+1)+": ����� = "+TaskArr[i].tplan+" ("+tcur+")\n");
					TaskArr[i].tcur_wait = 0;
					LogText->AppendText(DateTime::Now.ToLongTimeString() + " ������ �"+(i+1)+": ����� = "+TaskArr[i].tfact+"\n");
				}
			}

			if(ActiveTask == i)
				TaskArr[i].tcur_exec++;

			if(TaskArr[i].tplan > TaskArr[i].tfact && TaskArr[i].tplan<tcur && ActiveTask!=i)
				TaskArr[i].tcur_wait++;

			if(TaskArr[i].tcur_wait > TaskArr[i].twait && ActiveTask!=i)
			{
				TaskArr[i].status = CallDelayed;
				TaskArr[i].tend = tcur;
				LogText->AppendText(DateTime::Now.ToLongTimeString() + " ������ �"+(i+1)+": �������� ������ �������_��� = "+TaskArr[i].tcur_wait+" ("+TaskArr[i].tend+"). ������ ����� � ����������.\n");
				TaskArr[i].status = Aborted;
				dgrdParams->Rows[0]->Cells[i+1]->Value = TaskArr[i].tend;
			}

			if(TaskArr[i].tcur_exec>TaskArr[i].tmax_exec && TaskArr[i].status != ExecutionDelayed && ActiveTask==i && TaskArr[i].tcur_exec==TaskArr[i].tses)
			{
				TaskArr[i].status = ExecutionDelayed;
				ExecutionExcess(i);
			}

			if(tcur>=TaskArr[i].tend && TaskArr[i].status != EndActive)
			{
	
				for(TaskQueue_Iter=TaskQueue.begin();TaskQueue_Iter!=TaskQueue.end();TaskQueue_Iter++)
				{
					if(TaskQueue_Iter->first == i)
					{
						TaskQueue.erase(TaskQueue_Iter);
						TaskQueue_Iter=TaskQueue.begin();
					}
				}
				
				if(ActiveTask==i)
				{
					ActiveTask=-1;
					flSystemBusy=false;
				}

				TaskArr[i].status = EndActive;
				LogText->AppendText(DateTime::Now.ToLongTimeString() + " ������ �"+(i+1)+": ������� �������� ���������� � "+TaskArr[i].tend+" �.\n");

				switch(i)
				{
				case 0: if(Task1Thread)
							Task1Thread->Abort(); break;
				case 1: if(Task2Thread)
							Task2Thread->Abort(); break;
				}
			}
		}
	}
	finally
	{
		System::Threading::Monitor::Exit(pObject);
	}
		

	if(tcur >= Tmod)
	{
		TimerSystem->Enabled = false;
		StatusBarStatus->Text = "������������� ���������";
		LogText->AppendText(DateTime::Now.ToLongTimeString() + " ������������� ������� ���������!\n");
	}
	else if(tcur == Tmod-15)
		lblStopWatch->ForeColor = Color::DarkOrange;
	else if(tcur == Tmod-5)
		lblStopWatch->ForeColor = Color::Red;
	if(tcur > Tmod-15)
		lblStopWatch->Font = gcnew System::Drawing::Font(lblStopWatch->Font->Name, 16+tcur%2, lblStopWatch->Font->Style);
}
private: System::Void chrtMain_MouseDoubleClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
			 if(TaskArr[i_TimeTask2-1].status == Active || TaskArr[i_TimeTask2-1].status == Execution || TaskArr[i_TimeTask2-1].status == Paused)
				 if(e->Button == Windows::Forms::MouseButtons::Left)
				 {
					 flTaskLeftDoubleClick = true;
					 flTaskRightDoubleClick = false;
					 chrtMain->Series[i_TimeTask2]->Points[tcur]->MarkerColor = Color::Blue;
					 chrtMain->Series[i_TimeTask2]->Points[tcur]->MarkerStyle = MarkerStyle::Diamond;
				 }
				 else if(e->Button == Windows::Forms::MouseButtons::Right)
				 {
					 flTaskLeftDoubleClick = false;
					 flTaskRightDoubleClick = true;
					 chrtMain->Series[i_TimeTask2]->Points[tcur+1]->MarkerColor = Color::Blue;
					 chrtMain->Series[i_TimeTask2]->Points[tcur+1]->MarkerStyle = MarkerStyle::Diamond;
				 }
		 }
private: System::Void LogText_TextChanged(System::Object^  sender, System::EventArgs^  e) {
			 LogText->SelectionStart = LogText->TextLength;
			 LogText->ScrollToCaret();
		 }
private: System::Void chrtMain_KeyDown(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e) 
{
	if(e->KeyData == Keys::K)
	{
		flKeyInterrupt = true;

		chrtMain->Series[3]->Points[tcur+1]->MarkerColor = Color::Blue;
		chrtMain->Series[3]->Points[tcur+1]->MarkerStyle = MarkerStyle::Cross;
	}
}
};
}

