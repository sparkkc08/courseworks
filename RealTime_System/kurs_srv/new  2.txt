for(int l=N_TimeTask; i<N_Tasks; i++)
		{
			if (TaskArr[i].status == EndActive)
				continue;

			if(TaskArr[i].tbeg == tcur)
			{
				LogText->AppendText(DateTime::Now.ToLongTimeString() + " A����������� ������ �"+(i+1)+" � tcur = "+tcur+"\n");
				TaskArr[i].status = Active;

				if(TaskArr[i].status==NotActive)
					continue;
			}

			if(flKeyInterrupt)
			{
				if(ActiveTask==-1)
				{
					ActiveTask = i;
					if(TaskArr[i].prior>TaskArr[ActiveTask].prior)
						ActiveTask = i;

					flSystemBusy = true;
				}
				else
					TaskQueue.insert(Int_Pair( tcur, i ));
			}

			if(flKeyInterrupt || TaskArr[i].status == Extracted)
			{
				if (ActiveTask == i && TaskArr[i].status != Execution)
				{
					if(TaskArr[i].status == Extracted)
						chrtMain->Series[i+1]->Points[TaskArr[i].tplan]->MarkerStyle = MarkerStyle::None;

					TaskArr[i].status = Execution;
					LogText->AppendText(DateTime::Now.ToLongTimeString() + " ����������� ������ �"+(i+1)+" � tcur = "+tcur+"\n");
					TaskArr[i].tfact = tcur;
					TaskArr[i].tcur_wait = 0;
					chrtMain->Series[i+1]->Points[TaskArr[i].tfact]->YValues[0] = 1;
					chrtMain->Series[i+1]->Points[TaskArr[i].tfact]->MarkerColor = Color::Orange;
					chrtMain->Series[i+1]->Points[TaskArr[i].tfact]->MarkerStyle = MarkerStyle::Cross;
					LogText->AppendText(DateTime::Now.ToLongTimeString() + " ������ �"+(i+1)+": ����� = "+TaskArr[i].tfact+"\n");
				}
			}

			if(ActiveTask == i)
				TaskArr[i].tcur_exec++;

			/*if(TaskArr[i].prev_tplan > TaskArr[i].tfact && TaskArr[i].prev_tplan<tcur && ActiveTask!=i)
			TaskArr[i].tcur_wait++;*/

			if(TaskArr[i].tcur_wait > TaskArr[i].twait && ActiveTask!=i)
			{
				TaskArr[i].status = CallDelayed;
				TaskArr[i].tend = tcur;
				LogText->AppendText(DateTime::Now.ToLongTimeString() + " ������ �"+(i+1)+": �������� ������ �������_��� = "+TaskArr[i].tcur_wait+" ("+TaskArr[i].tend+"). ������ ����� � ����������.\n");
				TaskArr[i].status = Aborted;
				dgrdParams->Rows[0]->Cells[i+1]->Value = TaskArr[i].tend;
			}
		}