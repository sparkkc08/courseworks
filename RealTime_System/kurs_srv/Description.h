/** Description.h **/

#include <map>
using namespace std;

//Defines
#define Tmod 98					// ����� �������������
#define N_Tasks 3				// ����� ���-�� �����
#define N_TimeTask 2			// ���-�� ��������� �����
#define N_InterryptTask 1		// ���-�� ����� �� ����������
#define i_TimeTask1 1			// ������ 1-� ��������� ������
#define i_TimeTask2 2			// ������ 2-� ��������� ������
#define i_IntTask 3				// ������ ������ �� ����������

//Flags
bool flDataLoaded = false;
bool flDataEditing = false;
bool flPlayPressed = false;
bool flTaskLeftDoubleClick = false;
bool flTaskRightDoubleClick = false;
bool TaskPause = false;
bool flSystemBusy = false;
bool flKeyInterrupt = false;

enum TaskStatus{NotActive, Active, Extracted, CallDelayed, Execution, ExecutionDelayed, EndActive, Aborted, Paused};

static int ErrorNumber;

int tcur = -1;
int ActiveTask=-1;

//structs

struct Task{
	int tend;
	int tbeg;
	int tper;
	int twait;
	int tses;
	int tmax_exec;
	int prior;
	int tplan;
	int tfact;
	int tcur_exec;
	int tcur_wait;
	int prev_tplan;
	int Ndelay;
	TaskStatus status;
}TaskArr[N_Tasks];

typedef pair <int, int> Int_Pair;
multimap <int, int>::iterator TaskQueue_Iter;
multimap <int, int> TaskQueue;
pair<multimap<int,int>::iterator,multimap<int,int>::iterator> TaskQueueGroup;
//systems 

#define DlgRes Windows::Forms::DialogResult
#define _Excel Microsoft::Office::Interop::Excel

