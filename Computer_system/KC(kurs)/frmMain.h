#pragma once
#include "frmGraph.h"
#include <math.h>

int Zayavki[5]={12,13,14,15,16};
double Teta[5]={100000,100000,100000,100000,100000};
double V[5]={0,0,0,0,0};
double Bmin,Bmax,dB,iB;
double Lambda[5]={0.04,0.04,0.04,0.04,0.04};
double Nu[5]={0,0,0,0,0};
double Omegaz[5]={3,3,3,3,3};
double Omega[5]={0,0,0,0,0};
double dOmega[5]={0,0,0,0,0};
double Q[5][5]={{0,2,2,2,2},{0,0,2,2,2},{0,0,0,2,2},{0,0,0,0,2},{0,0,0,0,0}};
double p[5]={0,0,0,0,0};
double P[5]={0,0,0,0,0};
double R;
double U[5]={0,0,0,0,0};
double *pOmega;

namespace KCkurs {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Сводка для Form1
	///
	/// Внимание! При изменении имени этого класса необходимо также изменить
	///          свойство имени файла ресурсов ("Resource File Name") для средства компиляции управляемого ресурса,
	///          связанного со всеми файлами с расширением .resx, от которых зависит данный класс. В противном случае,
	///          конструкторы не смогут правильно работать с локализованными
	///          ресурсами, сопоставленными данной форме.
	/// </summary>
	public ref class Form1 : public System::Windows::Forms::Form
	{ 
	public:
		Form1(void)
		{
			InitializeComponent();
			//
			//TODO: добавьте код конструктора
			//
		}

	protected:
		/// <summary>
		/// Освободить все используемые ресурсы.
		/// </summary>
		~Form1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::MenuStrip^  menuStrip1;
	protected: 
	private: System::Windows::Forms::ToolStripMenuItem^  minБыстродействиеToolStripMenuItem;

	private: System::Windows::Forms::DataGridView^  grdData;





	private: System::Windows::Forms::Label^  lblBmin;
	private: System::Windows::Forms::Label^  lblInData;
	private: System::Windows::Forms::TextBox^  txtBmin;
	private: System::Windows::Forms::Label^  lblBminVal;
	private: System::Windows::Forms::Button^  btnExit;
	private: System::Windows::Forms::Button^  btnBackBmin;
	private: System::Windows::Forms::ToolStripMenuItem^  дОИХарактеристикиКСToolStripMenuItem;
	private: System::Windows::Forms::Label^  lblBmax;

	private: System::Windows::Forms::Label^  lblBmaxVal;


	private: System::Windows::Forms::TextBox^  txtBmax;
	private: System::Windows::Forms::DataGridView^  grdParKC;







	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column0;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column1;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column2;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column3;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column4;








	private: System::Windows::Forms::Button^  btnDraw;
	private: System::Windows::Forms::ToolStripMenuItem^  следующееБыстродействиеToolStripMenuItem;
	private: System::Windows::Forms::TextBox^  txtCurB;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column5;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column6;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column7;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column8;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column9;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column10;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Column11;
	private: System::Windows::Forms::TextBox^  txtR;
	private: System::Windows::Forms::TextBox^  txtSumLambda;
	private: System::Windows::Forms::TextBox^  txtLengthLine;
	private: System::Windows::Forms::TextBox^  txtPenalty;


































































	private:
		/// <summary>
		/// Требуется переменная конструктора.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Обязательный метод для поддержки конструктора - не изменяйте
		/// содержимое данного метода при помощи редактора кода.
		/// </summary>
		void InitializeComponent(void)
		{
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle15 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle16 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle17 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle18 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle19 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle20 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle28 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle21 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle22 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle23 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle24 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle25 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle26 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle27 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			this->grdData = (gcnew System::Windows::Forms::DataGridView());
			this->Column0 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Column1 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Column2 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Column3 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Column4 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->menuStrip1 = (gcnew System::Windows::Forms::MenuStrip());
			this->minБыстродействиеToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->дОИХарактеристикиКСToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->следующееБыстродействиеToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->lblBmin = (gcnew System::Windows::Forms::Label());
			this->lblInData = (gcnew System::Windows::Forms::Label());
			this->txtBmin = (gcnew System::Windows::Forms::TextBox());
			this->lblBminVal = (gcnew System::Windows::Forms::Label());
			this->btnExit = (gcnew System::Windows::Forms::Button());
			this->btnBackBmin = (gcnew System::Windows::Forms::Button());
			this->lblBmax = (gcnew System::Windows::Forms::Label());
			this->lblBmaxVal = (gcnew System::Windows::Forms::Label());
			this->txtBmax = (gcnew System::Windows::Forms::TextBox());
			this->grdParKC = (gcnew System::Windows::Forms::DataGridView());
			this->Column5 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Column6 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Column7 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Column8 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Column9 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Column10 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Column11 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->btnDraw = (gcnew System::Windows::Forms::Button());
			this->txtCurB = (gcnew System::Windows::Forms::TextBox());
			this->txtR = (gcnew System::Windows::Forms::TextBox());
			this->txtSumLambda = (gcnew System::Windows::Forms::TextBox());
			this->txtLengthLine = (gcnew System::Windows::Forms::TextBox());
			this->txtPenalty = (gcnew System::Windows::Forms::TextBox());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->grdData))->BeginInit();
			this->menuStrip1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->grdParKC))->BeginInit();
			this->SuspendLayout();
			// 
			// grdData
			// 
			this->grdData->AllowUserToResizeColumns = false;
			this->grdData->AllowUserToResizeRows = false;
			dataGridViewCellStyle15->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			dataGridViewCellStyle15->BackColor = System::Drawing::SystemColors::Control;
			dataGridViewCellStyle15->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			dataGridViewCellStyle15->ForeColor = System::Drawing::SystemColors::WindowText;
			dataGridViewCellStyle15->SelectionBackColor = System::Drawing::SystemColors::Control;
			dataGridViewCellStyle15->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle15->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
			this->grdData->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle15;
			this->grdData->ColumnHeadersHeight = 26;
			this->grdData->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
			this->grdData->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(5) {this->Column0, this->Column1, 
				this->Column2, this->Column3, this->Column4});
			dataGridViewCellStyle16->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			dataGridViewCellStyle16->BackColor = System::Drawing::SystemColors::InactiveCaptionText;
			dataGridViewCellStyle16->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			dataGridViewCellStyle16->ForeColor = System::Drawing::SystemColors::ControlText;
			dataGridViewCellStyle16->SelectionBackColor = System::Drawing::SystemColors::Control;
			dataGridViewCellStyle16->SelectionForeColor = System::Drawing::SystemColors::ControlText;
			dataGridViewCellStyle16->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
			this->grdData->DefaultCellStyle = dataGridViewCellStyle16;
			this->grdData->Location = System::Drawing::Point(44, 70);
			this->grdData->Name = L"grdData";
			this->grdData->ReadOnly = true;
			dataGridViewCellStyle17->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			dataGridViewCellStyle17->BackColor = System::Drawing::SystemColors::Control;
			dataGridViewCellStyle17->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			dataGridViewCellStyle17->ForeColor = System::Drawing::SystemColors::WindowText;
			dataGridViewCellStyle17->SelectionBackColor = System::Drawing::SystemColors::InactiveBorder;
			dataGridViewCellStyle17->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle17->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
			this->grdData->RowHeadersDefaultCellStyle = dataGridViewCellStyle17;
			this->grdData->RowHeadersVisible = false;
			this->grdData->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::DisableResizing;
			dataGridViewCellStyle18->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			dataGridViewCellStyle18->BackColor = System::Drawing::Color::White;
			dataGridViewCellStyle18->ForeColor = System::Drawing::Color::Black;
			dataGridViewCellStyle18->SelectionBackColor = System::Drawing::SystemColors::ButtonFace;
			dataGridViewCellStyle18->SelectionForeColor = System::Drawing::Color::Black;
			this->grdData->RowsDefaultCellStyle = dataGridViewCellStyle18;
			this->grdData->RowTemplate->DefaultCellStyle->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			this->grdData->RowTemplate->Height = 20;
			this->grdData->RowTemplate->ReadOnly = true;
			this->grdData->RowTemplate->Resizable = System::Windows::Forms::DataGridViewTriState::False;
			this->grdData->ScrollBars = System::Windows::Forms::ScrollBars::None;
			this->grdData->Size = System::Drawing::Size(292, 128);
			this->grdData->TabIndex = 1;
			this->grdData->Visible = false;
			// 
			// Column0
			// 
			this->Column0->HeaderText = L"№ заявки";
			this->Column0->Name = L"Column0";
			this->Column0->ReadOnly = true;
			this->Column0->Resizable = System::Windows::Forms::DataGridViewTriState::False;
			this->Column0->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
			this->Column0->Width = 70;
			// 
			// Column1
			// 
			this->Column1->HeaderText = L"λ";
			this->Column1->Name = L"Column1";
			this->Column1->ReadOnly = true;
			this->Column1->Resizable = System::Windows::Forms::DataGridViewTriState::False;
			this->Column1->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
			this->Column1->Width = 50;
			// 
			// Column2
			// 
			this->Column2->HeaderText = L"Θ";
			this->Column2->MaxInputLength = 100000;
			this->Column2->Name = L"Column2";
			this->Column2->ReadOnly = true;
			this->Column2->Resizable = System::Windows::Forms::DataGridViewTriState::False;
			this->Column2->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
			this->Column2->Width = 70;
			// 
			// Column3
			// 
			this->Column3->HeaderText = L"ν";
			this->Column3->Name = L"Column3";
			this->Column3->ReadOnly = true;
			this->Column3->Resizable = System::Windows::Forms::DataGridViewTriState::False;
			this->Column3->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
			this->Column3->Width = 50;
			// 
			// Column4
			// 
			this->Column4->HeaderText = L"ω*";
			this->Column4->Name = L"Column4";
			this->Column4->ReadOnly = true;
			this->Column4->Resizable = System::Windows::Forms::DataGridViewTriState::False;
			this->Column4->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
			this->Column4->Width = 50;
			// 
			// menuStrip1
			// 
			this->menuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {this->minБыстродействиеToolStripMenuItem, 
				this->дОИХарактеристикиКСToolStripMenuItem});
			this->menuStrip1->Location = System::Drawing::Point(0, 0);
			this->menuStrip1->Name = L"menuStrip1";
			this->menuStrip1->Size = System::Drawing::Size(841, 24);
			this->menuStrip1->TabIndex = 0;
			this->menuStrip1->Text = L"menuStrip1";
			// 
			// minБыстродействиеToolStripMenuItem
			// 
			this->minБыстродействиеToolStripMenuItem->Name = L"minБыстродействиеToolStripMenuItem";
			this->minБыстродействиеToolStripMenuItem->Size = System::Drawing::Size(132, 20);
			this->minБыстродействиеToolStripMenuItem->Text = L"Min быстродействие ";
			this->minБыстродействиеToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::minБыстродействиеToolStripMenuItem_Click);
			// 
			// дОИХарактеристикиКСToolStripMenuItem
			// 
			this->дОИХарактеристикиКСToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) {this->следующееБыстродействиеToolStripMenuItem});
			this->дОИХарактеристикиКСToolStripMenuItem->Name = L"дОИХарактеристикиКСToolStripMenuItem";
			this->дОИХарактеристикиКСToolStripMenuItem->Size = System::Drawing::Size(150, 20);
			this->дОИХарактеристикиКСToolStripMenuItem->Text = L"ДО и характеристики КС";
			this->дОИХарактеристикиКСToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::дОИХарактеристикиКСToolStripMenuItem_Click);
			// 
			// следующееБыстродействиеToolStripMenuItem
			// 
			this->следующееБыстродействиеToolStripMenuItem->Name = L"следующееБыстродействиеToolStripMenuItem";
			this->следующееБыстродействиеToolStripMenuItem->Size = System::Drawing::Size(224, 22);
			this->следующееБыстродействиеToolStripMenuItem->Text = L"Следующее быстродействие";
			this->следующееБыстродействиеToolStripMenuItem->Click += gcnew System::EventHandler(this, &Form1::следующееБыстродействиеToolStripMenuItem_Click);
			// 
			// lblBmin
			// 
			this->lblBmin->AutoSize = true;
			this->lblBmin->Location = System::Drawing::Point(41, 210);
			this->lblBmin->Name = L"lblBmin";
			this->lblBmin->Size = System::Drawing::Size(42, 13);
			this->lblBmin->TabIndex = 2;
			this->lblBmin->Text = L"Bmin = ";
			this->lblBmin->Visible = false;
			// 
			// lblInData
			// 
			this->lblInData->AutoSize = true;
			this->lblInData->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 11, System::Drawing::FontStyle::Underline, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->lblInData->ForeColor = System::Drawing::Color::Blue;
			this->lblInData->Location = System::Drawing::Point(41, 44);
			this->lblInData->Name = L"lblInData";
			this->lblInData->Size = System::Drawing::Size(139, 18);
			this->lblInData->TabIndex = 3;
			this->lblInData->Text = L"Исходные данные:";
			this->lblInData->Visible = false;
			// 
			// txtBmin
			// 
			this->txtBmin->BackColor = System::Drawing::SystemColors::ButtonFace;
			this->txtBmin->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->txtBmin->Location = System::Drawing::Point(80, 206);
			this->txtBmin->MaxLength = 1000000;
			this->txtBmin->Name = L"txtBmin";
			this->txtBmin->ReadOnly = true;
			this->txtBmin->Size = System::Drawing::Size(58, 21);
			this->txtBmin->TabIndex = 4;
			this->txtBmin->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->txtBmin->Visible = false;
			// 
			// lblBminVal
			// 
			this->lblBminVal->AutoSize = true;
			this->lblBminVal->Location = System::Drawing::Point(144, 211);
			this->lblBminVal->Name = L"lblBminVal";
			this->lblBminVal->Size = System::Drawing::Size(36, 13);
			this->lblBminVal->TabIndex = 5;
			this->lblBminVal->Text = L"оп./с.";
			this->lblBminVal->Visible = false;
			// 
			// btnExit
			// 
			this->btnExit->Location = System::Drawing::Point(499, 265);
			this->btnExit->Name = L"btnExit";
			this->btnExit->Size = System::Drawing::Size(75, 23);
			this->btnExit->TabIndex = 6;
			this->btnExit->Text = L"В&ыход ";
			this->btnExit->UseVisualStyleBackColor = true;
			this->btnExit->Click += gcnew System::EventHandler(this, &Form1::btnExit_Click);
			// 
			// btnBackBmin
			// 
			this->btnBackBmin->Location = System::Drawing::Point(402, 265);
			this->btnBackBmin->Name = L"btnBackBmin";
			this->btnBackBmin->Size = System::Drawing::Size(75, 23);
			this->btnBackBmin->TabIndex = 7;
			this->btnBackBmin->Text = L"Назад";
			this->btnBackBmin->UseVisualStyleBackColor = true;
			this->btnBackBmin->Click += gcnew System::EventHandler(this, &Form1::button1_Click);
			// 
			// lblBmax
			// 
			this->lblBmax->AutoSize = true;
			this->lblBmax->Location = System::Drawing::Point(41, 237);
			this->lblBmax->Name = L"lblBmax";
			this->lblBmax->Size = System::Drawing::Size(45, 13);
			this->lblBmax->TabIndex = 2;
			this->lblBmax->Text = L"Bmax = ";
			this->lblBmax->Visible = false;
			// 
			// lblBmaxVal
			// 
			this->lblBmaxVal->AutoSize = true;
			this->lblBmaxVal->Location = System::Drawing::Point(147, 237);
			this->lblBmaxVal->Name = L"lblBmaxVal";
			this->lblBmaxVal->Size = System::Drawing::Size(36, 13);
			this->lblBmaxVal->TabIndex = 5;
			this->lblBmaxVal->Text = L"оп./с.";
			this->lblBmaxVal->Visible = false;
			// 
			// txtBmax
			// 
			this->txtBmax->BackColor = System::Drawing::SystemColors::ButtonFace;
			this->txtBmax->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->txtBmax->Location = System::Drawing::Point(83, 232);
			this->txtBmax->MaxLength = 1000000;
			this->txtBmax->Name = L"txtBmax";
			this->txtBmax->ReadOnly = true;
			this->txtBmax->Size = System::Drawing::Size(58, 21);
			this->txtBmax->TabIndex = 4;
			this->txtBmax->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			this->txtBmax->Visible = false;
			// 
			// grdParKC
			// 
			this->grdParKC->AllowUserToResizeColumns = false;
			this->grdParKC->AllowUserToResizeRows = false;
			dataGridViewCellStyle19->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			dataGridViewCellStyle19->BackColor = System::Drawing::Color::WhiteSmoke;
			dataGridViewCellStyle19->ForeColor = System::Drawing::Color::Black;
			dataGridViewCellStyle19->SelectionBackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(224)), 
				static_cast<System::Int32>(static_cast<System::Byte>(224)), static_cast<System::Int32>(static_cast<System::Byte>(224)));
			dataGridViewCellStyle19->SelectionForeColor = System::Drawing::Color::Black;
			this->grdParKC->AlternatingRowsDefaultCellStyle = dataGridViewCellStyle19;
			dataGridViewCellStyle20->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			dataGridViewCellStyle20->BackColor = System::Drawing::SystemColors::ControlLightLight;
			dataGridViewCellStyle20->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			dataGridViewCellStyle20->ForeColor = System::Drawing::SystemColors::WindowText;
			dataGridViewCellStyle20->SelectionBackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(224)), 
				static_cast<System::Int32>(static_cast<System::Byte>(224)), static_cast<System::Int32>(static_cast<System::Byte>(224)));
			dataGridViewCellStyle20->SelectionForeColor = System::Drawing::SystemColors::ControlText;
			this->grdParKC->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle20;
			this->grdParKC->ColumnHeadersHeight = 26;
			this->grdParKC->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::DisableResizing;
			this->grdParKC->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(7) {this->Column5, this->Column6, 
				this->Column7, this->Column8, this->Column9, this->Column10, this->Column11});
			this->grdParKC->Location = System::Drawing::Point(342, 70);
			this->grdParKC->Name = L"grdParKC";
			this->grdParKC->ReadOnly = true;
			this->grdParKC->RowHeadersVisible = false;
			dataGridViewCellStyle28->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			dataGridViewCellStyle28->SelectionBackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(224)), 
				static_cast<System::Int32>(static_cast<System::Byte>(224)), static_cast<System::Int32>(static_cast<System::Byte>(224)));
			dataGridViewCellStyle28->SelectionForeColor = System::Drawing::Color::Black;
			this->grdParKC->RowsDefaultCellStyle = dataGridViewCellStyle28;
			this->grdParKC->RowTemplate->Height = 20;
			this->grdParKC->RowTemplate->ReadOnly = true;
			this->grdParKC->RowTemplate->Resizable = System::Windows::Forms::DataGridViewTriState::False;
			this->grdParKC->ScrollBars = System::Windows::Forms::ScrollBars::None;
			this->grdParKC->Size = System::Drawing::Size(485, 128);
			this->grdParKC->TabIndex = 8;
			// 
			// Column5
			// 
			dataGridViewCellStyle21->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			dataGridViewCellStyle21->NullValue = nullptr;
			dataGridViewCellStyle21->SelectionBackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(224)), 
				static_cast<System::Int32>(static_cast<System::Byte>(224)), static_cast<System::Int32>(static_cast<System::Byte>(224)));
			dataGridViewCellStyle21->SelectionForeColor = System::Drawing::Color::Black;
			this->Column5->DefaultCellStyle = dataGridViewCellStyle21;
			this->Column5->Frozen = true;
			this->Column5->HeaderText = L"№ потока";
			this->Column5->Name = L"Column5";
			this->Column5->ReadOnly = true;
			this->Column5->Resizable = System::Windows::Forms::DataGridViewTriState::False;
			this->Column5->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
			this->Column5->Width = 63;
			// 
			// Column6
			// 
			dataGridViewCellStyle22->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			dataGridViewCellStyle22->BackColor = System::Drawing::Color::White;
			dataGridViewCellStyle22->Format = L"N4";
			dataGridViewCellStyle22->NullValue = nullptr;
			this->Column6->DefaultCellStyle = dataGridViewCellStyle22;
			this->Column6->Frozen = true;
			this->Column6->HeaderText = L"V";
			this->Column6->Name = L"Column6";
			this->Column6->ReadOnly = true;
			this->Column6->Resizable = System::Windows::Forms::DataGridViewTriState::False;
			this->Column6->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
			this->Column6->Width = 70;
			// 
			// Column7
			// 
			dataGridViewCellStyle23->Format = L"N4";
			dataGridViewCellStyle23->NullValue = nullptr;
			this->Column7->DefaultCellStyle = dataGridViewCellStyle23;
			this->Column7->Frozen = true;
			this->Column7->HeaderText = L"ρ";
			this->Column7->Name = L"Column7";
			this->Column7->ReadOnly = true;
			this->Column7->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
			this->Column7->Width = 70;
			// 
			// Column8
			// 
			dataGridViewCellStyle24->Format = L"N4";
			dataGridViewCellStyle24->NullValue = nullptr;
			this->Column8->DefaultCellStyle = dataGridViewCellStyle24;
			this->Column8->Frozen = true;
			this->Column8->HeaderText = L"ω";
			this->Column8->Name = L"Column8";
			this->Column8->ReadOnly = true;
			this->Column8->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
			this->Column8->Width = 70;
			// 
			// Column9
			// 
			dataGridViewCellStyle25->Format = L"N4";
			dataGridViewCellStyle25->NullValue = nullptr;
			this->Column9->DefaultCellStyle = dataGridViewCellStyle25;
			this->Column9->Frozen = true;
			this->Column9->HeaderText = L"U";
			this->Column9->Name = L"Column9";
			this->Column9->ReadOnly = true;
			this->Column9->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
			this->Column9->Width = 70;
			// 
			// Column10
			// 
			dataGridViewCellStyle26->Format = L"N4";
			dataGridViewCellStyle26->NullValue = nullptr;
			this->Column10->DefaultCellStyle = dataGridViewCellStyle26;
			this->Column10->Frozen = true;
			this->Column10->HeaderText = L"ω*-ω";
			this->Column10->Name = L"Column10";
			this->Column10->ReadOnly = true;
			this->Column10->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
			this->Column10->Width = 70;
			// 
			// Column11
			// 
			dataGridViewCellStyle27->Format = L"N4";
			dataGridViewCellStyle27->NullValue = nullptr;
			this->Column11->DefaultCellStyle = dataGridViewCellStyle27;
			this->Column11->Frozen = true;
			this->Column11->HeaderText = L"P";
			this->Column11->Name = L"Column11";
			this->Column11->ReadOnly = true;
			this->Column11->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::Programmatic;
			this->Column11->Width = 70;
			// 
			// btnDraw
			// 
			this->btnDraw->Location = System::Drawing::Point(591, 265);
			this->btnDraw->Name = L"btnDraw";
			this->btnDraw->Size = System::Drawing::Size(75, 23);
			this->btnDraw->TabIndex = 9;
			this->btnDraw->Text = L"График";
			this->btnDraw->UseVisualStyleBackColor = true;
			this->btnDraw->Click += gcnew System::EventHandler(this, &Form1::btnDraw_Click);
			// 
			// txtCurB
			// 
			this->txtCurB->Location = System::Drawing::Point(683, 42);
			this->txtCurB->Name = L"txtCurB";
			this->txtCurB->ReadOnly = true;
			this->txtCurB->Size = System::Drawing::Size(74, 20);
			this->txtCurB->TabIndex = 10;
			this->txtCurB->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			// 
			// txtR
			// 
			this->txtR->Location = System::Drawing::Point(602, 42);
			this->txtR->Name = L"txtR";
			this->txtR->ReadOnly = true;
			this->txtR->Size = System::Drawing::Size(75, 20);
			this->txtR->TabIndex = 11;
			this->txtR->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			// 
			// txtSumLambda
			// 
			this->txtSumLambda->Location = System::Drawing::Point(518, 42);
			this->txtSumLambda->Name = L"txtSumLambda";
			this->txtSumLambda->ReadOnly = true;
			this->txtSumLambda->Size = System::Drawing::Size(78, 20);
			this->txtSumLambda->TabIndex = 12;
			this->txtSumLambda->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			// 
			// txtLengthLine
			// 
			this->txtLengthLine->Location = System::Drawing::Point(434, 44);
			this->txtLengthLine->Name = L"txtLengthLine";
			this->txtLengthLine->ReadOnly = true;
			this->txtLengthLine->Size = System::Drawing::Size(78, 20);
			this->txtLengthLine->TabIndex = 12;
			this->txtLengthLine->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			// 
			// txtPenalty
			// 
			this->txtPenalty->Location = System::Drawing::Point(350, 45);
			this->txtPenalty->MaxLength = 1;
			this->txtPenalty->Name = L"txtPenalty";
			this->txtPenalty->ReadOnly = true;
			this->txtPenalty->Size = System::Drawing::Size(78, 20);
			this->txtPenalty->TabIndex = 1;
			this->txtPenalty->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			// 
			// Form1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(841, 300);
			this->Controls->Add(this->txtPenalty);
			this->Controls->Add(this->txtLengthLine);
			this->Controls->Add(this->txtSumLambda);
			this->Controls->Add(this->txtR);
			this->Controls->Add(this->txtCurB);
			this->Controls->Add(this->btnDraw);
			this->Controls->Add(this->grdParKC);
			this->Controls->Add(this->btnBackBmin);
			this->Controls->Add(this->btnExit);
			this->Controls->Add(this->lblBmaxVal);
			this->Controls->Add(this->lblBminVal);
			this->Controls->Add(this->txtBmax);
			this->Controls->Add(this->txtBmin);
			this->Controls->Add(this->lblInData);
			this->Controls->Add(this->lblBmax);
			this->Controls->Add(this->lblBmin);
			this->Controls->Add(this->grdData);
			this->Controls->Add(this->menuStrip1);
			this->MainMenuStrip = this->menuStrip1;
			this->Name = L"Form1";
			this->Text = L"Миронюк М. В.";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->grdData))->EndInit();
			this->menuStrip1->ResumeLayout(false);
			this->menuStrip1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->grdParKC))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void Form1_Load(System::Object^  sender, System::EventArgs^  e) {
			 }
	private: System::Void minБыстродействиеToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
				 double A,C,D;
				 iB=0;
				 lblInData->Visible = true;
				 grdData->Visible = true;
			     lblBmin->Visible = true;
				 txtBmin->Visible = true;
				 lblBmaxVal->Visible = true;
				 lblBmax->Visible = true;
				 txtBmax->Visible = true;
				 lblBminVal->Visible = true;
				 
				 for(int i=0;i<5;i++)
				 {
					 grdData->Rows->Insert(i,Zayavki[i],Lambda[i],Teta[i],Nu[i],Omegaz[i]);
				 }			
				 for(int i=0;i<5;i++)
				 { 
					 A = A+Lambda[i]*Teta[i];
					 C = C+Lambda[i]*Teta[i]*Teta[i]*(1+Nu[i]*Nu[i]);
					 D = D+Lambda[i]*Teta[i]*Omegaz[i];
				 }
				 Bmin = ceil(0.5*A+sqrt(0.25*A*(A+2*C/D)));
				 Bmax = 2*Bmin;
				 dB = (Bmax-Bmin)/5;
				 txtBmin->Text=Bmin.ToString();
				 txtBmax->Text=Bmax.ToString();
			 }
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
				lblInData->Visible = false;
				grdData->Visible = false;
			    lblBmin->Visible = false;
				txtBmin->Visible = false;
				lblBminVal->Visible = false;
				lblBmaxVal->Visible = false;
				lblBmax->Visible = false;
				txtBmax->Visible = false;
				
		 }
double dround(double d, int digit)
{
	int pow10;
	pow10=pow(10.0,digit);
	d=d*pow10;
	d=ceil(d);
	return d/pow10;
}
int make_Omega(double B)
{
	double sum1,sum2,sum3,sum4;
	grdData->Rows->Clear();
	for(int k=0;k<5;k++)
	{
		sum1=sum2=sum3=sum4=0;
		for(int i=0;i<5;i++)
		{
			sum1=sum1+Q[i][k]*(Q[i][k]-1)*p[i];
			sum2=sum2+(2-Q[k][i])*(1+Q[k][i])*(1+Nu[i]*Nu[i])*Lambda[i]*Teta[i]*Teta[i];
			sum3=sum3+Q[i][k]*(3-Q[i][k])*p[i];
			sum4=sum4+(1-Q[k][i])*(2-Q[k][i])*p[i];
		}
		
		Omega[k]=((Teta[k]*sum1)/(B*(2-sum1)))+(sum2/((B*B)*(2-sum3)*(2-sum4)));
		dOmega[k]=Omegaz[k]-Omega[k];
		grdData->Rows->Insert(k,sum1,sum2,sum3,sum4,Omega[k]);
	}
	return 0;
}
int make_param(int B)
{
	double sumLambda,Penalty,LengthLine; 
	for(int k=iB;k<=(iB+1);k++)
	{	
		sumLambda=Penalty=LengthLine=0;
		for(int i=0;i<5;i++)
		{
			V[i]=double(Teta[i])/double(iB*dB+B);
			R=0;
			for(int j=0;j<5;j++)
			{
				p[j]=double(Teta[j])*Lambda[j]/double(iB*dB+B);
				R=R+p[j];
			}
			make_Omega(iB*dB+B);
			P[i]=R*exp(-R*(Omegaz[i]/Omega[i]));
			U[i]=Omega[i]+V[i];
			grdParKC->Rows->Insert(i,i+1,V[i],p[i],Omega[i],U[i],dOmega[i],P[i]);
			sumLambda+=Lambda[i];
			LengthLine+=Lambda[i]*Omega[i];
			Penalty+=Lambda[i]*P[i];
			fg->crtGraph->Series[i]->Points->AddXY(k*dB+B,Omega[i]);
		}
		Penalty=dround(Penalty,4);
		LengthLine=dround(LengthLine,4);
		txtLengthLine->Text=LengthLine.ToString();
		txtSumLambda->Text=sumLambda.ToString();
		txtPenalty->Text=Penalty.ToString();
		txtCurB->Text=(iB*dB+B).ToString();
		txtR->Text=R.ToString();
	}
	iB=iB+1;
	return 0;
}
private: System::Void btnExit_Click(System::Object^  sender, System::EventArgs^  e) {
			 Application::Exit();
		 }
private: System::Void дОИХарактеристикиКСToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

		 }
private: System::Void btnDraw_Click(System::Object^  sender, System::EventArgs^  e) {
			 pOmega=Omega;
			 frmGraph ^fg = gcnew frmGraph(Bmin,dB,pOmega);
			 fg->ShowDialog();
		 }
private: System::Void следующееБыстродействиеToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (iB==5)
				 iB=0;
			 iB=0;
			 make_param(50000);
		 }
};
}

