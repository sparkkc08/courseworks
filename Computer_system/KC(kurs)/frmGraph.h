#pragma once
//#include "frmMain.h"
using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;

namespace KCkurs {
	/// <summary>
	/// ������ ��� frmGraph
	///
	/// ��������! ��� ��������� ����� ����� ������ ���������� ����� ��������
	///          �������� ����� ����� �������� ("Resource File Name") ��� �������� ���������� ������������ �������,
	///          ���������� �� ����� ������� � ����������� .resx, �� ������� ������� ������ �����. � ��������� ������,
	///          ������������ �� ������ ��������� �������� � ���������������
	///          ���������, ��������������� ������ �����.
	/// </summary>
	public ref class frmGraph : public System::Windows::Forms::Form
	{
	public:
		frmGraph(int Bmin, int dB, double *pOmega)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
			this->Bmin=Bmin;
			this->dB=dB;
			this->pOmega=pOmega;
		}
	int Bmin,dB;
	double *pOmega;
	
	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~frmGraph()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::DataVisualization::Charting::Chart^  crtGraph;
	private: System::Windows::Forms::Button^  btnCloseGraph;
	protected: 

	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			System::Windows::Forms::DataVisualization::Charting::ChartArea^  chartArea1 = (gcnew System::Windows::Forms::DataVisualization::Charting::ChartArea());
			System::Windows::Forms::DataVisualization::Charting::Legend^  legend1 = (gcnew System::Windows::Forms::DataVisualization::Charting::Legend());
			System::Windows::Forms::DataVisualization::Charting::Series^  series1 = (gcnew System::Windows::Forms::DataVisualization::Charting::Series());
			System::Windows::Forms::DataVisualization::Charting::Title^  title1 = (gcnew System::Windows::Forms::DataVisualization::Charting::Title());
			System::Windows::Forms::DataVisualization::Charting::Title^  title2 = (gcnew System::Windows::Forms::DataVisualization::Charting::Title());
			this->crtGraph = (gcnew System::Windows::Forms::DataVisualization::Charting::Chart());
			this->btnCloseGraph = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->crtGraph))->BeginInit();
			this->SuspendLayout();
			// 
			// crtGraph
			// 
			chartArea1->Name = L"ChartArea1";
			this->crtGraph->ChartAreas->Add(chartArea1);
			legend1->Name = L"Legend1";
			this->crtGraph->Legends->Add(legend1);
			this->crtGraph->Location = System::Drawing::Point(12, 12);
			this->crtGraph->Name = L"crtGraph";
			series1->ChartArea = L"ChartArea1";
			series1->ChartType = System::Windows::Forms::DataVisualization::Charting::SeriesChartType::Spline;
			series1->IsXValueIndexed = true;
			series1->Legend = L"Legend1";
			series1->Name = L"Series1";
			series1->XValueType = System::Windows::Forms::DataVisualization::Charting::ChartValueType::Int32;
			series1->YValueType = System::Windows::Forms::DataVisualization::Charting::ChartValueType::Double;
			this->crtGraph->Series->Add(series1);
			this->crtGraph->Size = System::Drawing::Size(619, 295);
			this->crtGraph->TabIndex = 0;
			this->crtGraph->Text = L"chart1";
			title1->Name = L"Title1";
			title2->Name = L"Title2";
			this->crtGraph->Titles->Add(title1);
			this->crtGraph->Titles->Add(title2);
			// 
			// btnCloseGraph
			// 
			this->btnCloseGraph->Location = System::Drawing::Point(545, 332);
			this->btnCloseGraph->Name = L"btnCloseGraph";
			this->btnCloseGraph->Size = System::Drawing::Size(75, 23);
			this->btnCloseGraph->TabIndex = 1;
			this->btnCloseGraph->Text = L"&�������";
			this->btnCloseGraph->UseVisualStyleBackColor = true;
			this->btnCloseGraph->Click += gcnew System::EventHandler(this, &frmGraph::btnCloseGraph_Click);
			// 
			// frmGraph
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(643, 367);
			this->Controls->Add(this->btnCloseGraph);
			this->Controls->Add(this->crtGraph);
			this->Name = L"frmGraph";
			this->Text = L"������";
			this->Load += gcnew System::EventHandler(this, &frmGraph::frmGraph_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->crtGraph))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void frmGraph_Load(System::Object^  sender, System::EventArgs^  e) {
				 int B;
				 double buf;
				 buf=*pOmega;
				 for(int i=0;i<5;i++)
				 {
					 B=Bmin+dB*i;
					 buf=*(pOmega+i);
					 crtGraph->Series[0]->Points->AddXY(B,buf);
				 }
			 }
	private: System::Void btnCloseGraph_Click(System::Object^  sender, System::EventArgs^  e) {
				Close();
			 }
	};
}
