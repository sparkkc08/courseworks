out_drv macro string,n
		push ax bx es
		mov bx,seg rh_out
		mov es,bx
		mov bx,offset rh_out
		mov es:[bx].rh_cmd,8
		mov ax,seg string
		mov es:[bx].rh8_buf_seg,ax
		mov ax,offset string
		mov es:[bx].rh8_buf_ofs,ax
		mov es:[bx].rh8_count,n
		call strategy_drv							
		call interrupt_drv
		pop es bx ax
endm
dseg segment 'data'
	
	;�������� ��������� �����
	rh 		struc
	rh_len		db	?	;����� �����				|
	rh_unit		db	?	;����� ���ன�⢠ (�������)		|䨪�஢����� ����
	rh_cmd		db	?	;������� �ࠩ��� ���ன�⢠		|
	rh_status  	dw	?	;�����頥��� �ࠩ��஬			|
	rh_res1	   	dd	?	;��१�ࢨ஢���
    	rh_res2	   	dd	?	;��१�ࢨ஢���
	rh		ends

	rh4		struc		;3�������� ����� ��� ������� 4
	rh4_rh  rh     <?,?,?,?>	;䨪�஢����� ����
	rh4_media	db	?	;���ਯ�� ���⥫� �� DPB
	rh4_buf_ofs	dw	?	;���饭�� DTA
	rh4_buf_seg	dw	?	;������� DTA
	rh4_count	dw	?	;���稪 ��।�� (ᥪ�஢ ��� ��������, ���⮢ ��� ᨬ������)
	rh4_start	dw	?	;��砫�� ᥪ�� (�������)
	rh4		ends

	rh5		struc		;��������� ����� ��� ������� 5
	rh5_rh	db	size rh dup (?)	;����஢����� ����
	rh5_return	db	?	;�����頥�� ᨬ���
	rh5		ends

	rh7		struc		;��������� ����� ��� ������� 7
	rh7_rh  rh     <?,?,?,?>	;䨪�஢����� ����
	rh7		ends 
	
	rh8		struc		;3�������� ����� ��� ������� 8
	rh8_rh	db	size rh dup (?)	;����஢����� ����
	rh8_media	db	?	;���ਯ�� ���⥫� �� DPB
	rh8_buf_ofs	dw	?	;���饭�� DTA
	rh8_buf_seg	dw	?	;������� DTA
	rh8_count	dw	?	;���稪 ��।�� (ᥪ�஢ ��� ��������, ���⮢ ��� ᨬ������)
	rh8_start	dw	?	;��砫�� ᥪ�� (�������)
	rh8		ends
	
	rh_input rh4 ?
	rh_ndinput rh5 ?
	rh_flinput rh7 ?
	rh_out rh8 ?
	

	msgh db '      ���ᮢ�� �஥�� �� ��',13,10
	     db '             ���� �2',13,10,13,10,'$'
	msg0 db '0. ������ �ࠩ���',13,10,'$'
	msg1 db '1. ����஢���� ah=07h int 21h',13,10,'$'
	msg2 db '2. ����஢���� ah=0ah int 21h',13,10,'$'
	msg3 db '3. ����஢���� ah=0ch int 21h',13,10,'$'
	msg4 db '4. ������� �ࠩ��� ����������',13,10,'$'
	msg5 db '5. ������� �ࠩ��� ��࠭�',13,10,'$'
	msg6 db '6. ASCII ०��',13,10,'$'
	msg7 db '7. BIN ०��',13,10,'$'
	msg8 db '8. �������� ����� ���뢠��� int 21h',13,10,'$'
	msg9 db '9. ����⠭����� ����� ���뢠��� int 21h',13,10,'$'
	msg10 db 'q. ��室 ',13,10,'$'
	msg11 db '������ �������: $'
	msg12 db '����ୠ� �������. ������ ����!$'
	msgs dw msg0,msg1,msg2,msg3,msg4,msg5,msg6,msg7,msg8,msg9,msg10,msg11
	msg13 db 13,10,'�� ��ࠫ�:    $'
	msg14 db '�ࠩ��� ����������    $'
	msg15 db '�ࠩ��� ��࠭�    $'
	msg16 db 'ASCII ०��    $' 
	msg17 db 'BIN ०��    $'
	msgi21 db '��� int 21h $'
	msgc db '^C'
	bs db 8
	space db 32
	mesc db '\'
	ent db 13,10
	dz db 7
	bs_s db 8,' ',8
	commands dw new_0ah,test_07h,test_0ah,test_0ch,close_keyb,close_disp,ASCII_on
		 dw BIN_on,new_int21h_on,new_int21h_off
	msg18 db 13,10,'������ ᨬ����: $'
	msg19 db 'Head>$'
	msg20 db '<Tail$'
	msg21 db 13,10,'������ ᨬ���: $'
	msg22 db 13,10,'�� �����: $'
	msg23 db 13,10,'������ ᨬ���� ��� ���������� ���� ����������.$'
	msg24 db ' - ���� ���$' 
	sym_07h db ?
	sym_0ah db ?
	buf_s db 11,'             '
	l_0ah db ?
	time db ?
	con db 'CON     ',0
	ondisp db 1
	onkeyb db 1
	ascii db 1
	bin db 0
	my_int21h db 0
	int21h label dword
		int21h_ofs dw ?
		int21h_seg dw ?
	strategy_drv label dword	
		strategy_ofs dw	?
		strategy_seg dw ?
	interrupt_drv label dword
		interrupt_ofs dw ?
		interrupt_seg dw ?
dseg ends
sseg segment stack
	dw 256 dup (?)
sseg ends
cseg segment 'code'
		assume ss:sseg, ds:dseg, cs:cseg 
main proc far
		push ds
		xor ax,ax
		push ax
		mov ax,dseg
		mov ds,ax

		lea bx,msg12
		call prt_str
		mov ah,0eh
		mov al,8
		int 10h
		mov ah,0eh
		mov al,20h
		int 10h
		call find_drv
		call save_int21h
	begin:
		call clrscr
		call make_status
		lea bx,msgh
		call prt_str
		mov cx,12
		mov si,0
	menu:	
		mov bx,msgs[si]
		call prt_str
		add si,2
		loop menu
		call read	
		call nline
		sub al,30h
		cmp al,41h
		je exit
		cmp al,0
		jl err1
		cmp al,9
		jg err1
		jmp noerr1
	err1:	
		call nline
		lea bx,msg12
		call prt_str
		mov ah,00h
		int 16h
		jmp begin
	noerr1:
		mov ah,0
		mov si,ax
		shl si,1
		lea bx,msg13
		call prt_str
		mov bx,msgs[si]
		call prt_str		
		call commands[si]
		call make_status
		mov ah,00h
		int 16h
		jmp begin	
		cmp al,41h
		je exit	
		jmp begin
	exit:
		call new_int21h_off
     	ret
main endp
make_status proc
		push ax bx cx dx
		mov ah,0fh
		int 10h
		mov ah,03h
		int 10h
		push ax bx cx dx di
		mov ah,0fh
		int 10h
		mov dl,0
		mov dh,24 	
		mov ah,02h
		int 10h
		lea bx,msg14
		mov al,onkeyb
		cmp al,0
		je onkeybs
		mov dl,10
		call write_s
		jmp next1
	onkeybs:
		mov dl,12
		call write_s
	next1:
		lea bx,msg15
		mov al,ondisp
		cmp al,0
		je ondisps
		mov dl,10
		call write_s
		jmp next2
	ondisps:
		mov dl,12
		call write_s
	next2:
		lea bx,msg16
		mov al,ascii
		cmp al,0
		je asciis
		mov dl,10
		call write_s
		jmp next3
	asciis:
		mov dl,12
		call write_s
	next3:

		lea bx,msg17
		mov al,bin
		cmp al,0
		je bins
		mov dl,10
		call write_s
		jmp next4
	bins:
		mov dl,12
		call write_s
	next4:
		lea bx,msgi21
		mov al,my_int21h
		cmp al,0
		je my_int21hs
		mov dl,10
		call write_s
		jmp next5
	my_int21hs:
		mov dl,12
		call write_s
	next5:
		pop di dx cx bx ax
		mov ah,02h
		int 10h
		pop dx cx bx ax
		ret
make_status endp
write_s proc 
		push ax bx di
		mov di,0
	next_s:
		mov al,[bx+di]
        	cmp al,'$'
        	je exit_wr
		mov ah,09h
        	mov cx,1
		push bx ax
		mov ah,0fh
		int 10h
		mov bl,dl
		pop ax
        	int 10h
		pop bx
		inc di
		call move_curs
		jmp next_s
	exit_wr:
		pop di bx ax
		ret
write_s endp
move_curs proc
		push ax bx cx dx
		mov ah,0fh
		int 10h
		mov ah,03h
		int 10h
		inc dl
		mov ah,02h
		int 10h
		pop dx cx bx ax
		ret
move_curs endp
write_key_buf proc
		push ax bx cx dx si di es 
		lea bx,msg19
		call prt_str
		mov bh,1
		mov ah,03h
		int 10h
		mov bx,40h
		mov es,bx
		mov si,1ah
		mov di,1ch
		mov bx,es:[si]
		mov cx,es:[di]
		sub cx,bx
		shr cx,1
		cmp cx,0
		je clear_buf
	c1:	
		push cx dx bx
		mov dx,es:[bx]
		call out_hex
		mov al,' '
		mov cx,1 
		mov bh,1
		mov ah,0ah
		int 10h
		call move_curs
		mov al,dl
		mov cx,1 
		mov bh,1
		mov ah,0ah
		int 10h
		call move_curs
		mov al,' '
		mov cx,1
		mov bh,1
		mov ah,0ah
		int 10h
		call move_curs
		pop bx
		add bx,2
		pop dx
		push bx
		mov bh,1
		inc dh
		mov ah,02h
		int 10h
		pop bx
		pop cx
	loop c1
		dec dh
		add dl,6
		mov ah,02h
		mov bh,1
		int 10h
	clear_buf:
		lea bx,msg20
		call prt_str

		pop es di si dx cx bx ax
		ret
write_key_buf endp
Out_hex	proc
        	push ax bx cx dx
		mov dx,1Fe2h
		mov bx,dx
		mov cx,4
	make_num:
		push cx
		mov cl,12
		shr dx,cl
		pop cx
         	and dx,000fh
         	cmp dx,9
         	jbe met1
         	add dx,37h
         	jmp met2
	met1:    
		add dx,30h
	met2:    
		push bx cx
		mov al,dl
		mov ah,0ah
		mov cx,1
		mov bx,1
		int 10h
		call move_curs
		pop cx bx
    		shl bx,4
		mov dx,bx
		loop make_num
         	pop dx cx bx ax
         ret
Out_hex endp       
close_keyb proc
		push ax bx
		mov ah,3Eh
		mov bx,0
		int 21h
		mov onkeyb,0
		pop bx ax
		ret
close_keyb endp
close_disp proc	
		push ax bx
		mov ah,3Eh
		mov bx,1
		int 21h
		mov ondisp,0
		pop bx ax
		ret
close_disp endp
BIN_on proc
		push ax bx dx
		mov ax, 4400h
		mov bx, 1
		int 21h
		or dx, 20h
		mov ax, 4401h
		mov bx, 1
		xor dh, dh
		int 21h
		mov bin,1
		mov ascii,0
		pop dx bx ax
		ret
BIN_on endp
ASCII_on proc
		push ax bx dx
		mov ax, 4400h
		mov bx, 1
		int 21h
		and dl, 0DCh
		mov ax, 4401h
		mov bx, 1
		xor dh, dh
		int 21h
		mov ascii,1
		mov bin,0
		pop dx bx ax
		ret
ASCII_on endp
read proc	
		mov     ah,00h
        	int     16h
		ret
read endp
clrscr proc
		push ax dx
		mov ax,3
		int 10h
		pop dx ax
		ret
clrscr endp
prt_str proc
        	push ax bx cx dx di
        	mov di,0
	next_sym: 
		mov al,[bx+di]
        	cmp al,'$'
        	je exit_prt
        	cmp al,13
        	jne no_enter
        	cmp byte ptr [bx+di+1],10
        	jne no_enter
		call nline
		add bx,2
		jmp next_sym
	no_enter:	
		push bx ax
		mov ah,0fh
		int 10h
		pop ax
        	mov ah,0ah
        	mov cx,1
        	int 10h
       		inc di
        	pop bx
		call move_curs
        	jmp next_sym
	exit_prt:
        	pop di dx cx bx ax
     		ret
prt_str endp
nline proc 
		push ax bx dx cx
		mov ah,0fh
		int 10h
		mov ah,03h
		int 10h
		inc dh
		mov dl,0
		mov ah,02h
		int 10h
		pop cx dx bx ax
		ret
nline endp
timer proc 
		push ax bx cx
    	   	mov ah,02h       
       		int 1ah          
       		mov time,dh    
delay:
        	mov ah,02h
        	int 1ah
		mov dl,dh
		sub dl,time
		cmp dl,0
		jg not_neg
		neg dl
	not_neg:
       		cmp dl,5
        	jle delay
        	pop cx bx ax
        	ret
timer endp 
new_int21h_off proc
		push bx ax es
		mov my_int21h,0
		xor ax,ax
		mov es,ax
		mov bx,21h
		shl bx,2	 
		cli
		mov ax,int21h_seg
		mov es:[bx+2],ax
		mov ax,int21h_ofs    
		mov es:[bx],ax	
		sti
		pop es ax bx
		ret	
new_int21h_off endp
save_int21h proc
		push ax bx es
		mov ah,35h
		mov al,21h
		int 21h
		mov int21h_seg,es
		mov int21h_ofs,bx
		pop es bx ax
		ret
save_int21h endp 
find_drv proc	
		push ax bx cx es
		mov ah,52h      	  	
	  	int 21h
		add bx,0Ch            	
		mov ax,es:[bx+2]  	  	
		mov cx,es:[bx]		
		mov es,ax
		mov bx,cx
		mov strategy_seg,ax
		mov interrupt_seg,ax
		mov ax,es:[bx+6]
		mov strategy_ofs,ax
		mov ax,es:[bx+8]
		mov interrupt_ofs,ax
		pop es cx bx ax
		ret
find_drv endp
open_drv proc
		push ax cx dx
		mov ah,3Dh
		mov al,2
		mov cx,0
		lea dx,con
		int 21h
		mov onkeyb,1
		mov ondisp,1	
		pop dx cx ax
		ret
open_drv endp
test_07h proc
		push ax bx
		lea bx,msg21
		call prt_str
		mov ah,07h
		int 21h
		lea bx,msg22
		call prt_str
		mov ah,0eh
		int 10h
		pop bx ax
		ret
test_07h endp
test_0ah proc
		push ax bx dx
		lea bx,msg18
		call prt_str
		lea dx,buf_s
		mov ah,0ah
		int 21h 
		mov buf_s[12],'$'
		lea bx,msg22
		call prt_str
		lea bx,buf_s
		add bx,2
		call prt_str
		pop dx bx ax
		ret
test_0ah endp
test_0ch proc
		push ax bx
		lea bx,msg23
		call prt_str
		call timer
		call timer
		mov al,1
		mov ah,05h
		int 10h
	        call write_key_buf
		call nline
	        lea bx,msg21
	        call prt_str
	        mov ah,0ch
		mov al,01h
	        int 21h
		call move_curs
		mov dh,0
		mov dl,40
		mov ah,02h
		mov bh,1
		int 10h
		call write_key_buf
		lea bx,msg24
		call prt_str
		call read
		mov al,0
		mov ah,05h
		int 10h
	        pop bx ax
	        ret	
test_0ch endp
new_int21h_on proc     
		push dx ax ds
		mov my_int21h,1
		mov ax,seg new_int21h	 
		mov dx,offset new_int21h
		mov ds,ax
		mov ax,2521h
		int 21h
		pop ds ax dx
		ret
new_int21h_on endp
new_int21h proc
		cmp ah,07h	
		jne not_07h
		call new_07h
		jmp back
	not_07h:	
		cmp ah,0ah
		jne not_0ah
		call new_0ah
		jmp back
	not_0ah:
		cmp ah,0ch
		jne not_0ch
		call new_0ch
		jmp back
	not_0ch:
	        jmp dword ptr int21h		
	back:
		iret
new_int21h endp
new_07h proc
		push bx cx dx es
		cmp onkeyb,0
		je exit_07h
	not_sym:
		mov bx,offset rh_ndinput
		mov es:[bx].rh_len,14
		mov es:[bx].rh_cmd,5
		mov es:[bx].rh_status,0
		mov es:[bx].rh5_return,-1
		call strategy_drv							
		call interrupt_drv
		cmp es:[bx].rh5_return,-1
		je not_sym
		mov ax,seg rh_input
		mov es,ax
		mov bx,offset rh_input
		mov es:[bx].rh_len,19
		mov es:[bx].rh_cmd,4
		mov es:[bx].rh_status,0
		mov ax,offset sym_07h
		mov es:[bx].rh4_buf_ofs,ax
		mov ax,seg sym_07h
		mov es:[bx].rh4_buf_seg,ax
		mov es:[bx].rh4_count,1
		call strategy_drv							
		call interrupt_drv
		mov al,sym_07h
	exit_07h:
		pop es dx cx bx
		ret
new_07h endp
new_0ah proc
		push bx cx dx si di es
		
		mov si,seg buf_s
		mov di,offset buf_s
		mov l_0ah,0
		inc di
	get_sym:
		mov ax,seg rh_ndinput
		mov es,ax
		mov bx,offset rh_ndinput
		mov es:[bx].rh_len,14
		mov es:[bx].rh_cmd,5
		mov es:[bx].rh_status,0
		call strategy_drv							
		call interrupt_drv
		cmp onkeyb,0
		jne not_keyb 
		jmp exit_0ah
	not_keyb:
		cmp es:[bx].rh_status,0200h
		je get_sym
		mov ax,seg rh_input
		mov es,ax
		mov bx,offset rh_input
		inc di
		mov es:[bx].rh_len,19
		mov es:[bx].rh_cmd,4
		mov es:[bx].rh_status,0
		mov es:[bx].rh4_count,1	
		mov es:[bx].rh4_buf_ofs,di
		mov es:[bx].rh4_buf_seg,si
		call strategy_drv							
		call interrupt_drv
		mov al,es:[di]
		cmp al,3
		jne not_break
		out_drv msgc,2
		mov ax,4c00h
		int 21h
	not_break:
		cmp al,13
		jne not_e
		mov al,l_0ah
		mov buf_s[1],al
		jmp exit_0ah
	not_e:
		cmp al,8
		je sym_bs
		jmp not_bs
	sym_bs:
		cmp l_0ah,0
		jne not_first
		jmp get_sym
	not_first:
		out_drv bs,1
		out_drv space,1
		out_drv bs,1
		dec l_0ah
		jmp get_sym
	not_bs:
		cmp al,27
		jne not_esc
		cmp ondisp,1
		je disp_on2
		jmp get_sym
	disp_on2:
		out_drv mesc,1
		out_drv ent,2
		jmp get_sym
	not_esc:
		mov ah,buf_s[0]
		dec ah
		cmp l_0ah,ah
		je ring
		cmp ondisp,1
		je disp_on1
		jmp get_sym
	disp_on1:
		inc l_0ah
		mov bl,l_0ah
		mov bh,0
		mov ah,buf_s[bx+1]
		mov sym_0ah,ah
		out_drv sym_0ah,1
		jmp get_sym		
	ring:	
		out_drv dz,1
		mov ah,00h
		int 16h
		cmp al,8
		jne ch_enter
		jmp sym_bs
	ch_enter:
		cmp al,0Dh
		jne ring
		jmp get_sym
	exit_0ah:
		pop es di si dx cx bx
		ret
new_0ah endp
new_0ch proc
		push bx cx dx es
		cmp onkeyb,0
		jne nt_keyb
		jmp exit_0ch
	nt_keyb:
		mov bx,offset rh_ndinput
		mov es:[bx].rh_len,14
		mov es:[bx].rh_cmd,5
		mov es:[bx].rh_status,0
		mov es:[bx].rh5_return,-1
		call strategy_drv							
		call interrupt_drv
		mov ax,seg rh_flinput
		cmp es:[bx].rh5_return,-1
		je nt_keyb
		mov es,ax
		mov bx,offset rh_flinput
		mov es:[bx].rh_len,19
		mov es:[bx].rh_cmd,7
		mov es:[bx].rh_status,0
		call strategy_drv							
		call interrupt_drv		
		mov ax,seg rh_input
		mov es,ax
		mov bx,offset rh_input
		mov es:[bx].rh_len,19
		mov es:[bx].rh_cmd,4
		mov es:[bx].rh_status,0
		mov ax,offset sym_07h
		mov es:[bx].rh4_buf_ofs,ax
		mov ax,seg sym_07h
		mov es:[bx].rh4_buf_seg,ax
		mov es:[bx].rh4_count,1
		call strategy_drv							
		call interrupt_drv
		mov al,sym_07h
		out_drv sym_07h,1
	exit_0ch:
		pop es dx cx bx
		ret
new_0ch endp
cseg ends
end main