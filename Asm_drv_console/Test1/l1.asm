extrn dev_interrupt:far,dev_strategy:far;
dseg segment "data"
;�������� ��������� �����
	msg1 db '1. ���樠������',13,10,'$'
	msg2 db '2. ���� ',13,10,'$'
	msg3 db '3. ���⪠ ����� ',13,10,'$'
	msg4 db '4. ��室 ',13,10,'$'
	msg5 db '������ �������: $'
	msg6 db '����ୠ� �������. ������ ����!$'
	msgs dw msg1,msg2,msg3,msg4,msg5
	msg7 db '���� �����: $' 
	msg8 db '����ন��� �����: ',13,10,'$'
	msg9 db 'rh_len = $'
	msg10 db 'rh_unit = $'
	msg11 db 'rh_cmd = $'
	msg12 db 'rh_status = $'
	msg13 db '������ 5 ᨬ�����: $'
	msg14 db '�� �����: $'
	msg15 db '���� ���� �ࠩ���: $' 
	msg16 db '���� DTA: $'
	msg17 db 'rh4_count = $'
	msg18 db '������ ᨬ���� � �祭�� 5 ᥪ㭤: $'
	msg19 db 'Head>$'
	msg20 db '<Tail$'
	msg21 db '�� ���⪨: ',13,10,'$'
	msg22 db '��᫥ ���⪨: ',10,'$'
	inputS db 6 dup(?) 
	time db ?
	rh 		struc
	rh_len		db	?	;����� �����				|
	rh_unit		db	?	;����� ���ன�⢠ (�������)		|䨪�஢����� ����
	rh_cmd		db	?	;������� �ࠩ��� ���ன�⢠		|
	rh_status  	dw	?	;�����頥��� �ࠩ��஬			|
	rh		ends

	rh0		struc		;��������� ����� ��� ������� 0
	rh0_rh	rh    <17,0,0,0>	;䨪�஢����� ����
	rh0_brk_ofs	dw	?	;���饭�� ��� ����
	rh0_brk_seg	dw	?	;������� ��� ����
	rh0		ends

	rh4		struc		;3�������� ����� ��� ������� 4
	rh4_rh  rh     <20,0,4,0>	;䨪�஢����� ����
	rh4_media	db	?	;���ਯ�� ���⥫� �� DPB
	rh4_buf_ofs	dw	inputS	;���饭�� DTA
	rh4_buf_seg	dw	?	;������� DTA
	rh4_count	dw	5	;���稪 ��।�� (ᥪ�஢ ��� ��������, ���⮢ ��� ᨬ������)
	rh4		ends

	rh7		struc		;��������� ����� ��� ������� 7
	rh4_rh  rh     <13,0,7,0>	;䨪�஢����� ����
	rh7		ends
	
	rh_init rh0 1 dup(<>) 
	rh_input rh4 1 dup(<>) 
	rh_flinput rh7 1 dup(<>) 

dseg ends
sseg segment stack
	dw 256 dup (?)
sseg ends
cseg segment "code"
main proc far
		assume cs:cseg, ds:dseg, ss:sseg
		push ds
		xor ax,ax
		push ax
		mov ax,dseg
		mov ds,ax
		
	begin:
		call clrscr
		mov cx,5
		mov si,0
	menu:
		mov ah,09h
		mov dx,msgs[si]
		call prt_str
		add si,2
		loop menu
		call read
		call nline
		sub al,30h
		cmp al,1
		jl err1
		cmp al,4
		jg err1
		jmp noerr1
	err1:
		call nline
		lea dx,msg6
		call prt_str
		mov ah,08h
		int 21h
		jmp begin
	noerr1:
		cmp al,1
		jne n2
		call nline
		mov bx,seg rh_init
  		mov es,bx
  		lea bx,rh_init
		call dev_strategy
		call dev_interrupt
		call nline
		lea dx,msg7
		call prt_str
		call write_adr
		call write_rh
		lea dx,msg15
		call prt_str
		push bx es
		mov bx,rh_init.rh0_brk_ofs
		mov ax,rh_init.rh0_brk_seg
		mov es,ax
		call write_adr
		pop es bx
		mov ah,08h
		int 21h
		jmp begin
	n2:
		cmp al,2
		jne n3
		call nline
		mov bx,seg rh_input
  		mov es,bx
  		lea bx,rh_input
		lea dx,msg13
		call prt_str
		mov rh_input.rh4_buf_seg,ds
		call dev_strategy
		call dev_interrupt
		call nline
		lea dx,msg14
		call prt_str
		mov inputS[5],'$'
		lea dx,inputS
		call prt_str
		call nline			
		lea dx,msg7
		call prt_str
		call write_adr
		call write_rh
		lea dx,msg17
		call prt_str
		mov dx,es:[bx].rh4_count
		call write_dec
		call nline
		lea dx,msg16
		call prt_str
		push bx es
		mov bx,rh_input.rh4_buf_ofs
		mov ax,rh_input.rh4_buf_seg
		mov es,ax
		call write_adr
		pop es bx				
		mov ah,08h
		int 21h
		jmp begin

	n3:	
		cmp al,4
		je exit	
		call nline
		mov bx,seg rh_flinput
  		mov es,bx
  		lea bx,rh_flinput
		call dev_strategy
		lea dx,msg18
		call prt_str
		call timer
		call nline
		mov ah,05h
		mov al,1
		int 10h
		lea dx,msg21
		call prt_str
		call write_key_buf
		call dev_interrupt
		call timer
		mov dh,0
		mov dl,40
		mov ah,02h
		mov bh,1
		int 10h
		lea dx,msg22
		call prt_str
		mov dh,1
		mov dl,40
		mov ah,02h
		mov bh,1
		int 10h
		call write_key_buf
		call timer
		call timer
		mov ah,08h
		int 21h
		mov ah,05h
		mov al,0
		int 10h
		call nline
		lea dx,msg7
		call prt_str
		call write_adr
		call write_rh
		mov ah,08h
		int 21h
		jmp begin
	exit:
     	ret
main endp
write_key_buf proc

		push ax bx cx dx si di es 
		lea dx,msg19
		call prt_str
		mov bh,1
		mov ah,03h
		int 10h
		mov bx,40h
		mov es,bx
		mov si,1ah
		mov di,1ch
		mov bx,es:[si]
		mov cx,es:[di]
		sub cx,bx
		shr cx,1
		cmp cx,0
		je clear_buf
	c1:	
		push dx
		mov dx,es:[bx]
		push dx	
		call out_hex
		mov dl,' '
		mov ah,06h
		int 21h
		pop dx
		mov ah,06h
		int 21h
		mov dl,' '
		mov ah,06h
		int 21h
		add bx,2
		pop dx
		push bx
		mov bh,1
		inc dh
		mov ah,02h
		int 10h
		pop bx
	loop c1
		dec dh
		add dl,6
		mov ah,02h
		mov bh,1
		int 10h
	clear_buf:
		lea dx,msg20
		call prt_str

		pop es di si dx cx bx ax
		ret
write_key_buf endp

write_rh proc
	push dx
	lea dx,msg8
	call prt_str
	lea dx,msg9
	call prt_str
	mov dx,0
	mov dl,es:[bx].rh_len
	call Out_hex
	call nline
	lea dx,msg10
	call prt_str
	mov dx,0
	mov dl,es:[bx].rh_unit
	call Out_hex
	call nline
	lea dx,msg11
	call prt_str
	mov dx,0
	mov dl,es:[bx].rh_cmd
	call Out_hex
	call nline
	lea dx,msg12
	call prt_str
	mov dx,es:[bx].rh_status
	call Out_hex
	call nline
	pop dx
	ret
write_rh endp
write_adr proc
	push dx 
	mov dx,es
	call out_hex
	mov ah,06h
	mov dl,':'
	int 21h
	mov dx,bx
	call out_hex
	call nline
	pop dx
	ret
write_adr endp
read proc
		mov ah,01h
		int 21h
		ret
read endp
clrscr proc
		push ax dx
		mov ax,3
		int 10h
		pop dx ax
		ret
clrscr endp
prt_str proc
		push ax dx
		mov ah,09h
		int 21h
		pop dx ax
		ret
prt_str endp
nline proc 
		push ax dx
		mov ah,06h
     		mov dl,0dh
     		int 21h
     		mov dl,0ah
     		int 21h
		pop dx ax
		ret
nline endp
timer proc 
		push ax bx dx cx
    	   	mov ah,2ch       
       		int 21h          
       		mov time,dh    
delay:
        	mov ah,2ch
        	int 21h
		mov dl,dh
		sub dl,time
		cmp dl,0
		jg not_neg
		neg dl
	not_neg:
       		cmp dl,5
        	jle delay
        	pop cx dx bx ax
        	ret
timer endp
write_dec proc
		push ax bx cx dx
		mov cx, 0
		mov ax, dx
get_next:
		mov dx, 0
		mov bx, 10
		div bx
		push dx
		inc cx
		cmp ax, 0
		jne get_next
write_next:
		mov ah, 06h
		pop dx
		add dx, 30h
		int 21h
		loop write_next
		pop dx cx bx ax
		ret
write_dec endp
Out_hex	proc near
        	push ax bx cx dx

		mov bx,dx
		mov cx,4
	make_num:
		push cx
		mov cl,12
		shr dx,cl
		pop cx
         	and dx,000fh
         	cmp dx,9
         	jbe met1
         	add dx,37h
         	jmp met2
	met1:    
		add dx,30h
	met2:    
		mov ah,06h
		int 21h
    		shl bx,4
		mov dx,bx
		loop make_num
         	pop dx cx bx ax
         ret
Out_hex endp       
cseg ends
end main