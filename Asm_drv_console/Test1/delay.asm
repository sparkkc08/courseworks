dseg   segment
		msg1 db "������1 $"
		msg2 db "������2 $"
		time db 0
dseg   ends
cseg   segment
lab1    proc     far
		assume cs:cseg,ds:dseg
		push ds
		xor ax,ax
		push ax
		mov ax,dseg
		mov ds,ax
		
		
		lea dx,msg1
		mov ah,09h
		int 21h
		call timer
		lea dx,msg2
		mov ah,09h
		int 21h
		ret
lab1 endp 
proc timer
		push ax bx dx
        mov ah,2ch       
        int 21h          
        mov time,dh    
delay:
        mov ah,2ch
        int 21h
		mov dl,dh
		sub dl,time
        cmp dl,5
        jle delay
        pop dx bx ax
        ret
timer endp        
cseg    ends
end     lab1

