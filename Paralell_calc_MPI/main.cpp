#include <stdio.h>
#include <math.h>
#include <mpi.h>

void AddMatrix(double* MatrixA,double* MatrixB,double* MatrixC,int Size,int _2size,bool Add);
void MultMatrix(double* MatrixA,double* MatrixB,double* MatrixC,int m,int n,int l);
void PrintMatrix(double* Matrix,int m,int n);
int inv_matrix(double *a,int n,double *m2);
int main_seq(int argc, char * argv[]);
int main_paral(int argc, char * argv[]);

int main(int argc, char * argv[])
{
	MPI_Init(&argc,&argv);
	
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);
	double start_seq,start_par,end_seq,end_par;
	if(rank == 0)
	{
		start_seq = MPI_Wtime();
		main_seq(argc,argv);
		end_seq = MPI_Wtime();
	}
	MPI_Barrier(MPI_COMM_WORLD);
	start_par = MPI_Wtime();
	main_paral(argc,argv);
	end_par = MPI_Wtime();
	if(rank == 0)
	{
		printf("Sequential program time: %f\nParallel program time:  %f\n", end_seq - start_seq, end_par - start_par);
	}
	MPI_Finalize();
	return 0;
}

int main_seq(int argc, char * argv[])
{
	FILE ** files = new FILE*[10];
	char * file_name = "var";
	char * next_name = new char [10];
	int counter=0;
	long iter = 0;
	
	
	double S[6*10]= { 1.0,  0.0,  0.0,  1.0,  1.0,  0.0,  0.0,  0.0,  0.0,  0.0,
					1.0,  1.0,  0.0,  1.0,  0.0,  1.0,  0.0,  0.0,  0.0,  0.0,
					1.0,  1.0,  0.0,  1.0,  0.0,  0.0,  1.0,  0.0,  0.0,  0.0,
					0.0,  0.0, -1.0,  0.0,  0.0,  0.0,  0.0,  1.0,  0.0,  0.0,
					1.0,  1.0,  1.0,  0.0,  0.0,  0.0,  0.0,  0.0,  1.0,  0.0,
					1.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  1.0}; 

	double R[100] = {1.15, 0,    0,    0,    0,    0,    0,    0,    0,    0,
					 0,    3.53, 0,    0,    0,    0,    0,    0,    0,    0,
					 0,    0,    2.17, 0,    0,    0,    0,    0,    0,    0,
					 0,    0,    0,    2.32, 0,    0,    0,    0,    0,    0,
					 0,    0,    0,    0,    1.14, 0,    0,    0,    0,    0,
					 0,    0,    0,    0,    0,    1.677,0,    0,    0,    0,
					 0,    0,    0,    0,    0,    0,    1.98, 0,    0,    0,
					 0,    0,    0,    0,    0,    0,    0,    2.12, 0,    0,
					 0,    0,    0,    0,    0,    0,    0,    0,    2.35, 0,
					 0,    0,    0,    0,    0,    0,    0,    0,    0,    1.17};

	double TP[6*10];

	
	MultMatrix(R,S,TP,6,6,10);
	printf("Matrix TP=\n");
	PrintMatrix(TP,6,10);
	
	return 0;
}

int main_paral(int argc, char * argv[])
{
	
	double Ax[4*4]= {1.0, -1.0,  0.0,  0.0,
			 0.0,  0.0,  0.0, -1.0,
			 0.0,  1.0, -1.0,  0.0,
			 0.0,  0.0,  1.0,  0.0};
	
	double Ay[4*6]= {-1.0,  0.0,  0.0,  0.0,  0.0, -1.0,
			  1.0,  1.0,  1.0,  0.0,  0.0,  0.0,
			  0.0, -1.0, -1.0, -1.0,  0.0,  0.0,
			  0.0,  0.0,  0.0,  1.0, -1.0,  0.0};
	
	double Sx[6*4]= {1.0,  0.0,  0.0,  1.0,
					1.0,  1.0,  0.0,  1.0,
					1.0,  1.0,  0.0,  1.0,
					0.0,  0.0, -1.0,  0.0,
					1.0,  1.0,  1.0,  0.0,
					1.0,  0.0,  0.0,  0.0};
	
	double Sy[6*6]= { 1.0,  0.0,  0.0,  0.0,  0.0,  0.0,
					 0.0,  1.0,  0.0,  0.0,  0.0,  0.0,
					 0.0,  0.0,  1.0,  0.0,  0.0,  0.0,
					 0.0,  0.0,  0.0,  1.0,  0.0,  0.0,
					 0.0,  0.0,  0.0,  0.0,  1.0,  0.0,
					 0.0,  0.0,  0.0,  0.0,  0.0,  1.0};
	
	double Kx[4*4]= {140.0,   0.0,  0.0,  0.0, 
					  0.0,  95.0,  0.0,  0.0,
					  0.0,   0.0, 74.0,  0.0,
					  0.0,   0.0,  0.0, 52.0};
	
	double Ky[6*6]= {110.0,   0.0,   0.0,   0.0,   0.0,   0.0,
					  0.0,  85.0,   0.0,   0.0,   0.0,   0.0,
					  0.0,   0.0,  79.0,   0.0,   0.0,   0.0,
					  0.0,   0.0,   0.0,  77.0,   0.0,   0.0,
					  0.0,   0.0,   0.0,   0.0,  75.0,   0.0,
					  0.0,   0.0,   0.0,   0.0,   0.0,   130.0};
	
	double S[6*10]= { 1.0,  0.0,  0.0,  1.0,  1.0,  0.0,  0.0,  0.0,  0.0,  0.0,
			 1.0,  1.0,  0.0,  1.0,  0.0,  1.0,  0.0,  0.0,  0.0,  0.0,
					1.0,  1.0,  0.0,  1.0,  0.0,  0.0,  1.0,  0.0,  0.0,  0.0,
					0.0,  0.0, -1.0,  0.0,  0.0,  0.0,  0.0,  1.0,  0.0,  0.0,
					1.0,  1.0,  1.0,  0.0,  0.0,  0.0,  0.0,  0.0,  1.0,  0.0,
					1.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  0.0,  1.0}; 


	double Y[6]={0.001,0.001,0.001,0.001,0.001,0.001};
	double X[4]={0.001,0.001,0.001,0.001};
	double h = 0.0001;
	double e = 0.000001;
	double Z[10]={0,0,0,0,0,0,0,0,0,0};
	double H[10] = {4000,0,0,0,0,0,0,0,0,0};
	double R[100] = {1.15, 0,    0,    0,    0,    0,    0,    0,    0,    0,
					 0,    3.53, 0,    0,    0,    0,    0,    0,    0,    0,
					 0,    0,    2.17, 0,    0,    0,    0,    0,    0,    0,
					 0,    0,    0,    2.32, 0,    0,    0,    0,    0,    0,
					 0,    0,    0,    0,    1.14, 0,    0,    0,    0,    0,
					 0,    0,    0,    0,    0,    1.677,0,    0,    0,    0,
					 0,    0,    0,    0,    0,    0,    1.98, 0,    0,    0,
					 0,    0,    0,    0,    0,    0,    0,    2.12, 0,    0,
					 0,    0,    0,    0,    0,    0,    0,    0,    2.35, 0,
					 0,    0,    0,    0,    0,    0,    0,    0,    0,    1.17};

	double F[6] = {0,0,0,0,0,0};
	double buf10[10] = {0,0,0,0,0,0,0,0,0,0};
	double tmp[10] = {0.001,0.001,0.001,0.001,0.001,0.001,0.001,0.001,0.001,0.001};
	double Buf[4*4];
	double Buf_[6*6];
	double Buf1[6*4];
	double Buf2[6*6];
	double buf3[6*6];
	double W[4*6];
	double TP[6*10];
	

	int counter=0;
	double buf60[60];
	double buf_6[6];
	double buf_6_2[6];
	
	
	int rank;
	MPI_Comm_rank(MPI_COMM_WORLD,&rank);	

	if(rank == 0)
	{
		inv_matrix(Ax,4,Buf);
		MultMatrix(Buf,Ay,W,4,4,6);
		MPI_Send(W,24,MPI_DOUBLE,1,1,MPI_COMM_WORLD);
		MPI_Status stat;
		MPI_Recv(TP,60,MPI_DOUBLE,1,10,MPI_COMM_WORLD,&stat);
		MultMatrix(TP,H,F,6,10,1);
		MPI_Send(F,6,MPI_DOUBLE,1,3,MPI_COMM_WORLD);
	}
	else if(rank == 1)
	{
		MultMatrix(Sy,Ky,Buf_,6,6,6);
		MultMatrix(Sx,Kx,Buf1,6,4,4);
		MPI_Status stat;
		MPI_Recv(W,24,MPI_DOUBLE,0,1,MPI_COMM_WORLD,&stat);
		MultMatrix(Buf1,W,Buf2,6,4,6);
		AddMatrix(Buf_,Buf2,buf3,6,6,false);
		inv_matrix(buf3,6,Buf2);
		MultMatrix(Buf2,S,TP,6,6,10);
		MPI_Send(TP,60,MPI_DOUBLE,0,10,MPI_COMM_WORLD);
		MultMatrix(TP,R,buf60,6,10,10);		
		MPI_Recv(F,6,MPI_DOUBLE,0,3,MPI_COMM_WORLD,&stat);
	}
		
	
	char stateX = 0,stateY;
	char res;
	MPI_Status stat;
	while(true)
	{
		if(rank == 0)
		{
			tmp[0] = X[0];
			tmp[1] = X[1];
			tmp[2] = X[2];
			tmp[3] = X[3];
			
			for (int i=0;i<4;++i)
			{
				Z[i] = X[i]*fabs(X[i]);
			}
			
			MPI_Send(Z,4,MPI_DOUBLE,1,2,MPI_COMM_WORLD);
			
			MPI_Recv(Y,6,MPI_DOUBLE,1,4,MPI_COMM_WORLD,&stat);
			
			MultMatrix(W,Y,X,4,6,1);
			
			for (int i=0;i<4;++i)
			{
				X[i] = -X[i];
			}

			for (int i=0;i<4;++i)
			{
				if( fabs(tmp[i] - X[i]) < e)
				{
					stateX = 1;
				}
				else
				{
					stateX = 0;
					break;
				}
			}

			
			MPI_Recv(&stateY,1,MPI_CHAR,1,5,MPI_COMM_WORLD,&stat);
			res = stateX && stateY;
			MPI_Send(&res,1,MPI_CHAR,1,6,MPI_COMM_WORLD);
			if(res)
			{
				printf("Result Vector is:\n");
				PrintMatrix(tmp,1,4);
				printf("\n->Number of Steps:%d\n",counter);
				break;
			}
			++counter;
		}
		else if(rank == 1)
		{
			tmp[4] = Y[0];
			tmp[5] = Y[1];
			tmp[6] = Y[2];
			tmp[7] = Y[3];
			tmp[8] = Y[4];
			tmp[9] = Y[5];
			
			for (int i=4;i<10;++i)
			{
				Z[i] = Y[i-4]*fabs(Y[i-4]);
			}
			
			//MPI_Status stat;
			MPI_Recv(Z,6,MPI_DOUBLE,0,2,MPI_COMM_WORLD,&stat);
			
			MultMatrix(buf60,Z,buf_6,6,10,1);
			AddMatrix(F,buf_6,buf_6_2,6,1,false);

			for(int i=0;i<6;++i)
			{
				Y[i] = tmp[i+4] + h*buf_6_2[i];
			}

			MPI_Send(Y,6,MPI_DOUBLE,0,4,MPI_COMM_WORLD);
			
			for(int i=4;i<10;++i)
			{
				if( fabs(tmp[i] - Y[i-4]) < e)
				{
					stateY = 1;
				}
				else
				{
					stateY = 0;
					break;
				}
			}
			MPI_Send(&stateY,1,MPI_CHAR,0,5,MPI_COMM_WORLD);

			MPI_Recv(&res,1,MPI_CHAR,0,6,MPI_COMM_WORLD,&stat);
			if(res)
			{
				PrintMatrix(tmp+4,1,6);
				break;
			}
		}
	}
	return 0;
}

void AddMatrix(double* MatrixA,double* MatrixB,double* MatrixC,int Size,int _2size,bool Add)
{	
	for(int i=0;i<Size*_2size;i++)
	{
		if(Add)
		{
			MatrixC[i]=MatrixA[i]+MatrixB[i];
		}
		else
		{
			MatrixC[i]=MatrixA[i]-MatrixB[i];
		}
	}
}

void MultMatrix(double* MatrixA,double* MatrixB,double* MatrixC,int m,int n,int l)
{
	for (int i=0; i<m; i++)
	{
		for (int j=0; j<l; j++)
		{
			MatrixC[i*l+j] = 0;
			for (int k=0; k<n; k++)
			{
				MatrixC[i*l+j] = MatrixC[i*l+j] + MatrixA[i*n+k]*MatrixB[k*l+j];
			}
		}
	}
}

void PrintMatrix(double* Matrix,int m,int n)
{
	int k=0;
	for(int i=0;i<m;i++)
	{
		for(int j=0;j<n;j++,k++)
		{
			if(Matrix[k] == -0)
			{
				Matrix[k] = 0;
			}
		}
	}
	k=0;
	for(int i=0;i<m;i++)
	{
		for(int j=0;j<n;j++,k++)
		{
			printf("%9.5f ",Matrix[k]);
		}
		printf("\n");
	}
	printf("\n");
}

int inv_matrix(double *a,int n,double *m2)
{
	for(int i=0;i<n*n;i++)
	{
		*(m2+i)=*(a+i);          
	}

	double e1,d,y,w,p;
	double *b,*c;
	int j,k;
	int *z;

	e1=1.e-6;
	d=1;

	z= new int[n];
	c= new double[n];
	b= new double[n];

	for(int i=0; i<n; i++)
	{
		*(z+i)=i;
	}

	for(int i=0; i<n; i++)
	{
		k=i;
		y=*(m2+i*n+i);

		if(i+1 <= n )
		{
			for(j=1+i; j<n; j++)
			{
				w=*(m2+n*i+j);
				if(fabs(w)>fabs(y))
				{
					k=j;
					y=w;
				}
			}
		}
		d=d*y;

		if(fabs(y)<e1)
		{
			return 2;
		}
		y=1./y;

		for(j=0; j<n; j++)
		{
			*(c+j)=*(m2+n*j+k);
			*(m2+n*j+k)=*(m2+n*j+i);
			*(m2+j*n+i)=-(*(c+j))*y;
			*(b+j)=*(m2+i*n+j)*y;
			*(m2+i*n+j)=*(b+j);
		}
		j=*(z+i);
		*(z+i)=*(z+k);
		*(z+k)=j;
		*(m2+i*n+i)=y;

		for(k=0; k<n; k++)
		{
			if(k != i)
			{
				for(j=0; j<n; j++)
				{
					if(j != i)
					{
						*(m2+k*n+j)=*(m2+k*n+j)-(*(b+j))*(*(c+k));
					}
				}
			}
		}
	}

	for(int i=0; i<n; i++)
	{
		while(1)
		{
			k=*(z+i);
			if(k == i) 
			{
				break;
			}
			for(j=0; j<n; j++)
			{
				w=*(m2+i*n+j);
				*(m2+i*n+j)=*(m2+k*n+j);
				*(m2+k*n+j)=w;
			}
			p=*(z+i);
			*(z+i)=*(z+k);
			*(z+k)=(int)p;
			d=-d;
		}
	}
	delete[] z;
	delete[] b;
	delete[] c;
	
	return 0;
}
