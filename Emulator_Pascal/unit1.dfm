object Mymenu: TMymenu
  Left = 442
  Top = 229
  BorderStyle = bsSingle
  Caption = #1052#1080#1088#1086#1085#1102#1082
  ClientHeight = 268
  ClientWidth = 541
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object LNoF: TLabel
    Left = 32
    Top = 24
    Width = 132
    Height = 16
    Caption = #1042#1074#1077#1076#1080#1090#1077' '#1080#1084#1103' '#1092#1072#1081#1083#1072':'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    Visible = False
  end
  object Exit: TButton
    Left = 440
    Top = 224
    Width = 60
    Height = 25
    Caption = #1042#1099#1093#1086#1076' =>'
    TabOrder = 0
    OnClick = ExitClick
  end
  object Outmain: TMemo
    Left = 56
    Top = 72
    Width = 441
    Height = 129
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    ReadOnly = True
    TabOrder = 1
    Visible = False
  end
  object InFile: TEdit
    Left = 192
    Top = 24
    Width = 185
    Height = 24
    BiDiMode = bdLeftToRight
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -15
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    MaxLength = 25
    ParentBiDiMode = False
    ParentFont = False
    TabOrder = 2
    Visible = False
  end
  object CheckBut: TButton
    Left = 416
    Top = 24
    Width = 99
    Height = 25
    Caption = #1055#1088#1086#1074#1077#1088#1082#1072' '#1092#1072#1081#1083#1072
    Default = True
    TabOrder = 3
    Visible = False
    OnClick = CheckButClick
  end
  object Clear: TButton
    Left = 32
    Top = 224
    Width = 75
    Height = 25
    Caption = #1054#1095#1080#1089#1090#1080#1090#1100' '
    TabOrder = 4
    Visible = False
    OnClick = ClearClick
  end
  object BacktoMain: TButton
    Left = 368
    Top = 224
    Width = 60
    Height = 25
    Caption = '<= '#1053#1072#1079#1072#1076
    TabOrder = 5
    OnClick = BacktoMainClick
  end
  object OutReg: TStringGrid
    Left = 16
    Top = 24
    Width = 265
    Height = 174
    ColCount = 2
    DefaultRowHeight = 18
    RowCount = 9
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
    TabOrder = 6
    Visible = False
    ColWidths = (
      64
      195)
  end
  object OutRK: TStringGrid
    Left = 288
    Top = 24
    Width = 241
    Height = 98
    ColCount = 2
    DefaultRowHeight = 18
    TabOrder = 7
    Visible = False
    ColWidths = (
      64
      173)
  end
  object MainMenu1: TMainMenu
    Left = 552
    Top = 240
    object N1: TMenuItem
      Caption = #1047#1072#1075#1088#1091#1079#1082#1072' '#1090#1077#1089#1090#1072
      object N11: TMenuItem
        Caption = #1058#1077#1089#1090' 1'
        OnClick = N11Click
      end
      object N21: TMenuItem
        Caption = #1058#1077#1089#1090' 2'
        OnClick = N21Click
      end
      object N31: TMenuItem
        Caption = #1058#1077#1089#1090' 3'
        OnClick = N31Click
      end
    end
    object N2: TMenuItem
      Caption = #1055#1086#1083#1085#1099#1081' '#1079#1072#1087#1091#1089#1082
      Enabled = False
      ShortCut = 120
      OnClick = N2Click
    end
    object N3: TMenuItem
      Caption = #1055#1086#1096#1072#1075#1086#1074#1099#1081' '#1079#1072#1087#1091#1089#1082
      Enabled = False
      ShortCut = 119
      OnClick = N3Click
    end
    object Mydump: TMenuItem
      Caption = 'Dump '#1087#1072#1084#1103#1090#1080
      Enabled = False
      OnClick = MydumpClick
    end
    object N4: TMenuItem
      Caption = #1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1100#1085#1099#1081' '#1088#1077#1078#1080#1084
      OnClick = N4Click
    end
  end
end
