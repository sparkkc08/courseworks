unit Work;

interface

Uses DesUnit, DumpUnit ;


Procedure ModuleRead;
Procedure LoadTest;
Function Make2to8(sbin:string22):string8;
Function Make2to10(sbin:string22):string8;
Function Make8to2(soct:string8):string22;
Procedure FromString22ToNumber(Operand:string22;Var Number:longint);
Procedure FromNumberToString22(Number:longint;Var Operand:string22);
Procedure FromString8ToNumber(Operand:string8;Var Number:byte);
Procedure FromNumberToString8(Number:byte;Var Operand:string8);
Procedure FromString4ToNumber(Operand:string4;Var Number:byte);
implementation

Uses unit1,ActUnit;



{----------------------------------------------------------------------}


//���� ����� ����������� ���������
Procedure ModuleRead;
Var
    k    : byte;
    s    : string8;
begin
  Readln(ResFile,SAK);
  k:=SAK;
  StartAddress := SAK;
 While not eof(ResFile) do
   begin
     Readln(ResFile,s);
     Memory[k]:=Make8to2(s);
   Inc(k);
  end;
 EndAddress := k-1;
end; {ReadModule}
{----------------------------------------------------------------------}

Procedure LoadTest;
var i : byte;
Begin

 {$i-}
   AssignFile(ResFile,FileName);
   Reset(ResFile);
 {$i+}
   if IOResult=0 then
     begin
        for i := 0 to 255 do
          Memory[i]:='0000000000000000000000';
        ModuleRead;
     end;
End; {LoadTest}
{----------------------------------------------------------------------}
 Function Make2to8(sbin:string22):string8;
 Var i:byte;
     sbuf:string3;
     soct:string8;
 Begin
    soct:='';
    i:=length(sbin);
  While i>1 do
      begin
        sbuf := Copy(sbin,i-2,3);
        if sbuf='000' then soct:='0'+soct;
        if sbuf='001' then soct:='1'+soct;
        if sbuf='010' then soct:='2'+soct;
        if sbuf='011' then soct:='3'+soct;
        if sbuf='100' then soct:='4'+soct;
        if sbuf='101' then soct:='5'+soct;
        if sbuf='110' then soct:='6'+soct;
        if sbuf='111' then soct:='7'+soct;
        i:=i-3;
     end;
 if sbin[1]='1' then
    soct:='7'+soct;
 While length(soct)<8 do
    soct:='0'+soct;
 While length(soct)>8 do
    Delete(soct,1,1);
 Make2to8:=soct;
 End; {Make2to8}
 {----------------------------------------------------------------------}
 Function Make2to10(sbin:string22):string8;
 Var i,j:byte;
     sdec:string8;
     num, decim:integer;
 Begin
     num:=0;  decim:=0;  sdec:='';
   for i:=length(sbin) downto 1 do
     Begin
       num:=0;
       if sbin[i]='1' then
         Begin
           num:=1;
           for j:=length(sbin)-i  downto 1 do
             num:=2*num;
         end;
       decim:=num+decim;
     end;
    Str(decim, sdec);
    Make2to10:=sdec;
 End {Make2to10};
 {----------------------------------------------------------------------}
 Function Make8to2(soct:string8):string22;
 Var    i :byte;
     sbin :string25;
 Begin
   i:=length(soct);
   sbin:='';
  While i>=1 do
    begin
      if soct[i]='0' then sbin := '000' + sbin ;
      if soct[i]='1' then sbin := '001' + sbin ;
      if soct[i]='2' then sbin := '010' + sbin ;
      if soct[i]='3' then sbin := '011' + sbin ;
      if soct[i]='4' then sbin := '100' + sbin ;
      if soct[i]='5' then sbin := '101' + sbin ;
      if soct[i]='6' then sbin := '110' + sbin ;
      if soct[i]='7' then sbin := '111' + sbin ;
      dec(i);
    end;
 While length(sbin)<22 do
  sbin:='0'+sbin;
 While length(sbin)>22 do
  Delete(sbin,1,1);
 Make8to2:=sbin;
 End; {Make8to2}
{----------------------------------------------------------------------}
 Procedure FromString22ToNumber(Operand:string22;Var Number:longint);
{ �������������� ������ � ����� }
Var   i : byte;
   Cond : boolean;  { ������� �������������� ����� }
Begin
  Cond:=false;
  If Operand[1]='1' then
    Begin
      Cond:=true;
      For i:=1 to 22 do        { �������������� }
        If Operand[i]='1' then { ���� ����������-}
          Operand[i]:='0'      { ���� ��������� }
        Else                   { ����� }
          Operand[i]:='1';
    End;
  Number:=0;                  { ��������� ������- }
  For i:=2 to 22 do           { ���� �������� �� }
    Begin                     { ����� ������� }
      Number:=2*Number;
      If Operand[i]='1' then
        Inc(Number);
    End;
  If Cond then             { ���������� 1, ���� }
    Begin                  { ��� ������������ }
      Inc(Number);         { �������������� ���, }
      Number:=-Number      { � ��������� ����� }
    End;                   { ����� }
End ;
{----------------------------------------------------------------------}
Procedure FromNumberToString22(Number:longint;Var Operand:string22);
{ �������������� ����� � ������ }
Var      i : byte;
   k,
   Divisor : longint; { �������� }
      Cond : boolean;    { ������� �������������� ����� }
Begin
  Cond:=false;
  If Number<0 then
    Begin              { ��������� ����������- }
      Cond:=true;           { ���� ����� � ����- }
      Number:=-Number;      { ����� �� ���� }
      Dec(Number)           { ������� }
    End;
    Divisor:=1048576;  { �������� (n=20)}
  Operand:='0';
  For i:=2 to 22 do           { ���������������� }
    Begin                     { ������������ }
      k:=Number div Divisor;  { �������� ���� }
      If k>0 then             { ����� }
        Operand:=Operand+'1'
      Else
        Operand:=Operand+'0';
      Number:=Number mod Divisor;
      Divisor:=Divisor shr 1;
    End;
  If Cond then                { �������������� }
    For i:=1 to 22 do         { ���� ����������- }
      If Operand[i]='1' then  { ���� �����  (��- }
        Operand[i]:='0'       { ������� �������- }
      Else                    { �������� ����) }
        Operand[i]:='1';
End ;
{----------------------------------------------------------------------}
Procedure FromString8ToNumber(Operand:string8;Var Number:byte);
{ �������������� ������ � ����� }
Var   i : byte;
   Cond : boolean;  { ������� �������������� ����� }
Begin
  Cond:=false;
  If Operand[1]='1' then
    Begin
      Cond:=true;
      For i:=1 to 8 do        { �������������� }
        If Operand[i]='1' then { ���� ����������-}
          Operand[i]:='0'      { ���� ��������� }
        Else                   { ����� }
          Operand[i]:='1';
    End;
  Number:=0;                  { ��������� ������- }
  For i:=2 to 8 do           { ���� �������� �� }
    Begin                     { ����� ������� }
      Number:=2*Number;
      If Operand[i]='1' then
        Inc(Number);
    End;
  If Cond then             { ���������� 1, ���� }
    Begin                  { ��� ������������ }
      Inc(Number);         { �������������� ���, }
      Number:=-Number      { � ��������� ����� }
    End;                   { ����� }
End ;
{----------------------------------------------------------------------}
Procedure FromNumberToString8(Number:byte;Var Operand:string8);
{ �������������� ����� � ������ }
Var     i : byte;
  k,
  Divisor : integer;    { �������� }
     Cond : boolean;    { ������� �������������� ����� }
Begin
  Cond:=false;
  If Number<0 then
    Begin              { ��������� ����������- }
      Cond:=true;           { ���� ����� � ����- }
      Number:=-Number;      { ����� �� ���� }
      Dec(Number)           { ������� }
    End;
    Divisor:=64;  { �������� (n=6)}
  Operand:='0';
  For i:=2 to 8 do           { ���������������� }
    Begin                     { ������������ }
      k:=Number div Divisor;  { �������� ���� }
      If k>0 then             { ����� }
        Operand:=Operand+'1'
      Else
        Operand:=Operand+'0';
      Number:=Number mod Divisor;
      Divisor:=Divisor shr 1;
    End;
  If Cond then                { �������������� }
    For i:=1 to 8 do         { ���� ����������- }
      If Operand[i]='1' then  { ���� �����  (��- }
        Operand[i]:='0'       { ������� �������- }
      Else                    { �������� ����) }
        Operand[i]:='1';
End ;
{----------------------------------------------------------------------}
Procedure FromString4ToNumber(Operand:string4;Var Number:byte);
{ �������������� ������ � ����� }
Var   i : byte;
Begin
  Number:=0;                  { ��������� ������- }
  For i:=1 to 4 do           { ���� �������� �� }
    Begin                     { ����� ������� }
      Number:=2*Number;
      If Operand[i]='1' then
        Inc(Number);
    End;
End ;

end.