program C1;

uses
  Forms,
  Unit1 in 'unit1.pas' {Mymenu},
  work in 'work.pas',
  DesUnit in 'DesUnit.PAS',
  DumpUnit in 'DumpUnit.pas' {Dump},
  ActUnit in 'ACTUNIT.PAS';

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TMymenu, Mymenu);
  Application.CreateForm(TDump, Dump);
  Application.Run;
end.
