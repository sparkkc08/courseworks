unit DumpUnit;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, Grids;

type
  TDump = class(TForm)
    St: TStringGrid;
    procedure FormActivate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Dump: TDump;

implementation

Uses DesUnit, Work;

{$R *.dfm}

Procedure TDump.FormActivate(Sender: TObject);
Var i,j,xy : byte;
    k,t : integer;
    sbin : string22;
    soct : string8;
    LBuf : longint;
begin
  for i := 0 to St.ColCount do
    for j := 1 to St.RowCount do
      St.Cells[i,j] := '';

  St.ColWidths[0] := 35;
  St.ColWidths[1] := 268;
  St.ColWidths[2] := 100;
  St.ColWidths[3] := 100;

  St.Cells[0,0] := '�/�';
  St.Cells[1,0] := '                           2 �.�';
  St.Cells[2,0] := '           10 �.�';
  St.Cells[3,0] := '           8 �.�';

  k  := 1;
  St.RowCount := 2;
  St.FixedRows := 1;
  for i:= StartAddress to MaxAddress do
  begin
      sbin:=Memory[i];
      Dump.St.Cells[0,k]:=IntToStr(i);
      Dump.St.Cells[1,k]:=sbin;
      FromString22ToNumber(sbin,LBuf);
      Dump.St.Cells[2,k]:=IntToStr(LBuf);
      Dump.St.Cells[3,k]:=Make2to8(sbin);
      Inc(k);
      St.RowCount := St.RowCount+1;
  end;
  St.RowCount := St.RowCount -1;
end;
end.
