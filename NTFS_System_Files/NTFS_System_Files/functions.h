typedef struct _NTFS_BOOT
{	
	/*0x00*/UCHAR  jump[3];						/* ������� �� ����������� ��� */
	/*0x03*/UCHAR oem_id[8];					/* ��������� "NTFS    ". */
	/*0x0b*/USHORT bytes_per_sector;			/* ������ �������, � ������ */
	/*0x0d*/UCHAR  sectors_per_cluster;			/* �������� � �������� */
	/*0x0e*/USHORT reserved_sectors;			/* ������ ���� ���� */
	/*0x10*/UCHAR  num_fats;					/* ������ ���� ���� */
	/*0x11*/USHORT root_dir_entries;			/* ������ ���� ���� */ 
	/*0x13*/USHORT sectors;						/* ������ ���� ���� */ 
	/*0x15*/UCHAR  media_type;					/* ��� ��������, 0xf8 = hard disk */
	/*0x16*/USHORT sectors_per_fat;				/* ������ ���� ���� */
	/*0x18*/USHORT sectors_per_track;			/* �� ������������ */
	/*0x1a*/USHORT heads;						/* �� ������������ */
	/*0x1c*/ULONG hidden_sectors;				/* �� ������������ */
	/*0x20*/ULONG large_sectors;				/* ������ ���� ���� */ 
	/*0x24*/ULONG not_used;						/* �� ������������ */
	/*0x28*/__int64 total_ntfs_sectors;			/* ���������� �������� �� ����. */
	/*0x30*/__int64 mft_start_cluster;			/* ��������� ������� MFT. */
	/*0x38*/__int64 mftmirr_start_cluster;		/* ��������� ������� ����� MFT */
	/*0x40*/UCHAR  clusters_per_mft_record;		/* ������ MFT ������ � ���������. */
	/*0x41*/UCHAR  reserved0[3];				/* ��������������� */
	/*0x44*/UCHAR  clusters_per_index_record;	/* ������ ��������� ������ � ���������. */
	/*0x45*/UCHAR  reserved1[3];				/* ��������������� */
	/*0x48*/__int64 volume_serial_number;		/* ���������� �������� ����� ���� */
	/*0x50*/ULONG checksum;						/* �� ������������ */
	/*0x54*/UCHAR  bootstrap[426];				/* �����������-��� */
	/*0x1fe*/USHORT end_of_sector_marker;		/* ����� ������������ �������, ��������� 0xaa55 */
	/* sizeof() = 512 (0x200) bytes */
} NTFS_BOOT, *P_NTFS_BOOT;

// ��������� �������� ������
struct FileRecordHeader 
{ 
	char ID[4];  // ������������� �������� ������  
	WORD UpdateSequenceOffset; // �������� �������� ���������� ������������������
	WORD CntItemUpdateSequence; // ���������� ��������� �������, ����������� ��������
	// ���������� ������������������
	BYTE LSN[8]; // ���������������
	WORD NumberSequence;  // ����� ������������������
	WORD HardLinkCnt;  // ������� ������� ������
	WORD AttribBegOffset;  // �������� ������ ����� �������� ������, ������������������
	// ��������� �����
	WORD Flags;  // ����
	DWORD RealSizeFileRecord;  // �������� ������ �������� ������
	DWORD AllocateSizeFileRecord;  // ���������� ����, ���������� ��� �������� ������
	INT64 FileReferencePointer;  // ������ �� �������� ������
	WORD AttribMaxID;  // ������������� ��������, ������� ������������ ��������,  
	// ����������� �� 1 
	WORD UpDateSequenceNum;  // ������� ���������� ������������������
}; 

struct StandartAttribHeader  // ��������� ��������
{ 
	DWORD Type;  // ��� ��������
	DWORD Length;  // ����� ��������
	BYTE RezidentFlag;  // ���� �������������
	BYTE LengthName;  // ����� �����
	WORD ContentOffset;  // �������� ������
	WORD Flags;  // �����
	WORD ID;  // �������������
	DWORD LengthBodyAttrib;
	WORD BodyAttribOffset;
	BYTE IndexFlag;
	BYTE Alignment;
};

struct NotRezidentAttribHeader  // ��������� ��������
{ 
	DWORD Type;  // ��� ��������
	DWORD Length;  // ����� ��������
	BYTE RezidentFlag;  // ���� �������������
	BYTE LengthName;  // ����� �����
	WORD ContentOffset;  // �������� ������
	WORD Flags;  // �����
	WORD ID;  // �������������
	INT64 StartVCN; //��������� ����������� �������
	INT64 LastVCN;	//�������� ����������� �������
	WORD DataRunsOffset;	//�������� ������ ��������
	WORD CompressionBlockSize;	//������ ����� ������
	DWORD Alignment;			//������������
	INT64 ByteAllocated;  //  ������������ ����
	INT64 RealSize; //  �������� ������
	INT64 InitializedSize; //  ������������������ ������
};

struct StandartInfoAttrCont  // ���������� �������� ����������� ����������
{ 
	INT64 CreateTime;  //  ����� ��������
	INT64 EditTime;		//  ����� ���������� ��������� 
	INT64 EditRecordTime; 	//  ����� ���������� ��������� �������� ������
	INT64 ReadTime;		//  ����� ���������� ������
	DWORD AccessFlags;  //  ����� �������
	DWORD VersionNumerH;  //  ������� ������� ����� ������ ������
	DWORD VersionNumerL;  //  ������� ������� ����� ������ ������
	DWORD ClassID;  //  ������������� ������
	
};

struct FileNameAttrCont  // ���������� �������� �����
{ 
	INT64 FileReference;  //  �������� ������ �� ����������
	INT64 CreateTime;  //  ����� ��������
	INT64 EditTime;		//  ����� ���������� ��������� 
	INT64 EditRecordTime; 	//  ����� ���������� ��������� �������� ������
	INT64 ReadTime;		//  ����� ���������� ������
	INT64 ByteAllocated;  //  ������������ ����
	INT64 RealSize; //  �������� ������
	DWORD AccessFlags;  //  ����� �������
	DWORD HPFS;			//������������ � HPFS
	BYTE LengthName;  //  ����� ����� � ��������
	BYTE TypeName;  //  ��� �����
	wchar_t FileName[128]; //���
};

struct AttrDefRecord  	// ������ �������� ������ ���� ���������
{ 
	wchar_t AttrName[64]; //��� ��������
	DWORD TypeAttr;  //  ��� ��������
	DWORD DisplayRule;		
	DWORD CollationRule;  
	DWORD Flags;  //  ����� ��������
	__int64 MinSize;	// ����������� ������ ��������
	__int64 MaxSize;	// ������������ ������ ��������
};

wchar_t VolumeName[128];

struct VolumeInfAttrCont{
	__int64 Unknown;		// ����������
	UCHAR MajorVersion;	//�������� ������ �������� �������
	UCHAR MinorVersion;	//�������������� ������ �������� �������
	USHORT Flags;			//�����
};

struct DataRunsSizes{
	BYTE SizeLenDR:4;
	BYTE SizeStartClusterDR:4;
};

char *pointerbuf;
UCHAR buf[512]="", *pbuf;
NTFS_BOOT BootSector;
DWORD ReadBytesCnt;

void Get_BOOT_Inf(HANDLE hDrive)
{
	pbuf=buf;
	if(hDrive!=INVALID_HANDLE_VALUE)
	{
		memset(&BootSector,0,512);
		memset(&buf,0,512);
		ReadFile(hDrive, &buf, 512, &ReadBytesCnt, NULL);
		memcpy(&BootSector.jump,pbuf,3);
		memcpy(&BootSector.oem_id,pbuf+3,8);
		memcpy(&BootSector.bytes_per_sector,pbuf+11,2);
		memcpy(&BootSector.sectors_per_cluster,pbuf+13,1);
		memcpy(&BootSector.reserved_sectors,pbuf+14,2);
		memcpy(&BootSector.num_fats,pbuf+16,1);
		memcpy(&BootSector.root_dir_entries,pbuf+17,2);
		memcpy(&BootSector.sectors,pbuf+19,2);
		memcpy(&BootSector.media_type,pbuf+21,1);
		memcpy(&BootSector.sectors_per_fat,pbuf+22,2);
		memcpy(&BootSector.sectors_per_track,pbuf+24,2);
		memcpy(&BootSector.heads,pbuf+26,2);
		memcpy(&BootSector.hidden_sectors,pbuf+28,4);
		memcpy(&BootSector.large_sectors,pbuf+32,4);
		memcpy(&BootSector.not_used,pbuf+36,4);
		memcpy(&BootSector.total_ntfs_sectors,pbuf+40,8);
		memcpy(&BootSector.mft_start_cluster,pbuf+48,8);
		memcpy(&BootSector.mftmirr_start_cluster,pbuf+56,8);
		memcpy(&BootSector.clusters_per_mft_record,pbuf+64,1);
		memcpy(&BootSector.reserved0,pbuf+65,3);
		memcpy(&BootSector.clusters_per_index_record,pbuf+68,1);
		memcpy(&BootSector.reserved1,pbuf+69,3);
		memcpy(&BootSector.volume_serial_number,pbuf+72,8);
		memcpy(&BootSector.checksum,pbuf+80,4);
		memcpy(&BootSector.bootstrap,pbuf+84,426);
		memcpy(&BootSector.end_of_sector_marker,pbuf+510,2);
	}
}


CHAR* Get_Drive_Mem(HANDLE hDrive, DWORD StartSec, long CntBytes)
{
	LARGE_INTEGER AdrSec;
	CHAR *MemPointer;

	MemPointer = new CHAR[CntBytes];
	if(!MemPointer)
		return 0;
	else
	{
		AdrSec.LowPart = StartSec;
		AdrSec.HighPart = 0;
		AdrSec.QuadPart <<= 9;
		SetFilePointerEx (hDrive, AdrSec, NULL, FILE_BEGIN);
		memset(MemPointer, 0, CntBytes);
		ReadFile(hDrive, MemPointer, CntBytes, &ReadBytesCnt, NULL);
		return MemPointer;
	}	
}