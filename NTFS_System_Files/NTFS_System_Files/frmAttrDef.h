#pragma once

namespace NTFS_System_Files {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� frmAttrDef
	/// </summary>
	public ref class frmAttrDef : public System::Windows::Forms::Form
	{
	public:
		frmAttrDef(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~frmAttrDef()
		{
			if (components)
			{
				delete components;
			}
		}
	public: System::Windows::Forms::DataGridView^  dgvAttrDef;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ClmTypeAttr;
	public: 
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ClmNameAttr;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ClmFlagsAttr;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ClmMinSizeAttr;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ClmMaxSizeAttr;

	public: 





	public: 





	public: 





	public: 



	protected: 

	public: 




	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle1 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle7 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle8 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle2 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle3 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle4 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle5 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle6 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			this->dgvAttrDef = (gcnew System::Windows::Forms::DataGridView());
			this->ClmTypeAttr = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->ClmNameAttr = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->ClmFlagsAttr = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->ClmMinSizeAttr = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->ClmMaxSizeAttr = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dgvAttrDef))->BeginInit();
			this->SuspendLayout();
			// 
			// dgvAttrDef
			// 
			this->dgvAttrDef->AllowUserToResizeRows = false;
			dataGridViewCellStyle1->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			dataGridViewCellStyle1->BackColor = System::Drawing::SystemColors::Control;
			dataGridViewCellStyle1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			dataGridViewCellStyle1->ForeColor = System::Drawing::SystemColors::WindowText;
			dataGridViewCellStyle1->SelectionBackColor = System::Drawing::SystemColors::Highlight;
			dataGridViewCellStyle1->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle1->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
			this->dgvAttrDef->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this->dgvAttrDef->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dgvAttrDef->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(5) {this->ClmTypeAttr, 
				this->ClmNameAttr, this->ClmFlagsAttr, this->ClmMinSizeAttr, this->ClmMaxSizeAttr});
			dataGridViewCellStyle7->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
			dataGridViewCellStyle7->BackColor = System::Drawing::SystemColors::Window;
			dataGridViewCellStyle7->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			dataGridViewCellStyle7->ForeColor = System::Drawing::SystemColors::ControlText;
			dataGridViewCellStyle7->SelectionBackColor = System::Drawing::SystemColors::Highlight;
			dataGridViewCellStyle7->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle7->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
			this->dgvAttrDef->DefaultCellStyle = dataGridViewCellStyle7;
			this->dgvAttrDef->Location = System::Drawing::Point(12, 12);
			this->dgvAttrDef->Name = L"dgvAttrDef";
			this->dgvAttrDef->ReadOnly = true;
			dataGridViewCellStyle8->BackColor = System::Drawing::SystemColors::Control;
			dataGridViewCellStyle8->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			dataGridViewCellStyle8->ForeColor = System::Drawing::SystemColors::WindowText;
			dataGridViewCellStyle8->SelectionBackColor = System::Drawing::SystemColors::Highlight;
			dataGridViewCellStyle8->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle8->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
			this->dgvAttrDef->RowHeadersDefaultCellStyle = dataGridViewCellStyle8;
			this->dgvAttrDef->RowHeadersVisible = false;
			this->dgvAttrDef->ScrollBars = System::Windows::Forms::ScrollBars::Vertical;
			this->dgvAttrDef->Size = System::Drawing::Size(692, 352);
			this->dgvAttrDef->TabIndex = 0;
			// 
			// ClmTypeAttr
			// 
			dataGridViewCellStyle2->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			this->ClmTypeAttr->DefaultCellStyle = dataGridViewCellStyle2;
			this->ClmTypeAttr->HeaderText = L"���";
			this->ClmTypeAttr->Name = L"ClmTypeAttr";
			this->ClmTypeAttr->ReadOnly = true;
			// 
			// ClmNameAttr
			// 
			dataGridViewCellStyle3->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
			this->ClmNameAttr->DefaultCellStyle = dataGridViewCellStyle3;
			this->ClmNameAttr->HeaderText = L"��� ��������";
			this->ClmNameAttr->Name = L"ClmNameAttr";
			this->ClmNameAttr->ReadOnly = true;
			this->ClmNameAttr->Width = 200;
			// 
			// ClmFlagsAttr
			// 
			dataGridViewCellStyle4->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			dataGridViewCellStyle4->NullValue = L"-";
			this->ClmFlagsAttr->DefaultCellStyle = dataGridViewCellStyle4;
			this->ClmFlagsAttr->HeaderText = L"�����";
			this->ClmFlagsAttr->Name = L"ClmFlagsAttr";
			this->ClmFlagsAttr->ReadOnly = true;
			this->ClmFlagsAttr->ToolTipText = L"I - ���������, R - ����� �����������, N - �������� �������������";
			// 
			// ClmMinSizeAttr
			// 
			dataGridViewCellStyle5->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			dataGridViewCellStyle5->NullValue = L"-";
			this->ClmMinSizeAttr->DefaultCellStyle = dataGridViewCellStyle5;
			this->ClmMinSizeAttr->HeaderText = L"Min ������, ����";
			this->ClmMinSizeAttr->Name = L"ClmMinSizeAttr";
			this->ClmMinSizeAttr->ReadOnly = true;
			this->ClmMinSizeAttr->Width = 140;
			// 
			// ClmMaxSizeAttr
			// 
			dataGridViewCellStyle6->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			dataGridViewCellStyle6->NullValue = L"-";
			this->ClmMaxSizeAttr->DefaultCellStyle = dataGridViewCellStyle6;
			this->ClmMaxSizeAttr->HeaderText = L"Max ������, ����";
			this->ClmMaxSizeAttr->Name = L"ClmMaxSizeAttr";
			this->ClmMaxSizeAttr->ReadOnly = true;
			this->ClmMaxSizeAttr->Width = 140;
			// 
			// frmAttrDef
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(716, 375);
			this->Controls->Add(this->dgvAttrDef);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
			this->MaximizeBox = false;
			this->Name = L"frmAttrDef";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"������ ���������";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dgvAttrDef))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	};
}
