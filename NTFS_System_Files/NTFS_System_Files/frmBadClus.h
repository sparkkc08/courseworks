#pragma once

namespace NTFS_System_Files {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� frmBadClus
	/// </summary>
	public ref class frmBadClus : public System::Windows::Forms::Form
	{
	public:
		frmBadClus(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~frmBadClus()
		{
			if (components)
			{
				delete components;
			}
		}
	public: System::Windows::Forms::DataGridView^  dgvBadClust;
	protected: 

	protected: 
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ClmCluster;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Clm0;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Clm1;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Clm2;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Clm3;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Clm4;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Clm5;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Clm6;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Clm7;










	protected: 









	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle1 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			this->dgvBadClust = (gcnew System::Windows::Forms::DataGridView());
			this->ClmCluster = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Clm0 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Clm1 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Clm2 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Clm3 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Clm4 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Clm5 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Clm6 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Clm7 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dgvBadClust))->BeginInit();
			this->SuspendLayout();
			// 
			// dgvBadClust
			// 
			this->dgvBadClust->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dgvBadClust->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(9) {this->ClmCluster, 
				this->Clm0, this->Clm1, this->Clm2, this->Clm3, this->Clm4, this->Clm5, this->Clm6, this->Clm7});
			this->dgvBadClust->Location = System::Drawing::Point(12, 12);
			this->dgvBadClust->Name = L"dgvBadClust";
			this->dgvBadClust->RowHeadersVisible = false;
			this->dgvBadClust->ScrollBars = System::Windows::Forms::ScrollBars::Vertical;
			this->dgvBadClust->Size = System::Drawing::Size(281, 238);
			this->dgvBadClust->TabIndex = 0;
			// 
			// ClmCluster
			// 
			dataGridViewCellStyle1->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			this->ClmCluster->DefaultCellStyle = dataGridViewCellStyle1;
			this->ClmCluster->HeaderText = L"�/�";
			this->ClmCluster->Name = L"ClmCluster";
			// 
			// Clm0
			// 
			this->Clm0->HeaderText = L"0";
			this->Clm0->Name = L"Clm0";
			this->Clm0->ReadOnly = true;
			this->Clm0->Width = 20;
			// 
			// Clm1
			// 
			this->Clm1->HeaderText = L"1";
			this->Clm1->Name = L"Clm1";
			this->Clm1->ReadOnly = true;
			this->Clm1->Width = 20;
			// 
			// Clm2
			// 
			this->Clm2->HeaderText = L"2";
			this->Clm2->Name = L"Clm2";
			this->Clm2->ReadOnly = true;
			this->Clm2->Width = 20;
			// 
			// Clm3
			// 
			this->Clm3->HeaderText = L"3";
			this->Clm3->Name = L"Clm3";
			this->Clm3->ReadOnly = true;
			this->Clm3->Width = 20;
			// 
			// Clm4
			// 
			this->Clm4->HeaderText = L"4";
			this->Clm4->Name = L"Clm4";
			this->Clm4->ReadOnly = true;
			this->Clm4->Width = 20;
			// 
			// Clm5
			// 
			this->Clm5->HeaderText = L"5";
			this->Clm5->Name = L"Clm5";
			this->Clm5->ReadOnly = true;
			this->Clm5->Width = 20;
			// 
			// Clm6
			// 
			this->Clm6->HeaderText = L"6";
			this->Clm6->Name = L"Clm6";
			this->Clm6->ReadOnly = true;
			this->Clm6->Width = 20;
			// 
			// Clm7
			// 
			this->Clm7->HeaderText = L"7";
			this->Clm7->Name = L"Clm7";
			this->Clm7->ReadOnly = true;
			this->Clm7->Width = 20;
			// 
			// frmBadClus
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(307, 262);
			this->Controls->Add(this->dgvBadClust);
			this->MaximizeBox = false;
			this->Name = L"frmBadClus";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"����� ����� ��������";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dgvBadClust))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	};
}
