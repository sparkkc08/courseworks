#pragma once

namespace NTFS_System_Files {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� frmMTFMirr1
	/// </summary>
	public ref class frmMTFMirr : public System::Windows::Forms::Form
	{
	public:
		frmMTFMirr(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~frmMTFMirr()
		{
			if (components)
			{
				delete components;
			}
		}
	public: System::Windows::Forms::DataGridView^  dgvMFTMirrData;
	protected: 

	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ClmFileName;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ClmFileSize;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ClmCreateTime;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ClmEditTime;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ClmAttributs;
	protected: 

	protected: 

	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle1 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle2 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			this->dgvMFTMirrData = (gcnew System::Windows::Forms::DataGridView());
			this->ClmFileName = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->ClmFileSize = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->ClmCreateTime = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->ClmEditTime = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->ClmAttributs = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dgvMFTMirrData))->BeginInit();
			this->SuspendLayout();
			// 
			// dgvMFTMirrData
			// 
			this->dgvMFTMirrData->AllowUserToResizeRows = false;
			dataGridViewCellStyle1->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			dataGridViewCellStyle1->BackColor = System::Drawing::SystemColors::Control;
			dataGridViewCellStyle1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			dataGridViewCellStyle1->ForeColor = System::Drawing::SystemColors::WindowText;
			dataGridViewCellStyle1->SelectionBackColor = System::Drawing::SystemColors::Highlight;
			dataGridViewCellStyle1->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle1->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
			this->dgvMFTMirrData->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this->dgvMFTMirrData->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dgvMFTMirrData->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(5) {this->ClmFileName, 
				this->ClmFileSize, this->ClmCreateTime, this->ClmEditTime, this->ClmAttributs});
			dataGridViewCellStyle2->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			dataGridViewCellStyle2->BackColor = System::Drawing::SystemColors::Window;
			dataGridViewCellStyle2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			dataGridViewCellStyle2->ForeColor = System::Drawing::SystemColors::ControlText;
			dataGridViewCellStyle2->SelectionBackColor = System::Drawing::SystemColors::Highlight;
			dataGridViewCellStyle2->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle2->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
			this->dgvMFTMirrData->DefaultCellStyle = dataGridViewCellStyle2;
			this->dgvMFTMirrData->Location = System::Drawing::Point(12, 12);
			this->dgvMFTMirrData->Name = L"dgvMFTMirrData";
			this->dgvMFTMirrData->ReadOnly = true;
			this->dgvMFTMirrData->RowHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::None;
			this->dgvMFTMirrData->RowHeadersVisible = false;
			this->dgvMFTMirrData->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::DisableResizing;
			this->dgvMFTMirrData->ScrollBars = System::Windows::Forms::ScrollBars::None;
			this->dgvMFTMirrData->Size = System::Drawing::Size(696, 110);
			this->dgvMFTMirrData->TabIndex = 0;
			// 
			// ClmFileName
			// 
			this->ClmFileName->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
			this->ClmFileName->HeaderText = L"��� �����";
			this->ClmFileName->Name = L"ClmFileName";
			this->ClmFileName->ReadOnly = true;
			this->ClmFileName->Width = 152;
			// 
			// ClmFileSize
			// 
			this->ClmFileSize->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
			this->ClmFileSize->HeaderText = L"������ �����, ����";
			this->ClmFileSize->Name = L"ClmFileSize";
			this->ClmFileSize->ReadOnly = true;
			this->ClmFileSize->Width = 140;
			// 
			// ClmCreateTime
			// 
			this->ClmCreateTime->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
			this->ClmCreateTime->HeaderText = L"����� ��������";
			this->ClmCreateTime->Name = L"ClmCreateTime";
			this->ClmCreateTime->ReadOnly = true;
			this->ClmCreateTime->Width = 120;
			// 
			// ClmEditTime
			// 
			this->ClmEditTime->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
			this->ClmEditTime->HeaderText = L"����� ���������";
			this->ClmEditTime->Name = L"ClmEditTime";
			this->ClmEditTime->ReadOnly = true;
			this->ClmEditTime->Width = 130;
			// 
			// ClmAttributs
			// 
			this->ClmAttributs->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
			this->ClmAttributs->HeaderText = L"��������";
			this->ClmAttributs->Name = L"ClmAttributs";
			this->ClmAttributs->ReadOnly = true;
			this->ClmAttributs->ToolTipText = L"R - ������ ������, � - �������, S - ���������, � - ��������, � - ������, D - ����" 
				L"������";
			this->ClmAttributs->Width = 150;
			// 
			// frmMTFMirr
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(720, 141);
			this->Controls->Add(this->dgvMFTMirrData);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
			this->MaximizeBox = false;
			this->Name = L"frmMTFMirr";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"������� MTFMirr";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dgvMFTMirrData))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	};
}
