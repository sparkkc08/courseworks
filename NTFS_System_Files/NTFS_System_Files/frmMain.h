﻿#include "frmMFT.h"
#include "frmMTFMirr.h"
#include "frmLogFile.h"
#include "frmVolume.h"
#include "frmAttrDef.h"
#include "frmBitmap.h"
#include "frmBoot.h"
#include "frmBadClus.h"
#include "frmSecure.h"
#include "frmUpCase.h"

#include <windows.h>
#include <stdio.h>
#include <string>
#include <stdlib.h>
#include "functions.h"

HANDLE hDrive;
CHAR DrivesString[120],*chPointer;
CHAR DrivesArray[40][4];
CHAR DriveFileSystem[20];
CHAR SelectedDrive[10]="\\\\.\\",*pSelectedDrive;
INT DriveType, FlagFileSystem, NTFS_DrivesCnt;
DWORD DrivesCount;
BOOL FlagVolumeInf;
UINT i;
INT64 FileSize;
char MultiByteStr[130];
FILETIME FileTimeStruc;
SYSTEMTIME SystemTimeCreateStruc, SystemTimeEditStruc;
CHAR OutBuffer1[50],OutBuffer2[50],OutBuffer3[50],OutBuffer4[50];
VolumeInfAttrCont VolumeInformation;


BYTE BufFileRecord[1024];
DWORD dwPtrLow;
LARGE_INTEGER BeginMFTxSector;
FileRecordHeader  FileHeader; 
StandartAttribHeader StandartAttributHeader;
NotRezidentAttribHeader NotRezidentAttributHeader;
WORD AttributOffset; 
FileNameAttrCont AttrFileName;
StandartInfoAttrCont AttrStandartInformation;
BOOL FlagWinNameFile;
BYTE AttribRezidentFlag;

#pragma once

namespace NTFS_System_Files {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace std;
	using std::string;

	/// <summary>
	/// Сводка для frmMain
	/// </summary>
	public ref class frmMain : public System::Windows::Forms::Form
	{
	public:
		frmMain(void)
		{
			InitializeComponent();
			//
			//TODO: добавьте код конструктора
			//
		}

	protected:
		/// <summary>
		/// Освободить все используемые ресурсы.
		/// </summary>
		~frmMain()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::MenuStrip^  mnMain;
	protected: 

	protected: 
	private: System::Windows::Forms::ToolStripMenuItem^  mFTToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  mTFMirrToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  logFileToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  volumeToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  attrDefToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  bitmapToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  bootToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  badClusToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  secureToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  upcaseToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  extendToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  objIdToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  quotaToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  reparseToolStripMenuItem;
	private: System::Windows::Forms::ToolStripMenuItem^  usnJrnlToolStripMenuItem;
	private: System::Windows::Forms::ComboBox^  DrivesComboBox;
	private: System::Windows::Forms::Label^  lblDriveChoice;


	private: System::ComponentModel::IContainer^  components;

	private:
		/// <summary>
		/// Требуется переменная конструктора.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// Обязательный метод для поддержки конструктора - не изменяйте
		/// содержимое данного метода при помощи редактора кода.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(frmMain::typeid));
			this->mnMain = (gcnew System::Windows::Forms::MenuStrip());
			this->mFTToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->mTFMirrToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->logFileToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->volumeToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->attrDefToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->bitmapToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->bootToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->badClusToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->secureToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->upcaseToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->extendToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->objIdToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->quotaToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->reparseToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->usnJrnlToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->DrivesComboBox = (gcnew System::Windows::Forms::ComboBox());
			this->lblDriveChoice = (gcnew System::Windows::Forms::Label());
			this->mnMain->SuspendLayout();
			this->SuspendLayout();
			// 
			// mnMain
			// 
			this->mnMain->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(11) {this->mFTToolStripMenuItem, 
				this->mTFMirrToolStripMenuItem, this->logFileToolStripMenuItem, this->volumeToolStripMenuItem, this->attrDefToolStripMenuItem, 
				this->bitmapToolStripMenuItem, this->bootToolStripMenuItem, this->badClusToolStripMenuItem, this->secureToolStripMenuItem, this->upcaseToolStripMenuItem, 
				this->extendToolStripMenuItem});
			this->mnMain->Location = System::Drawing::Point(0, 0);
			this->mnMain->Name = L"mnMain";
			this->mnMain->Size = System::Drawing::Size(691, 24);
			this->mnMain->TabIndex = 0;
			this->mnMain->Text = L"mnMain";
			// 
			// mFTToolStripMenuItem
			// 
			this->mFTToolStripMenuItem->Enabled = false;
			this->mFTToolStripMenuItem->Name = L"mFTToolStripMenuItem";
			this->mFTToolStripMenuItem->Size = System::Drawing::Size(49, 20);
			this->mFTToolStripMenuItem->Text = L"$MFT";
			this->mFTToolStripMenuItem->ToolTipText = L"Главная файловая таблица (Master File Table, MFT)";
			this->mFTToolStripMenuItem->Click += gcnew System::EventHandler(this, &frmMain::mFTToolStripMenuItem_Click);
			// 
			// mTFMirrToolStripMenuItem
			// 
			this->mTFMirrToolStripMenuItem->Enabled = false;
			this->mTFMirrToolStripMenuItem->Name = L"mTFMirrToolStripMenuItem";
			this->mTFMirrToolStripMenuItem->Size = System::Drawing::Size(71, 20);
			this->mTFMirrToolStripMenuItem->Text = L"$MTFMirr";
			this->mTFMirrToolStripMenuItem->ToolTipText = L"Резервная копия первых четырех элементов 4 MFT";
			this->mTFMirrToolStripMenuItem->Click += gcnew System::EventHandler(this, &frmMain::mTFMirrToolStripMenuItem_Click);
			// 
			// logFileToolStripMenuItem
			// 
			this->logFileToolStripMenuItem->Enabled = false;
			this->logFileToolStripMenuItem->Name = L"logFileToolStripMenuItem";
			this->logFileToolStripMenuItem->Size = System::Drawing::Size(63, 20);
			this->logFileToolStripMenuItem->Text = L"$LogFile";
			this->logFileToolStripMenuItem->ToolTipText = L"Журнал транзакций (transactional logging file)";
			// 
			// volumeToolStripMenuItem
			// 
			this->volumeToolStripMenuItem->Enabled = false;
			this->volumeToolStripMenuItem->Name = L"volumeToolStripMenuItem";
			this->volumeToolStripMenuItem->Size = System::Drawing::Size(66, 20);
			this->volumeToolStripMenuItem->Text = L"$Volume";
			this->volumeToolStripMenuItem->ToolTipText = L"Серийный номер, время создания, dirty flag (флаг не сброшенного кэша) тома";
			this->volumeToolStripMenuItem->Click += gcnew System::EventHandler(this, &frmMain::volumeToolStripMenuItem_Click);
			// 
			// attrDefToolStripMenuItem
			// 
			this->attrDefToolStripMenuItem->Enabled = false;
			this->attrDefToolStripMenuItem->Name = L"attrDefToolStripMenuItem";
			this->attrDefToolStripMenuItem->Size = System::Drawing::Size(63, 20);
			this->attrDefToolStripMenuItem->Text = L"$AttrDef";
			this->attrDefToolStripMenuItem->ToolTipText = L"Определение атрибутов";
			this->attrDefToolStripMenuItem->Click += gcnew System::EventHandler(this, &frmMain::attrDefToolStripMenuItem_Click);
			// 
			// bitmapToolStripMenuItem
			// 
			this->bitmapToolStripMenuItem->Enabled = false;
			this->bitmapToolStripMenuItem->Name = L"bitmapToolStripMenuItem";
			this->bitmapToolStripMenuItem->Size = System::Drawing::Size(63, 20);
			this->bitmapToolStripMenuItem->Text = L"$Bitmap";
			this->bitmapToolStripMenuItem->ToolTipText = L"Карта свободного/занятого пространства";
			this->bitmapToolStripMenuItem->Click += gcnew System::EventHandler(this, &frmMain::bitmapToolStripMenuItem_Click);
			// 
			// bootToolStripMenuItem
			// 
			this->bootToolStripMenuItem->Enabled = false;
			this->bootToolStripMenuItem->Name = L"bootToolStripMenuItem";
			this->bootToolStripMenuItem->Size = System::Drawing::Size(50, 20);
			this->bootToolStripMenuItem->Text = L"$Boot";
			this->bootToolStripMenuItem->ToolTipText = L"Загрузочная запись (boot record) тома";
			this->bootToolStripMenuItem->Click += gcnew System::EventHandler(this, &frmMain::bootToolStripMenuItem_Click);
			// 
			// badClusToolStripMenuItem
			// 
			this->badClusToolStripMenuItem->Enabled = false;
			this->badClusToolStripMenuItem->Name = L"badClusToolStripMenuItem";
			this->badClusToolStripMenuItem->Size = System::Drawing::Size(68, 20);
			this->badClusToolStripMenuItem->Text = L"$BadClus";
			this->badClusToolStripMenuItem->ToolTipText = L"Список плохих кластеров (bad clusters) тома";
			this->badClusToolStripMenuItem->Click += gcnew System::EventHandler(this, &frmMain::badClusToolStripMenuItem_Click);
			// 
			// secureToolStripMenuItem
			// 
			this->secureToolStripMenuItem->Enabled = false;
			this->secureToolStripMenuItem->Name = L"secureToolStripMenuItem";
			this->secureToolStripMenuItem->Size = System::Drawing::Size(60, 20);
			this->secureToolStripMenuItem->Text = L"$Secure";
			this->secureToolStripMenuItem->ToolTipText = L"Использованные дескрипторы безопасности (security descriptors)";
			// 
			// upcaseToolStripMenuItem
			// 
			this->upcaseToolStripMenuItem->Enabled = false;
			this->upcaseToolStripMenuItem->Name = L"upcaseToolStripMenuItem";
			this->upcaseToolStripMenuItem->Size = System::Drawing::Size(63, 20);
			this->upcaseToolStripMenuItem->Text = L"$Upcase";
			this->upcaseToolStripMenuItem->ToolTipText = L"Таблица заглавных символов (uppercase characters ) для трансляции имен";
			this->upcaseToolStripMenuItem->Click += gcnew System::EventHandler(this, &frmMain::upcaseToolStripMenuItem_Click);
			// 
			// extendToolStripMenuItem
			// 
			this->extendToolStripMenuItem->DropDownItems->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(4) {this->objIdToolStripMenuItem, 
				this->quotaToolStripMenuItem, this->reparseToolStripMenuItem, this->usnJrnlToolStripMenuItem});
			this->extendToolStripMenuItem->Enabled = false;
			this->extendToolStripMenuItem->Name = L"extendToolStripMenuItem";
			this->extendToolStripMenuItem->Size = System::Drawing::Size(60, 20);
			this->extendToolStripMenuItem->Text = L"$Extend";
			this->extendToolStripMenuItem->ToolTipText = L"Каталоги: $ObjId, $Quota, $Reparse, $UsnJrnl";
			// 
			// objIdToolStripMenuItem
			// 
			this->objIdToolStripMenuItem->Name = L"objIdToolStripMenuItem";
			this->objIdToolStripMenuItem->Size = System::Drawing::Size(121, 22);
			this->objIdToolStripMenuItem->Text = L"$ObjId";
			// 
			// quotaToolStripMenuItem
			// 
			this->quotaToolStripMenuItem->Name = L"quotaToolStripMenuItem";
			this->quotaToolStripMenuItem->Size = System::Drawing::Size(121, 22);
			this->quotaToolStripMenuItem->Text = L"$Quota";
			// 
			// reparseToolStripMenuItem
			// 
			this->reparseToolStripMenuItem->Name = L"reparseToolStripMenuItem";
			this->reparseToolStripMenuItem->Size = System::Drawing::Size(121, 22);
			this->reparseToolStripMenuItem->Text = L"$Reparse";
			// 
			// usnJrnlToolStripMenuItem
			// 
			this->usnJrnlToolStripMenuItem->Name = L"usnJrnlToolStripMenuItem";
			this->usnJrnlToolStripMenuItem->Size = System::Drawing::Size(121, 22);
			this->usnJrnlToolStripMenuItem->Text = L"$UsnJrnl";
			// 
			// DrivesComboBox
			// 
			this->DrivesComboBox->FormattingEnabled = true;
			this->DrivesComboBox->Location = System::Drawing::Point(497, 51);
			this->DrivesComboBox->Name = L"DrivesComboBox";
			this->DrivesComboBox->Size = System::Drawing::Size(78, 21);
			this->DrivesComboBox->TabIndex = 1;
			this->DrivesComboBox->SelectedIndexChanged += gcnew System::EventHandler(this, &frmMain::DrivesComboBox_SelectedIndexChanged);
			// 
			// lblDriveChoice
			// 
			this->lblDriveChoice->AutoSize = true;
			this->lblDriveChoice->Font = (gcnew System::Drawing::Font(L"Segoe UI", 9.75F, System::Drawing::FontStyle::Italic, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(0)));
			this->lblDriveChoice->Location = System::Drawing::Point(12, 51);
			this->lblDriveChoice->Name = L"lblDriveChoice";
			this->lblDriveChoice->Size = System::Drawing::Size(479, 17);
			this->lblDriveChoice->TabIndex = 2;
			this->lblDriveChoice->Text = L"Для начала работы необходимо выбрать один из предложенных ниже дисков:";
			// 
			// frmMain
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(691, 262);
			this->Controls->Add(this->lblDriveChoice);
			this->Controls->Add(this->DrivesComboBox);
			this->Controls->Add(this->mnMain);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
			this->Icon = (cli::safe_cast<System::Drawing::Icon^  >(resources->GetObject(L"$this.Icon")));
			this->MainMenuStrip = this->mnMain;
			this->Name = L"frmMain";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Курсовая работа Миронюк М. В.";
			this->Load += gcnew System::EventHandler(this, &frmMain::frmMain_Load);
			this->mnMain->ResumeLayout(false);
			this->mnMain->PerformLayout();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
		
#pragma endregion

String^ ComboDrive,  ^strFileName;


void ReadMFTx(System::Windows::Forms::DataGridView^  dgvMFTxData, __int64 mftx_startcluster)
{
					
	dgvMFTxData->Rows->Clear();
	BeginMFTxSector.LowPart = BootSector.sectors_per_cluster * mftx_startcluster;
	BeginMFTxSector.HighPart = 0;
	BeginMFTxSector.QuadPart <<= 9;
	dwPtrLow = SetFilePointerEx (hDrive, BeginMFTxSector, NULL, FILE_BEGIN);
	ReadFile(hDrive, BufFileRecord, 1024, &ReadBytesCnt, NULL);
	memcpy(&FileHeader, BufFileRecord, sizeof(FileHeader));
	while(strncmp(FileHeader.ID, "FILE",4) == 0) 
	{ 
		AttributOffset = FileHeader.AttribBegOffset; 
		//AttribRezidentFlag = BufFileRecord[FileHeader.AttribBegOffset + 8];
		memcpy(&StandartAttributHeader, BufFileRecord + AttributOffset, sizeof(StandartAttributHeader));
		if(StandartAttributHeader.RezidentFlag == 1)
			memcpy(&NotRezidentAttributHeader, BufFileRecord + AttributOffset, sizeof(NotRezidentAttribHeader));
		// Пока не прочли все атрибуты
		FlagWinNameFile = false;
		FileSize = 0;
		while(StandartAttributHeader.Type != 0xffffffff) 
		{ 
			if(FileHeader.Flags == 0)
				break;
			switch(StandartAttributHeader.Type) 
			{ 
			case 0x10:	// Если тип атрибута стандартная инфорация
				{
					// Извлекаем из считаного содержание атрибута имя
					memset(&AttrStandartInformation,0,sizeof(AttrStandartInformation));
					memcpy(&AttrStandartInformation, BufFileRecord + AttributOffset + 24 + 2*StandartAttributHeader.LengthName, sizeof(AttrStandartInformation));

					FileTimeStruc.dwLowDateTime = AttrStandartInformation.CreateTime & 0xFFFFFFFF;
					FileTimeStruc.dwHighDateTime = (AttrStandartInformation.CreateTime>>32) & 0xFFFFFFFF;

					FileTimeToSystemTime(&FileTimeStruc, &SystemTimeCreateStruc);

					FileTimeStruc.dwLowDateTime = AttrStandartInformation.EditTime & 0xFFFFFFFF;
					FileTimeStruc.dwHighDateTime = (AttrStandartInformation.EditTime>>32) & 0xFFFFFFFF;

					FileTimeToSystemTime(&FileTimeStruc, &SystemTimeEditStruc);
					break;
				}
			case 0x30:  // Если тип атрибута имя
				{  
					// Извлекаем из считаного содержание атрибута имя
					memset(&AttrFileName,0,sizeof(AttrFileName));
					memcpy(&AttrFileName, BufFileRecord + AttributOffset + 24 + 2*StandartAttributHeader.LengthName, sizeof(AttrFileName));

				 	//  Если длина имени  больше нуля, выводим имя на экран
				 	if((AttrFileName.LengthName > 0) && (!FlagWinNameFile))  
				 	{
						memset(&OutBuffer3,0,sizeof(OutBuffer3));
				 		AttrFileName.FileName[AttrFileName.LengthName] = '\0';

						if(AttrFileName.AccessFlags & 0x1)
							strcat(OutBuffer3,"_R_");
						if(AttrFileName.AccessFlags & 0x2)
							strcat(OutBuffer3,"_H_");
						if(AttrFileName.AccessFlags & 0x4)
							strcat(OutBuffer3,"_S_");
						if(AttrFileName.AccessFlags & 0x20)
							strcat(OutBuffer3,"_A_");
						if(AttrFileName.AccessFlags & 0x800)
							strcat(OutBuffer3,"_C_");
						if(AttrFileName.AccessFlags & 0x10000000)
							strcat(OutBuffer3,"_D_");
				 	} 
					if(AttrFileName.TypeName & 1)
						FlagWinNameFile = true;
				 	break; 
				} 
			case 0x60: // Если тип атрибута имя тома
				{
					// Извлекаем из считаного содержание атрибута имя тома
					memset(&VolumeName,0,sizeof(VolumeName));
					memcpy(&VolumeName, BufFileRecord + AttributOffset + 24 + 2*StandartAttributHeader.LengthName, sizeof(VolumeName));
					break;
				}
			case 0x70: // Если тип атрибута информация тома
				{
					// Извлекаем из считаного содержание атрибута имя тома
					memset(&VolumeInformation,0,sizeof(VolumeInformation));
					memcpy(&VolumeInformation, BufFileRecord + AttributOffset + 24 + 2*StandartAttributHeader.LengthName, sizeof(VolumeInformation));
					break;
				}
			case 0x80: // Если тип атрибута данные
				{
					if(StandartAttributHeader.RezidentFlag == 0)
					{
						FileSize = StandartAttributHeader.LengthBodyAttrib;
					}
					else if(StandartAttributHeader.RezidentFlag == 1)
					{
						FileSize = NotRezidentAttributHeader.RealSize;
					}
					break;
				}
			} 
			// Извлекаем из считанного заголовок очередного атрибута
			AttributOffset = AttributOffset + StandartAttributHeader.Length; 
			//AttribRezidentFlag = BufFileRecord[AttributOffset + 8];
			memcpy(&StandartAttributHeader, BufFileRecord + AttributOffset, sizeof(StandartAttributHeader));
			if(StandartAttributHeader.RezidentFlag == 1)
				memcpy(&NotRezidentAttributHeader, BufFileRecord + AttributOffset, sizeof(NotRezidentAttribHeader));
		}     
		if((FileHeader.Flags != 0) && (wcslen(AttrFileName.FileName) != 0))
		{
			sprintf(OutBuffer1,"%d/%02d/%d %02d:%02d",SystemTimeCreateStruc.wDay,SystemTimeCreateStruc.wMonth,SystemTimeCreateStruc.wYear,SystemTimeCreateStruc.wHour,SystemTimeCreateStruc.wMinute);
			sprintf(OutBuffer2,"%d/%02d/%d %02d:%02d",SystemTimeEditStruc.wDay,SystemTimeEditStruc.wMonth,SystemTimeEditStruc.wYear,SystemTimeEditStruc.wHour,SystemTimeEditStruc.wMinute);
			strFileName = gcnew String(AttrFileName.FileName);
						 
			dgvMFTxData->Rows->Add(strFileName, FileSize, gcnew String(OutBuffer1),gcnew String(OutBuffer2),gcnew String(OutBuffer3));
			memset(&AttrFileName.FileName,0,sizeof(AttrFileName.FileName));
		}
		memset(&BufFileRecord,0,sizeof(BufFileRecord));
		ReadFile(hDrive, BufFileRecord, 1024, &ReadBytesCnt, NULL);
		// Извлекаем из считанного заголовок файловой записи
		memcpy(&FileHeader, BufFileRecord, sizeof(FileHeader)); 
	}
}

CHAR* FindMFTRecord(DWORD Finde_Attr, wchar_t* FindeRecName, wchar_t* Finde_Attr_Name)
{
	CHAR *Attr_Pointer;
	BeginMFTxSector.LowPart = BootSector.sectors_per_cluster * BootSector.mft_start_cluster;
	BeginMFTxSector.HighPart = 0;
	BeginMFTxSector.QuadPart <<= 9;
	dwPtrLow = SetFilePointerEx (hDrive, BeginMFTxSector, NULL, FILE_BEGIN);
	ReadFile(hDrive, BufFileRecord, 1024, &ReadBytesCnt, NULL);
	memcpy(&FileHeader, BufFileRecord, sizeof(FileHeader));
	while(strncmp(FileHeader.ID, "FILE",4) == 0) 
	{ 
		AttributOffset = FileHeader.AttribBegOffset; 
		memcpy(&StandartAttributHeader, BufFileRecord + AttributOffset, sizeof(StandartAttributHeader));
		while(StandartAttributHeader.Type != 0xffffffff) 
		{ 
			if(FileHeader.Flags == 0)
				break;
			if (StandartAttributHeader.Type == 0x30)  // Если тип атрибута имя
				{  
					// Извлекаем из считаного содержание атрибута имя
					memset(&AttrFileName,0,sizeof(AttrFileName));
					memcpy(&AttrFileName, BufFileRecord + AttributOffset + 24 + 2*StandartAttributHeader.LengthName, sizeof(AttrFileName));
				}

			if ((StandartAttributHeader.Type == Finde_Attr) && (wcsncmp(AttrFileName.FileName,FindeRecName, sizeof(FindeRecName)/2)==0)) 
				{
					if(Finde_Attr_Name!=NULL)
					{
						wchar_t* Attr_Name;
						Attr_Name = (wchar_t*)malloc(sizeof(Finde_Attr_Name));

						memset(Attr_Name, 0, sizeof(Finde_Attr_Name));
						memcpy(Attr_Name, BufFileRecord+AttributOffset+StandartAttributHeader.ContentOffset,sizeof(Finde_Attr_Name));
						if(wcsncmp(Attr_Name,Finde_Attr_Name, sizeof(Finde_Attr_Name)/2)!=0)
						{
							free(Attr_Name);
							// Извлекаем из считанного заголовок очередного атрибута
							AttributOffset = AttributOffset + StandartAttributHeader.Length; 
							memcpy(&StandartAttributHeader, BufFileRecord + AttributOffset, sizeof(StandartAttributHeader));
							continue;
						}
					}
					Attr_Pointer = (CHAR*)malloc(StandartAttributHeader.Length);
					memcpy(Attr_Pointer, BufFileRecord + AttributOffset, StandartAttributHeader.Length);
					if (Attr_Pointer != NULL)
						return Attr_Pointer;
				}
			// Извлекаем из считанного заголовок очередного атрибута
			AttributOffset = AttributOffset + StandartAttributHeader.Length; 
			memcpy(&StandartAttributHeader, BufFileRecord + AttributOffset, sizeof(StandartAttributHeader));
		}

		memset(&BufFileRecord,0,sizeof(BufFileRecord));
		ReadFile(hDrive, BufFileRecord, 1024, &ReadBytesCnt, NULL);
		// Извлекаем из считанного заголовок файловой записи
		memcpy(&FileHeader, BufFileRecord, sizeof(FileHeader)); 
	}
}
	private: System::Void frmMain_Load(System::Object^  sender, System::EventArgs^  e) {
				 
				 mnMain->ShowItemToolTips = true;

				 DrivesCount = GetLogicalDriveStringsA(sizeof(DrivesString),DrivesString);
				 chPointer = DrivesString;
				 memset(&DrivesArray,0,160);
				 for(i = 0; i < DrivesCount; i++)
				 {
					 DriveType = GetDriveTypeA(chPointer);
					 FlagVolumeInf = GetVolumeInformationA(chPointer, NULL, NULL, NULL, NULL, NULL, DriveFileSystem, 20);  
					 FlagFileSystem = strcmp(DriveFileSystem,"NTFS");
					 if(((DriveType==DRIVE_REMOVABLE) || (DriveType==DRIVE_FIXED)) && (!strcmp(DriveFileSystem,"NTFS")) && (FlagVolumeInf))
					 {
						 memcpy(DrivesArray[NTFS_DrivesCnt],chPointer,2);
						 NTFS_DrivesCnt++;
					 }
					 chPointer+=4;
				 }

				 i=0;
				 while(DrivesArray[i][0]!='\0')
				 {
					 ComboDrive = gcnew String(DrivesArray[i]);
					 DrivesComboBox->Items->Insert(i, ComboDrive);
					 i++;
				 }

		 }
	private: System::Void mFTToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

				 frmMFT^ hMFT=gcnew frmMFT();
				 ReadMFTx(hMFT->dgvMFTData, BootSector.mft_start_cluster);
				 hMFT->ShowDialog();
				 volumeToolStripMenuItem->Enabled = TRUE;


			 }

private: System::Void mTFMirrToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

				 frmMTFMirr^ hMTFMirr=gcnew frmMTFMirr();
				 ReadMFTx(hMTFMirr->dgvMFTMirrData, BootSector.mftmirr_start_cluster);
				 hMTFMirr->ShowDialog();
				 volumeToolStripMenuItem->Enabled = TRUE;

		 }
private: System::Void DrivesComboBox_SelectedIndexChanged(System::Object^  sender, System::EventArgs^  e) {

				 BYTE SelectedDriveIndex;

				 SelectedDriveIndex = DrivesComboBox->SelectedIndex;
				 pSelectedDrive=SelectedDrive+4;
				 strcpy(pSelectedDrive,DrivesArray[SelectedDriveIndex]);
				 hDrive = CreateFile(SelectedDrive, GENERIC_READ | GENERIC_WRITE, FILE_SHARE_READ | FILE_SHARE_WRITE, NULL, OPEN_EXISTING, 0, NULL);
				 if(hDrive!=INVALID_HANDLE_VALUE)
				 {
					 MessageBox::Show("Вы выбрали диск "+ DrivesComboBox->SelectedItem->ToString(), "Выбор активного диска", MessageBoxButtons::OK, MessageBoxIcon::Information);
					 Get_BOOT_Inf(hDrive);
					 mFTToolStripMenuItem->Enabled = TRUE;
					 mTFMirrToolStripMenuItem->Enabled = TRUE;
					 attrDefToolStripMenuItem->Enabled = TRUE;	
					 bootToolStripMenuItem->Enabled = TRUE;
					 bitmapToolStripMenuItem->Enabled = TRUE;
					 upcaseToolStripMenuItem->Enabled = TRUE;
					 badClusToolStripMenuItem->Enabled = TRUE;
				 }
				 else
					 MessageBox::Show("Диск " + DrivesComboBox->SelectedItem->ToString() + " не может быть выбран, так как используется системой или по другой неизвестной причине. Выберите другой диск!", "Выбор активного диска", MessageBoxButtons::OK, MessageBoxIcon::Warning);
		 }
private: System::Void volumeToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

				 frmVolume^ hVolume=gcnew frmVolume();
				 sprintf(OutBuffer1,"%d.%d", VolumeInformation.MajorVersion, VolumeInformation.MinorVersion);
				 sprintf(OutBuffer2,"0x%04X", VolumeInformation.Flags);
				 hVolume->lblVolumeName->Text = gcnew String(VolumeName);
				 hVolume->lblVolumeVers->Text = gcnew String(OutBuffer1);
				 hVolume->lblFlags->Text = gcnew String(OutBuffer2);
				 hVolume->ShowDialog();


		 }
		  
private: System::Void attrDefToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {
					
				 CHAR *Finde_Attr_Pointer, *AttributsDefPt, *BufAttributsDefPt;
				 UCHAR *DataRunsPointer;
				 NotRezidentAttribHeader AttrHeader;
				 AttrDefRecord AttributDef;
				 DataRunsSizes DRFieldSizes;
				 LONGLONG SizeDR, StartClusterDR;
				 UCHAR BufSizeDR[8];
				 frmAttrDef^ hAttrDef=gcnew frmAttrDef();

				 Finde_Attr_Pointer = FindMFTRecord(0x80, L"$AttrDef", NULL);
				 memcpy(&AttrHeader, Finde_Attr_Pointer, sizeof(AttrHeader));
				 DataRunsPointer = (UCHAR*)malloc(AttrHeader.Length-sizeof(AttrHeader)-2*AttrHeader.LengthName);
				 memset(DataRunsPointer, 0, AttrHeader.Length-sizeof(AttrHeader)-2*AttrHeader.LengthName);
				 memcpy(DataRunsPointer, Finde_Attr_Pointer+AttrHeader.DataRunsOffset, AttrHeader.Length-sizeof(AttrHeader)-2*AttrHeader.LengthName);
				 
				 while(*DataRunsPointer!='\0')
				 {
					 memcpy(&DRFieldSizes, DataRunsPointer, sizeof(DataRunsSizes));
					 DataRunsPointer++;
					 memset(BufSizeDR, 0, 8);
					 memcpy(BufSizeDR, DataRunsPointer, DRFieldSizes.SizeLenDR);
					 SizeDR = 0;
					 for(int i=0; i<8; i++)
					 {
						 SizeDR += (LONGLONG)BufSizeDR[i];
						 SizeDR = _rotr64(SizeDR, 8);
					 }
				 
					 memset(BufSizeDR, 0, 8);
					 DataRunsPointer += DRFieldSizes.SizeLenDR;
					 memcpy(BufSizeDR, DataRunsPointer, DRFieldSizes.SizeStartClusterDR);
					 StartClusterDR = 0;
					 for(int i=0; i<8; i++)
					 {
						 StartClusterDR += (LONGLONG)BufSizeDR[i];
						 StartClusterDR = _rotr64(StartClusterDR, 8);
					 }
					 DataRunsPointer += DRFieldSizes.SizeStartClusterDR;
					 AttributsDefPt = Get_Drive_Mem(hDrive, StartClusterDR*BootSector.sectors_per_cluster, SizeDR*BootSector.sectors_per_cluster*BootSector.bytes_per_sector);
					 BufAttributsDefPt = AttributsDefPt;
					 memcpy(&AttributDef, AttributsDefPt, sizeof(AttributDef));
					 while(AttributDef.TypeAttr!=0)
					 {
						 sprintf(OutBuffer1,"0x%X",AttributDef.TypeAttr);
						 memset(OutBuffer2, 0, sizeof(OutBuffer2));
						 if(AttributDef.Flags&0x02)
							 strcat(OutBuffer2, "_I_");
						 if(AttributDef.Flags&0x40)
							 strcat(OutBuffer2, "_R_");
						 if(AttributDef.Flags&0x80)
							 strcat(OutBuffer2, "_N_");
						 memset(OutBuffer3, 0, sizeof(OutBuffer3));
						 _i64toa(AttributDef.MinSize, OutBuffer3, 16);
						 _i64toa(AttributDef.MaxSize, OutBuffer4, 16);
						 strupr(OutBuffer3);
						 strupr(OutBuffer4);
						 hAttrDef->dgvAttrDef->Rows->Add(gcnew String(OutBuffer1), gcnew String(AttributDef.AttrName), gcnew String(OutBuffer2), "0x"+gcnew String(OutBuffer3), "0x"+gcnew String(OutBuffer4));
						 BufAttributsDefPt += sizeof(AttributDef);
						 memcpy(&AttributDef, BufAttributsDefPt, sizeof(AttributDef));
					 }
					 delete[] AttributsDefPt;
				 }
				 free(Finde_Attr_Pointer);
				 
				 hAttrDef->ShowDialog();
				 

		 }
private: System::Void bootToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {


			 frmBoot^ hBoot = gcnew frmBoot();
			 memcpy(OutBuffer1, BootSector.oem_id, sizeof(BootSector.oem_id));
			 hBoot->dgvBoot->Rows->Add("Идентификатор OEM ID", gcnew String(OutBuffer1));
			 hBoot->dgvBoot->Rows->Add("Размер сектора в байтах", BootSector.bytes_per_sector);
			 hBoot->dgvBoot->Rows->Add("Размер кластера в секторах", BootSector.sectors_per_cluster);
			 hBoot->dgvBoot->Rows->Add("Количество резервных секторов", BootSector.reserved_sectors);
			 hBoot->dgvBoot->Rows->Add("Количество FAT", BootSector.num_fats);
			 hBoot->dgvBoot->Rows->Add("Адрес входа в ROOT", BootSector.root_dir_entries);
			 hBoot->dgvBoot->Rows->Add("Количество секторов", BootSector.sectors);
			 hBoot->dgvBoot->Rows->Add("Дескриптор носителя", BootSector.media_type);
			 hBoot->dgvBoot->Rows->Add("Размер FAT в секторах", BootSector.sectors_per_fat);
			 hBoot->dgvBoot->Rows->Add("Количество секторов на дорожке", BootSector.sectors_per_track);
			 hBoot->dgvBoot->Rows->Add("Количество головок", BootSector.heads);
			 hBoot->dgvBoot->Rows->Add("Количество скрытых секторов", BootSector.hidden_sectors);
			 hBoot->dgvBoot->Rows->Add("Общее количество секторов", BootSector.large_sectors);
			 hBoot->dgvBoot->Rows->Add("Общее количество секторов NTFS", BootSector.total_ntfs_sectors);
			 hBoot->dgvBoot->Rows->Add("Начальный кластер MFT", BootSector.mft_start_cluster);
			 hBoot->dgvBoot->Rows->Add("Начальный кластер MFTMirr", BootSector.mftmirr_start_cluster);
			 hBoot->dgvBoot->Rows->Add("Размер MFT записи в кластерах*", BootSector.clusters_per_mft_record);
			 hBoot->dgvBoot->Rows->Add("Размер индексной записи в кластерах", BootSector.clusters_per_index_record);
			 sprintf(OutBuffer1, "%X", BootSector.volume_serial_number);
			 hBoot->dgvBoot->Rows->Add("Серийный номер раздела", gcnew String(OutBuffer1));
			 sprintf(OutBuffer1, "0x%X", BootSector.end_of_sector_marker);
			 hBoot->dgvBoot->Rows->Add("Сигнатура загрузочного сектора", gcnew String(OutBuffer1));
			 hBoot->ShowDialog();
		 }
private: System::Void bitmapToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

			 CHAR *Finde_Attr_Pointer, *AttributsDefPt, *BufAttributsDefPt;
			 UCHAR *DataRunsPointer;
			 NotRezidentAttribHeader AttrHeader;
			 AttrDefRecord AttributDef;
			 DataRunsSizes DRFieldSizes;
			 LONGLONG SizeDR, StartClusterDR;
			 UCHAR BufSizeDR[8];
			 __int64 CntSectors=0;
			 frmBitmap^ hBitmap=gcnew frmBitmap();

			 Finde_Attr_Pointer = FindMFTRecord(0x80, L"$Bitmap", NULL);
			 memcpy(&AttrHeader, Finde_Attr_Pointer, sizeof(AttrHeader));
			 DataRunsPointer = (UCHAR*)malloc(AttrHeader.Length-sizeof(AttrHeader)-2*AttrHeader.LengthName);
			 memset(DataRunsPointer, 0, AttrHeader.Length-sizeof(AttrHeader)-2*AttrHeader.LengthName);
			 memcpy(DataRunsPointer, Finde_Attr_Pointer+AttrHeader.DataRunsOffset, AttrHeader.Length-sizeof(AttrHeader)-2*AttrHeader.LengthName);

			 while(*DataRunsPointer!='\0')
			 {
				 memcpy(&DRFieldSizes, DataRunsPointer, sizeof(DataRunsSizes));
				 DataRunsPointer++;
				 memset(BufSizeDR, 0, 8);
				 memcpy(BufSizeDR, DataRunsPointer, DRFieldSizes.SizeLenDR);
				 SizeDR = 0;
				 for(int i=0; i<8; i++)
				 {
					 SizeDR += (LONGLONG)BufSizeDR[i];
					 SizeDR = _rotr64(SizeDR, 8);
				 }

				 memset(BufSizeDR, 0, 8);
				 DataRunsPointer += DRFieldSizes.SizeLenDR;
				 memcpy(BufSizeDR, DataRunsPointer, DRFieldSizes.SizeStartClusterDR);
				 StartClusterDR = 0;
				 for(int i=0; i<8; i++)
				 {
					 StartClusterDR += (LONGLONG)BufSizeDR[i];
					 StartClusterDR = _rotr64(StartClusterDR, 8);
				 }
				 DataRunsPointer += DRFieldSizes.SizeStartClusterDR;
				 AttributsDefPt = Get_Drive_Mem(hDrive, StartClusterDR*BootSector.sectors_per_cluster, SizeDR*BootSector.sectors_per_cluster*BootSector.bytes_per_sector);
				 BufAttributsDefPt = AttributsDefPt;
				 while(CntSectors<10000)
				 {
					 sprintf(OutBuffer1, "0x%X",CntSectors);
					 hBitmap->dgvBitmap->Rows->Add(gcnew String(OutBuffer1), *BufAttributsDefPt&1, (*BufAttributsDefPt&2)>>1, (*BufAttributsDefPt&4)>>2, (*BufAttributsDefPt&8)>>3, (*BufAttributsDefPt&16)>>4, (*BufAttributsDefPt&32)>>5, (*BufAttributsDefPt&64)>>6,(*BufAttributsDefPt&128)>>7, (*BufAttributsDefPt&256)>>8);
					 CntSectors+=8;
					 BufAttributsDefPt++;
				 }
				 delete[] AttributsDefPt;
			 }
			 free(Finde_Attr_Pointer);

			 hBitmap->ShowDialog();

		 }
private: System::Void upcaseToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

			 CHAR *Finde_Attr_Pointer, *BufAttributsDefPt;
			 wchar_t *AttributsDefPt;
			 UCHAR *DataRunsPointer;
			 NotRezidentAttribHeader AttrHeader;
			 AttrDefRecord AttributDef;
			 DataRunsSizes DRFieldSizes;
			 LONGLONG SizeDR, StartClusterDR;
			 UCHAR BufSizeDR[8];
			 frmUpcase^ hUpcase=gcnew frmUpcase();
			 int rr;
			 Finde_Attr_Pointer = FindMFTRecord(0x80, L"$Upcase", NULL);
			 memcpy(&AttrHeader, Finde_Attr_Pointer, sizeof(AttrHeader));
			 DataRunsPointer = (UCHAR*)malloc(AttrHeader.Length-sizeof(AttrHeader)-2*AttrHeader.LengthName);
			 memset(DataRunsPointer, 0, AttrHeader.Length-sizeof(AttrHeader)-2*AttrHeader.LengthName);
			 memcpy(DataRunsPointer, Finde_Attr_Pointer+AttrHeader.DataRunsOffset, AttrHeader.Length-sizeof(AttrHeader)-2*AttrHeader.LengthName);

			 while(*DataRunsPointer!='\0')
			 {
				 memcpy(&DRFieldSizes, DataRunsPointer, sizeof(DataRunsSizes));
				 DataRunsPointer++;
				 memset(BufSizeDR, 0, 8);
				 memcpy(BufSizeDR, DataRunsPointer, DRFieldSizes.SizeLenDR);
				 SizeDR = 0;
				 for(int i=0; i<8; i++)
				 {
					 SizeDR += (LONGLONG)BufSizeDR[i];
					 SizeDR = _rotr64(SizeDR, 8);
				 }

				 memset(BufSizeDR, 0, 8);
				 DataRunsPointer += DRFieldSizes.SizeLenDR;
				 memcpy(BufSizeDR, DataRunsPointer, DRFieldSizes.SizeStartClusterDR);
				 StartClusterDR = 0;
				 for(int i=0; i<8; i++)
				 {
					 StartClusterDR += (LONGLONG)BufSizeDR[i];
					 StartClusterDR = _rotr64(StartClusterDR, 8);
				 }
				 DataRunsPointer += DRFieldSizes.SizeStartClusterDR;
				 AttributsDefPt = (wchar_t*)Get_Drive_Mem(hDrive, StartClusterDR*BootSector.sectors_per_cluster, SizeDR*BootSector.sectors_per_cluster*BootSector.bytes_per_sector);
				 BufAttributsDefPt = (CHAR*)AttributsDefPt;
				 while(rr<65536)
				 {
					 sprintf(OutBuffer1, "0x%02X",(BYTE)*(BufAttributsDefPt+1));
					 hUpcase->dgvUpcase->Rows->Add(gcnew String(OutBuffer1), *(AttributsDefPt+rr), *(AttributsDefPt+rr+1), *(AttributsDefPt+rr+2), *(AttributsDefPt+rr+3), *(AttributsDefPt+rr+4), *(AttributsDefPt+rr+5), *(AttributsDefPt+rr+6), *(AttributsDefPt+rr+7));
					 rr+=8;
					 BufAttributsDefPt+=16;
				 }
				 delete[] AttributsDefPt;
			 }
			 free(Finde_Attr_Pointer);

			 hUpcase->ShowDialog();
		 }
private: System::Void badClusToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) {

			 CHAR *Finde_Attr_Pointer, *AttributsDefPt, *BufAttributsDefPt;
			 UCHAR *DataRunsPointer;
			 NotRezidentAttribHeader AttrHeader;
			 AttrDefRecord AttributDef;
			 DataRunsSizes DRFieldSizes;
			 LONGLONG SizeDR, StartClusterDR;
			 UCHAR BufSizeDR[8];
			 __int64 CntSectors=0;
			 frmBadClus^ hBadClus=gcnew frmBadClus();
			 memset(&AttrHeader, 0, sizeof(AttrHeader));

			 Finde_Attr_Pointer = FindMFTRecord(0x80, L"$BadClus", L"$Bad");

			 memcpy(&AttrHeader, Finde_Attr_Pointer, sizeof(AttrHeader));
			 if(AttrHeader.RezidentFlag)
			 {
				 DataRunsPointer = (UCHAR*)malloc(AttrHeader.Length-sizeof(AttrHeader)-2*AttrHeader.LengthName);
				 memset(DataRunsPointer, 0, AttrHeader.Length-sizeof(AttrHeader)-2*AttrHeader.LengthName);
				 memcpy(DataRunsPointer, Finde_Attr_Pointer+AttrHeader.DataRunsOffset, AttrHeader.Length-sizeof(AttrHeader)-2*AttrHeader.LengthName);

				 while(*DataRunsPointer!='\0')
				 {
					 memcpy(&DRFieldSizes, DataRunsPointer, sizeof(DataRunsSizes));
					 DataRunsPointer++;
					 memset(BufSizeDR, 0, 8);
					 memcpy(BufSizeDR, DataRunsPointer, DRFieldSizes.SizeLenDR);
					 SizeDR = 0;
					 for(int i=0; i<8; i++)
					 {
						 SizeDR += (LONGLONG)BufSizeDR[i];
						 SizeDR = _rotr64(SizeDR, 8);
					 }
					 if(SizeDR*BootSector.sectors_per_cluster==BootSector.total_ntfs_sectors)
						 break;
					 memset(BufSizeDR, 0, 8);
					 DataRunsPointer += DRFieldSizes.SizeLenDR;
					 memcpy(BufSizeDR, DataRunsPointer, DRFieldSizes.SizeStartClusterDR);
					 StartClusterDR = 0;
					 for(int i=0; i<8; i++)
					 {
						 StartClusterDR += (LONGLONG)BufSizeDR[i];
						 StartClusterDR = _rotr64(StartClusterDR, 8);
					 }
					 DataRunsPointer += DRFieldSizes.SizeStartClusterDR;
					 AttributsDefPt = Get_Drive_Mem(hDrive, StartClusterDR*BootSector.sectors_per_cluster, SizeDR*BootSector.sectors_per_cluster*BootSector.bytes_per_sector);
					 BufAttributsDefPt = AttributsDefPt;
					 while(CntSectors<10000)
					 {
						 sprintf(OutBuffer1, "0x%X",CntSectors);
						 hBadClus->dgvBadClust->Rows->Add(gcnew String(OutBuffer1), *BufAttributsDefPt&1, (*BufAttributsDefPt&2)>>1, (*BufAttributsDefPt&4)>>2, (*BufAttributsDefPt&8)>>3, (*BufAttributsDefPt&16)>>4, (*BufAttributsDefPt&32)>>5, (*BufAttributsDefPt&64)>>6,(*BufAttributsDefPt&128)>>7, (*BufAttributsDefPt&256)>>8);
						 CntSectors+=8;
						 BufAttributsDefPt++;
					 }
					 delete[] AttributsDefPt;
				 }
				 free(Finde_Attr_Pointer);
			 }
			 if(SizeDR*BootSector.sectors_per_cluster==BootSector.total_ntfs_sectors)
			 {
				 MessageBox::Show("На диске " + DrivesComboBox->SelectedItem->ToString() + " битых секторов не обнаруженно.", "Поиск битых секторов", MessageBoxButtons::OK, MessageBoxIcon::Information);
			 }
			 else 
				hBadClus->ShowDialog();
		 }
};
}

