#pragma once

namespace NTFS_System_Files {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� frmBitmap
	/// </summary>
	public ref class frmBitmap : public System::Windows::Forms::Form
	{
	public:
		frmBitmap(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~frmBitmap()
		{
			if (components)
			{
				delete components;
			}
		}
	public: System::Windows::Forms::DataGridView^  dgvBitmap;
	protected: 

	protected: 
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ClmIndex;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ClmCluster0;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ClmCluster1;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ClmCluster2;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ClmCluster3;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ClmCluster4;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ClmCluster5;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ClmCluster6;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ClmCluster7;

	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle3 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle4 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			this->dgvBitmap = (gcnew System::Windows::Forms::DataGridView());
			this->ClmIndex = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->ClmCluster0 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->ClmCluster1 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->ClmCluster2 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->ClmCluster3 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->ClmCluster4 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->ClmCluster5 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->ClmCluster6 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->ClmCluster7 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dgvBitmap))->BeginInit();
			this->SuspendLayout();
			// 
			// dgvBitmap
			// 
			this->dgvBitmap->AllowUserToDeleteRows = false;
			this->dgvBitmap->AllowUserToResizeColumns = false;
			this->dgvBitmap->AllowUserToResizeRows = false;
			dataGridViewCellStyle3->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			dataGridViewCellStyle3->BackColor = System::Drawing::SystemColors::Control;
			dataGridViewCellStyle3->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			dataGridViewCellStyle3->ForeColor = System::Drawing::SystemColors::WindowText;
			dataGridViewCellStyle3->SelectionBackColor = System::Drawing::SystemColors::Highlight;
			dataGridViewCellStyle3->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle3->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
			this->dgvBitmap->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
			this->dgvBitmap->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dgvBitmap->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(9) {this->ClmIndex, this->ClmCluster0, 
				this->ClmCluster1, this->ClmCluster2, this->ClmCluster3, this->ClmCluster4, this->ClmCluster5, this->ClmCluster6, this->ClmCluster7});
			this->dgvBitmap->Location = System::Drawing::Point(12, 12);
			this->dgvBitmap->Name = L"dgvBitmap";
			this->dgvBitmap->RowHeadersVisible = false;
			this->dgvBitmap->ScrollBars = System::Windows::Forms::ScrollBars::Vertical;
			this->dgvBitmap->Size = System::Drawing::Size(281, 238);
			this->dgvBitmap->TabIndex = 0;
			// 
			// ClmIndex
			// 
			dataGridViewCellStyle4->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			this->ClmIndex->DefaultCellStyle = dataGridViewCellStyle4;
			this->ClmIndex->HeaderText = L"�/�";
			this->ClmIndex->Name = L"ClmIndex";
			this->ClmIndex->ReadOnly = true;
			// 
			// ClmCluster0
			// 
			this->ClmCluster0->HeaderText = L"0";
			this->ClmCluster0->Name = L"ClmCluster0";
			this->ClmCluster0->ReadOnly = true;
			this->ClmCluster0->Width = 20;
			// 
			// ClmCluster1
			// 
			this->ClmCluster1->HeaderText = L"1";
			this->ClmCluster1->Name = L"ClmCluster1";
			this->ClmCluster1->ReadOnly = true;
			this->ClmCluster1->Width = 20;
			// 
			// ClmCluster2
			// 
			this->ClmCluster2->HeaderText = L"2";
			this->ClmCluster2->Name = L"ClmCluster2";
			this->ClmCluster2->ReadOnly = true;
			this->ClmCluster2->Width = 20;
			// 
			// ClmCluster3
			// 
			this->ClmCluster3->HeaderText = L"3";
			this->ClmCluster3->Name = L"ClmCluster3";
			this->ClmCluster3->ReadOnly = true;
			this->ClmCluster3->Width = 20;
			// 
			// ClmCluster4
			// 
			this->ClmCluster4->HeaderText = L"4";
			this->ClmCluster4->Name = L"ClmCluster4";
			this->ClmCluster4->ReadOnly = true;
			this->ClmCluster4->Width = 20;
			// 
			// ClmCluster5
			// 
			this->ClmCluster5->HeaderText = L"5";
			this->ClmCluster5->Name = L"ClmCluster5";
			this->ClmCluster5->ReadOnly = true;
			this->ClmCluster5->Width = 20;
			// 
			// ClmCluster6
			// 
			this->ClmCluster6->HeaderText = L"6";
			this->ClmCluster6->Name = L"ClmCluster6";
			this->ClmCluster6->ReadOnly = true;
			this->ClmCluster6->Width = 20;
			// 
			// ClmCluster7
			// 
			this->ClmCluster7->HeaderText = L"7";
			this->ClmCluster7->Name = L"ClmCluster7";
			this->ClmCluster7->ReadOnly = true;
			this->ClmCluster7->Width = 20;
			// 
			// frmBitmap
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(305, 262);
			this->Controls->Add(this->dgvBitmap);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
			this->MaximizeBox = false;
			this->Name = L"frmBitmap";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"����� �����";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dgvBitmap))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	};
}
