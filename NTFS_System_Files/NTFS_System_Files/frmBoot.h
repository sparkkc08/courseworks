#pragma once

namespace NTFS_System_Files {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� frmBoot
	/// </summary>
	public ref class frmBoot : public System::Windows::Forms::Form
	{
	public:
		frmBoot(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~frmBoot()
		{
			if (components)
			{
				delete components;
			}
		}
	public: System::Windows::Forms::DataGridView^  dgvBoot;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ClmDescription;
	public: 
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ClmValue;
	protected: 

	public: 


	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle1 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle4 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle2 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle3 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			this->dgvBoot = (gcnew System::Windows::Forms::DataGridView());
			this->ClmDescription = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->ClmValue = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dgvBoot))->BeginInit();
			this->SuspendLayout();
			// 
			// dgvBoot
			// 
			this->dgvBoot->AllowUserToResizeRows = false;
			dataGridViewCellStyle1->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			dataGridViewCellStyle1->BackColor = System::Drawing::SystemColors::Control;
			dataGridViewCellStyle1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			dataGridViewCellStyle1->ForeColor = System::Drawing::SystemColors::WindowText;
			dataGridViewCellStyle1->SelectionBackColor = System::Drawing::SystemColors::Highlight;
			dataGridViewCellStyle1->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			this->dgvBoot->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this->dgvBoot->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dgvBoot->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(2) {this->ClmDescription, 
				this->ClmValue});
			this->dgvBoot->Location = System::Drawing::Point(12, 13);
			this->dgvBoot->Name = L"dgvBoot";
			this->dgvBoot->ReadOnly = true;
			this->dgvBoot->RowHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::None;
			dataGridViewCellStyle4->BackColor = System::Drawing::SystemColors::Control;
			dataGridViewCellStyle4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			dataGridViewCellStyle4->ForeColor = System::Drawing::SystemColors::WindowText;
			dataGridViewCellStyle4->SelectionBackColor = System::Drawing::SystemColors::Highlight;
			dataGridViewCellStyle4->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle4->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
			this->dgvBoot->RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
			this->dgvBoot->RowHeadersVisible = false;
			this->dgvBoot->ScrollBars = System::Windows::Forms::ScrollBars::None;
			this->dgvBoot->Size = System::Drawing::Size(460, 462);
			this->dgvBoot->TabIndex = 0;
			// 
			// ClmDescription
			// 
			dataGridViewCellStyle2->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
			this->ClmDescription->DefaultCellStyle = dataGridViewCellStyle2;
			this->ClmDescription->HeaderText = L"��������";
			this->ClmDescription->Name = L"ClmDescription";
			this->ClmDescription->ReadOnly = true;
			this->ClmDescription->Width = 300;
			// 
			// ClmValue
			// 
			dataGridViewCellStyle3->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			this->ClmValue->DefaultCellStyle = dataGridViewCellStyle3;
			this->ClmValue->HeaderText = L"��������";
			this->ClmValue->Name = L"ClmValue";
			this->ClmValue->ReadOnly = true;
			this->ClmValue->Width = 160;
			// 
			// frmBoot
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(489, 491);
			this->Controls->Add(this->dgvBoot);
			this->MaximizeBox = false;
			this->Name = L"frmBoot";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"���������� ������������ �������";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dgvBoot))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	};
}
