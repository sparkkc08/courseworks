#pragma once

namespace NTFS_System_Files {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� frmVolume
	/// </summary>
	public ref class frmVolume : public System::Windows::Forms::Form
	{
	public:
		frmVolume(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~frmVolume()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  lblVolumeNameCaption;
	protected: 
	private: System::Windows::Forms::Label^  lblVolumeVersCaption;
	public: System::Windows::Forms::Label^  lblVolumeName;
	private: 
	public: System::Windows::Forms::Label^  lblVolumeVers;
	private: System::Windows::Forms::Label^  lblFlagsCaption;
	public: System::Windows::Forms::Label^  lblFlags;
	private: 
	public: 




	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->lblVolumeNameCaption = (gcnew System::Windows::Forms::Label());
			this->lblVolumeVersCaption = (gcnew System::Windows::Forms::Label());
			this->lblVolumeName = (gcnew System::Windows::Forms::Label());
			this->lblVolumeVers = (gcnew System::Windows::Forms::Label());
			this->lblFlagsCaption = (gcnew System::Windows::Forms::Label());
			this->lblFlags = (gcnew System::Windows::Forms::Label());
			this->SuspendLayout();
			// 
			// lblVolumeNameCaption
			// 
			this->lblVolumeNameCaption->AutoSize = true;
			this->lblVolumeNameCaption->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			this->lblVolumeNameCaption->Location = System::Drawing::Point(28, 26);
			this->lblVolumeNameCaption->Name = L"lblVolumeNameCaption";
			this->lblVolumeNameCaption->Size = System::Drawing::Size(79, 17);
			this->lblVolumeNameCaption->TabIndex = 0;
			this->lblVolumeNameCaption->Text = L"��� ����: ";
			// 
			// lblVolumeVersCaption
			// 
			this->lblVolumeVersCaption->AutoSize = true;
			this->lblVolumeVersCaption->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			this->lblVolumeVersCaption->Location = System::Drawing::Point(28, 65);
			this->lblVolumeVersCaption->Name = L"lblVolumeVersCaption";
			this->lblVolumeVersCaption->Size = System::Drawing::Size(194, 17);
			this->lblVolumeVersCaption->TabIndex = 1;
			this->lblVolumeVersCaption->Text = L"������ �������� �������: ";
			// 
			// lblVolumeName
			// 
			this->lblVolumeName->AutoSize = true;
			this->lblVolumeName->Font = (gcnew System::Drawing::Font(L"Tahoma", 11, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->lblVolumeName->ForeColor = System::Drawing::Color::MediumBlue;
			this->lblVolumeName->Location = System::Drawing::Point(115, 26);
			this->lblVolumeName->Name = L"lblVolumeName";
			this->lblVolumeName->Size = System::Drawing::Size(0, 18);
			this->lblVolumeName->TabIndex = 2;
			// 
			// lblVolumeVers
			// 
			this->lblVolumeVers->AutoSize = true;
			this->lblVolumeVers->Font = (gcnew System::Drawing::Font(L"Mistral", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->lblVolumeVers->ForeColor = System::Drawing::Color::Navy;
			this->lblVolumeVers->Location = System::Drawing::Point(228, 64);
			this->lblVolumeVers->Name = L"lblVolumeVers";
			this->lblVolumeVers->Size = System::Drawing::Size(0, 19);
			this->lblVolumeVers->TabIndex = 3;
			// 
			// lblFlagsCaption
			// 
			this->lblFlagsCaption->AutoSize = true;
			this->lblFlagsCaption->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->lblFlagsCaption->Location = System::Drawing::Point(28, 102);
			this->lblFlagsCaption->Name = L"lblFlagsCaption";
			this->lblFlagsCaption->Size = System::Drawing::Size(97, 17);
			this->lblFlagsCaption->TabIndex = 4;
			this->lblFlagsCaption->Text = L"���� ������:";
			// 
			// lblFlags
			// 
			this->lblFlags->AutoSize = true;
			this->lblFlags->Font = (gcnew System::Drawing::Font(L"MV Boli", 12, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point, 
				static_cast<System::Byte>(204)));
			this->lblFlags->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(192)), 
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->lblFlags->Location = System::Drawing::Point(134, 102);
			this->lblFlags->Name = L"lblFlags";
			this->lblFlags->Size = System::Drawing::Size(0, 18);
			this->lblFlags->TabIndex = 5;
			// 
			// frmVolume
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(592, 136);
			this->Controls->Add(this->lblFlags);
			this->Controls->Add(this->lblFlagsCaption);
			this->Controls->Add(this->lblVolumeVers);
			this->Controls->Add(this->lblVolumeName);
			this->Controls->Add(this->lblVolumeVersCaption);
			this->Controls->Add(this->lblVolumeNameCaption);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
			this->MaximizeBox = false;
			this->Name = L"frmVolume";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"���������� � ����";
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	};
}
