#pragma once

namespace NTFS_System_Files {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� frmMFT
	/// </summary>
	public ref class frmMFT : public System::Windows::Forms::Form
	{
	public:
		frmMFT(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~frmMFT()
		{
			if (components)
			{
				delete components;
			}
		}
	public: System::Windows::Forms::DataGridView^  dgvMFTData;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ClmFileName;
	public: 
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ClmFileSize;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ClmCreateTime;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ClmEditTime;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ClmAttributs;

	public: 





	public: 





	public: 





	public: 





	public: 





	public: 





	public: 




	protected: 

	protected: 

	protected: 






	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle1 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle4 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle5 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle2 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle3 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			this->dgvMFTData = (gcnew System::Windows::Forms::DataGridView());
			this->ClmFileName = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->ClmFileSize = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->ClmCreateTime = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->ClmEditTime = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->ClmAttributs = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dgvMFTData))->BeginInit();
			this->SuspendLayout();
			// 
			// dgvMFTData
			// 
			this->dgvMFTData->AllowUserToResizeRows = false;
			dataGridViewCellStyle1->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			dataGridViewCellStyle1->BackColor = System::Drawing::SystemColors::Control;
			dataGridViewCellStyle1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			dataGridViewCellStyle1->ForeColor = System::Drawing::SystemColors::WindowText;
			dataGridViewCellStyle1->SelectionBackColor = System::Drawing::SystemColors::Highlight;
			dataGridViewCellStyle1->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle1->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
			this->dgvMFTData->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this->dgvMFTData->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dgvMFTData->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(5) {this->ClmFileName, 
				this->ClmFileSize, this->ClmCreateTime, this->ClmEditTime, this->ClmAttributs});
			dataGridViewCellStyle4->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			dataGridViewCellStyle4->BackColor = System::Drawing::SystemColors::Window;
			dataGridViewCellStyle4->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			dataGridViewCellStyle4->ForeColor = System::Drawing::SystemColors::ControlText;
			dataGridViewCellStyle4->SelectionBackColor = System::Drawing::SystemColors::Highlight;
			dataGridViewCellStyle4->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle4->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
			this->dgvMFTData->DefaultCellStyle = dataGridViewCellStyle4;
			this->dgvMFTData->Location = System::Drawing::Point(12, 12);
			this->dgvMFTData->Name = L"dgvMFTData";
			this->dgvMFTData->ReadOnly = true;
			this->dgvMFTData->RowHeadersBorderStyle = System::Windows::Forms::DataGridViewHeaderBorderStyle::None;
			dataGridViewCellStyle5->BackColor = System::Drawing::SystemColors::Control;
			dataGridViewCellStyle5->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			dataGridViewCellStyle5->ForeColor = System::Drawing::SystemColors::WindowText;
			dataGridViewCellStyle5->SelectionBackColor = System::Drawing::SystemColors::Highlight;
			dataGridViewCellStyle5->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			this->dgvMFTData->RowHeadersDefaultCellStyle = dataGridViewCellStyle5;
			this->dgvMFTData->RowHeadersVisible = false;
			this->dgvMFTData->RowHeadersWidthSizeMode = System::Windows::Forms::DataGridViewRowHeadersWidthSizeMode::DisableResizing;
			this->dgvMFTData->Size = System::Drawing::Size(712, 345);
			this->dgvMFTData->TabIndex = 0;
			// 
			// ClmFileName
			// 
			this->ClmFileName->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
			dataGridViewCellStyle2->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleLeft;
			this->ClmFileName->DefaultCellStyle = dataGridViewCellStyle2;
			this->ClmFileName->HeaderText = L"��� �����";
			this->ClmFileName->Name = L"ClmFileName";
			this->ClmFileName->ReadOnly = true;
			this->ClmFileName->Width = 152;
			// 
			// ClmFileSize
			// 
			this->ClmFileSize->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
			dataGridViewCellStyle3->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			this->ClmFileSize->DefaultCellStyle = dataGridViewCellStyle3;
			this->ClmFileSize->HeaderText = L"������ �����, ����";
			this->ClmFileSize->Name = L"ClmFileSize";
			this->ClmFileSize->ReadOnly = true;
			this->ClmFileSize->Width = 140;
			// 
			// ClmCreateTime
			// 
			this->ClmCreateTime->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
			this->ClmCreateTime->HeaderText = L"����� ��������";
			this->ClmCreateTime->Name = L"ClmCreateTime";
			this->ClmCreateTime->ReadOnly = true;
			this->ClmCreateTime->Width = 120;
			// 
			// ClmEditTime
			// 
			this->ClmEditTime->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
			this->ClmEditTime->HeaderText = L"����� ���������";
			this->ClmEditTime->Name = L"ClmEditTime";
			this->ClmEditTime->ReadOnly = true;
			this->ClmEditTime->Width = 130;
			// 
			// ClmAttributs
			// 
			this->ClmAttributs->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::None;
			this->ClmAttributs->HeaderText = L"��������";
			this->ClmAttributs->Name = L"ClmAttributs";
			this->ClmAttributs->ReadOnly = true;
			this->ClmAttributs->ToolTipText = L"R - ������ ������, � - �������, S - ���������, � - ��������, � - ������, D - ����������";
			this->ClmAttributs->Width = 150;
			// 
			// frmMFT
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(736, 369);
			this->Controls->Add(this->dgvMFTData);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
			this->MaximizeBox = false;
			this->Name = L"frmMFT";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"������� MFT";
			this->Load += gcnew System::EventHandler(this, &frmMFT::frmMFT_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dgvMFTData))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void frmMFT_Load(System::Object^  sender, System::EventArgs^  e) {
			 }
	};
}
