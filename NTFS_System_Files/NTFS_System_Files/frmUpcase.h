#pragma once

namespace NTFS_System_Files {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// ������ ��� frmUpcase
	/// </summary>
	public ref class frmUpcase : public System::Windows::Forms::Form
	{
	public:
		frmUpcase(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~frmUpcase()
		{
			if (components)
			{
				delete components;
			}
		}
	public: System::Windows::Forms::DataGridView^  dgvUpcase;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  ClmStr;
	public: 
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Clm0;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Clm1;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Clm2;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Clm3;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Clm4;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Clm5;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Clm6;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  Clm7;
	protected: 

	protected: 









	protected: 

	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle1 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			System::Windows::Forms::DataGridViewCellStyle^  dataGridViewCellStyle2 = (gcnew System::Windows::Forms::DataGridViewCellStyle());
			this->dgvUpcase = (gcnew System::Windows::Forms::DataGridView());
			this->ClmStr = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Clm0 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Clm1 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Clm2 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Clm3 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Clm4 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Clm5 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Clm6 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->Clm7 = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dgvUpcase))->BeginInit();
			this->SuspendLayout();
			// 
			// dgvUpcase
			// 
			this->dgvUpcase->AllowUserToDeleteRows = false;
			this->dgvUpcase->AllowUserToResizeColumns = false;
			this->dgvUpcase->AllowUserToResizeRows = false;
			this->dgvUpcase->ClipboardCopyMode = System::Windows::Forms::DataGridViewClipboardCopyMode::EnableAlwaysIncludeHeaderText;
			dataGridViewCellStyle1->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			dataGridViewCellStyle1->BackColor = System::Drawing::SystemColors::Control;
			dataGridViewCellStyle1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			dataGridViewCellStyle1->ForeColor = System::Drawing::SystemColors::WindowText;
			dataGridViewCellStyle1->SelectionBackColor = System::Drawing::SystemColors::Highlight;
			dataGridViewCellStyle1->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle1->WrapMode = System::Windows::Forms::DataGridViewTriState::True;
			this->dgvUpcase->ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
			this->dgvUpcase->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dgvUpcase->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(9) {this->ClmStr, this->Clm0, 
				this->Clm1, this->Clm2, this->Clm3, this->Clm4, this->Clm5, this->Clm6, this->Clm7});
			dataGridViewCellStyle2->Alignment = System::Windows::Forms::DataGridViewContentAlignment::MiddleCenter;
			dataGridViewCellStyle2->BackColor = System::Drawing::SystemColors::Window;
			dataGridViewCellStyle2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 8.25F, System::Drawing::FontStyle::Regular, 
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			dataGridViewCellStyle2->ForeColor = System::Drawing::SystemColors::ControlText;
			dataGridViewCellStyle2->SelectionBackColor = System::Drawing::SystemColors::Highlight;
			dataGridViewCellStyle2->SelectionForeColor = System::Drawing::SystemColors::HighlightText;
			dataGridViewCellStyle2->WrapMode = System::Windows::Forms::DataGridViewTriState::False;
			this->dgvUpcase->DefaultCellStyle = dataGridViewCellStyle2;
			this->dgvUpcase->Location = System::Drawing::Point(12, 12);
			this->dgvUpcase->Name = L"dgvUpcase";
			this->dgvUpcase->ReadOnly = true;
			this->dgvUpcase->RowHeadersVisible = false;
			this->dgvUpcase->ScrollBars = System::Windows::Forms::ScrollBars::Vertical;
			this->dgvUpcase->Size = System::Drawing::Size(524, 238);
			this->dgvUpcase->TabIndex = 0;
			// 
			// ClmStr
			// 
			this->ClmStr->HeaderText = L"� ��������";
			this->ClmStr->Name = L"ClmStr";
			this->ClmStr->ReadOnly = true;
			// 
			// Clm0
			// 
			this->Clm0->HeaderText = L"0";
			this->Clm0->Name = L"Clm0";
			this->Clm0->ReadOnly = true;
			this->Clm0->Width = 50;
			// 
			// Clm1
			// 
			this->Clm1->HeaderText = L"1";
			this->Clm1->Name = L"Clm1";
			this->Clm1->ReadOnly = true;
			this->Clm1->Width = 50;
			// 
			// Clm2
			// 
			this->Clm2->HeaderText = L"2";
			this->Clm2->Name = L"Clm2";
			this->Clm2->ReadOnly = true;
			this->Clm2->Width = 50;
			// 
			// Clm3
			// 
			this->Clm3->HeaderText = L"3";
			this->Clm3->Name = L"Clm3";
			this->Clm3->ReadOnly = true;
			this->Clm3->Width = 50;
			// 
			// Clm4
			// 
			this->Clm4->HeaderText = L"4";
			this->Clm4->Name = L"Clm4";
			this->Clm4->ReadOnly = true;
			this->Clm4->Width = 50;
			// 
			// Clm5
			// 
			this->Clm5->HeaderText = L"5";
			this->Clm5->Name = L"Clm5";
			this->Clm5->ReadOnly = true;
			this->Clm5->Width = 50;
			// 
			// Clm6
			// 
			this->Clm6->HeaderText = L"6";
			this->Clm6->Name = L"Clm6";
			this->Clm6->ReadOnly = true;
			this->Clm6->Width = 50;
			// 
			// Clm7
			// 
			this->Clm7->HeaderText = L"7";
			this->Clm7->Name = L"Clm7";
			this->Clm7->ReadOnly = true;
			this->Clm7->Width = 50;
			// 
			// frmUpcase
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(548, 262);
			this->Controls->Add(this->dgvUpcase);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
			this->MaximizeBox = false;
			this->Name = L"frmUpcase";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"������� Unicode";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^  >(this->dgvUpcase))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	};
}
